#!/usr/bin/env bash

# ----------------------------------------------------------------------------------------------------------------------
# "Library"
# ----------------------------------------------------------------------------------------------------------------------

set -o errexit
set -o nounset
set -o pipefail

# Internal constants

declare -r _blue_="\e[34m"
declare -r _cyan_="\e[36m"
declare -r _default_="\e[0m"
declare -r _green_="\e[32m"
declare -r _red_="\e[31m"

# Bootstrap

if [ "`getopt --longoptions xtrace -- x "$@" 2> /dev/null | grep --color=none "\(^\|\s\)\(\-x\|\-\-xtrace\)\($\|\s\)"`" != "" ] ; then
    declare -r __XTRACE__=1
    set -o xtrace
else
    declare -r __XTRACE__=0
fi

declare -r __PID__="${$}"

declare -r __FILE__="$(realpath "${0}")"
declare -r __SCRIPT__="$(basename "${__FILE__}")"
declare -r __ROOT__="$(realpath "$(dirname "${__FILE__}")")"

__FILTER__=""

printf "${_cyan_}Startup:${_default_} started process ${__PID__}\n\n"

# Error and exit handling: exit is trapped, as well as signals.
# If a __cleanup__ function exists, it will be called on signal or exit and the exit code will be passed as parameter.

__trap_signals__()
{
    local code="${?}"
    local signal
    local name

    if [ ${code} -eq 0 ] ; then
        signal=0
        name="EXIT"
        printf "\nProcess ${__PID__} received SIG${name} (${signal}), exiting..."
    else
        signal=$((${code} - 128))
        name="`kill -l ${signal}`"
        >&2 printf "\nProcess ${__PID__} received SIG${name} (${signal}), exiting..."
    fi
}

__trap_exit__()
{
    local code="${?}"

    if [ ${code} -eq 0 ] ; then
        printf "\n${_green_}Success:${_default_} process ${__PID__} exited normally\n"
    else
        >&2 printf "\n${_red_}Error:${_default_} process ${__PID__} exited with error code ${code}\n"
    fi

    # @todo: no cleanup should be done for SIGQUIT ?
    if [ "`type -t __cleanup__`" = "function" ] ; then
        __cleanup__
    fi
}

trap "__trap_signals__" SIGHUP SIGINT SIGQUIT SIGTERM
trap "__trap_exit__" EXIT

# ----------------------------------------------------------------------------------------------------------------------
# Custom code
# ----------------------------------------------------------------------------------------------------------------------

PLUGIN_NAME="LibricielBootstrap3"
# ln -s /tmp/cakephp-libriciel-bootstrap3 app/Plugin/LibricelBootstrap3
# ini_set('include_path', $vendor . DS . $phpunitPath . PATH_SEPARATOR . ini_get('include_path'));

__APP__="${__ROOT__}/app"
__TMP__="${__APP__}/tmp"
__OUT__="${__TMP__}/out/Plugin_${PLUGIN_NAME}"

# Usage function

__usage__()
{
    printf "NAME\n"
    printf "  %s\n" "$__SCRIPT__"
    printf "\nDESCRIPTION\n"
    printf "  Commandes courantes pour un projet CakePHP 2.x chez Libriciel SCOP\"\n"
    printf "\nSYNOPSIS\n"
    printf "  %s [OPTION] [COMMAND]\n" "$__SCRIPT__"
    printf "\nCOMMANDS\n"
    printf "  check\t\tVérifie que l'on soit bien à la racine d'un projet CakePHP 2.x\n"
    printf "  clear\t\tVérifie l'installation (commande check) et nettoie le cache d'un projet CakePHP 2.x\n"
    printf "  lint_css\tVérification de la syntaxe des fichiers .css\n"
    printf "  lint_js\tVérification de la syntaxe des fichiers .js\n"
    printf "  lint_php\tVérification de la syntaxe des fichiers .php, .ctp\n"
    printf "  lint_po\tVérification de la syntaxe des fichiers .po\n"
    printf "  lint_sh\tVérification de la syntaxe des fichiers .sh (bash)\n"
    printf "  phpcbf\tPHP Code Beautifier and Fixer, pas effectuée dans les fichiers .ctp ni dans le dossier Test\n"
    printf "  phpcpd\tVérifications de code copié/collé, pas effectuée dans les fichiers .ctp\n"
    printf "  phpcs\t\tVérifications CheckStyle, pas effectuée dans les fichiers .ctp ni dans le dossier Test\n"
    printf "  phpmd\t\tVérifications mess detector, pas effectuée dans les fichiers .ctp ni dans le dossier Test\n"
    printf "  pre_commit\tLance une partie des vérifications lancées par le .gitlab-ci.yml\n"
    printf "  tail\t\tTail les fichiers de log de CakePHP 2.x (et les crée si besoin)\n"
    printf "  tests\t\tNettoie le cache (commande clear) et effectue les tests unitaires\n"
    printf "\nOPTIONS\n"
    printf "  --filter\tOption filter passée aux tests unitaires PHPUnit.\n"
    printf "  -h|--help\tAffiche cette aide\n"
    printf "  -x|--xtrace\tMode debug, affiche chaque commande avant de l'exécuter (set -o xtrace)\n"
    printf "\nEXEMPLES\n"
    printf "  %s -h\n" "$__SCRIPT__"
    printf "  %s clear\n" "$__SCRIPT__"
    printf "  %s lint_php\n" "$__SCRIPT__"
    printf "  %s lint_po\n" "$__SCRIPT__"
    printf "  %s tests\n" "$__SCRIPT__"
    printf "  %s tests View/Helper/LibricielBootstrap3Helper --filter testIndexPage1Of7\n" "$__SCRIPT__"
}

cake2_dir()
{
    candidates="lib/Cake \
        vendor/cakephp/cakephp/lib/Cake \
        vendors/cakephp/cakephp/lib/Cake"

    for candidate in $candidates ; do
        if [ -d "${candidate}" ] ; then
            echo "${candidate}"
            return 0
        fi
    done

    >&2 printf "\n${_red_}Error:${_default_} could not find CakePHP's VERSION.txt file (searched in ${candidates})"
    return 1
}

cake2_check()
{
    echo "Checking for a CakePHP 2.x install"

    fileName="`cake2_dir`/VERSION.txt"

    if [ ! -f "${fileName}" ] ; then
        >&2 printf "${_red_}Error:${_default_} could not find CakePHP's VERSION.txt file (${fileName})"
        return 1
    else
        grep --color=none "^2\.[0-9]\+" $fileName
        return $?
    fi
}

# Clears temporary files in app/tmp for a CakePHP 2.x install
cake2_clear()
{
    local find_cmd="find -L ${__TMP__} -type f ! -name 'empty'"
    cake2_check \
    && echo "Clearing the CakePHP 2.x install" \
		&& sudo bash -c "( if [ ! -d ${__TMP__} ] ; then mkdir -p ${__TMP__} ; fi )" \
		&& nb_target="`sudo bash -c "( ${find_cmd} ) | wc -l"`" \
		&& sudo bash -c "( ${find_cmd} -exec rm {} \; )" \
		&& nb_remaining="`sudo bash -c "( ${find_cmd} ) | wc -l"`" \
		&& nb_done=$((nb_target - nb_remaining))

    if [ ${nb_target} -le ${nb_done} ] ; then
        printf "${_cyan_}Success:${_default_} ${nb_done} file(s) cleared\n"
    else
        >&2 printf "${_red_}Error:${_default_} ${nb_done} file(s) cleared out of ${nb_target} (`${find_cmd}`)\n"
    fi

    dirs="${__TMP__}/cache/models ${__TMP__}/cache/persistent ${__TMP__}/cache/views ${__TMP__}/logs ${__TMP__}/sessions ${__TMP__}/tests"
    for dir in $dirs ; do
        if [ ! -d "${dir}" ] ; then
            mkdir -p "${dir}"
        fi
        touch "${dir}/empty"
    done

	return $?
}

tests()
{
    local case="${1}"

    # @todo: --strict
    # test app ${case} -app app \
    cake2_clear \
    && rm -rf "${__OUT__}/build" \
    && mkdir -p "${__OUT__}/build" \
    && "${__ROOT__}/vendors/bin/cake" \
        test ${PLUGIN_NAME} ${case} \
        ${__FILTER__} \
        --verbose \
        --stderr \
        --debug \
        --configuration "${__ROOT__}/phpunit.xml" \
        -app "${__APP__}"
}

lint_css()
{
    local code=0
    local return=0
    which csslint || ( >&2 printf "${_red_}Error:${_default_} csslint is not installed, fix it with: ${_cyan_}sudo npm install --global csslint${_default_}" ; exit 1 )
    set +o errexit

    for file in $(find -L "${__APP__}/Plugin/LibricielBootstrap3" -type f -name "*.css" -not -path "*/docker/*"  -not -path "*/Plugin/*" -not -path "*/plugins/*" -not -path "*/vendors/*" -not -path "*/Vendor/*" | sort -t '/') ; do
        echo "${file}"
        csslint --quiet "${file}"
        code=$?
        if [ $code -ne 0 ] ; then
            return=$code
        fi
    done

    set -o errexit
    return $return
}

lint_js()
{
    local code=0
    local return=0
    which esvalidate || ( >&2 printf "${_red_}Error:${_default_} esvalidate is not installed, fix it with: ${_cyan_}sudo npm install --global esvalidate${_default_}" ; exit 1 )
    set +o errexit

    for file in $(find -L "${__APP__}/Plugin/LibricielBootstrap3" -type f -name "*.js" -not -path "*/docker/*"  -not -path "*/Plugin/*" -not -path "*/plugins/*" -not -path "*/vendors/*" -not -path "*/Vendor/*" | sort -t '/') ; do
        echo "${file}"
        esvalidate "${file}"
        code=$?
        if [ $code -ne 0 ] ; then
            return=$code
        fi
    done

    set -o errexit
    return $return
}

lint_php()
{
    local code=0
    local return=0
    set +o errexit

    for file in $(find -L "${__APP__}/Plugin/LibricielBootstrap3" -type f \( -name "*.php" -or -name "*.ctp" \) -not -path "*/docker/*" | sort -t '/') ; do
        php -l "${file}"
        code=$?
        if [ $code -ne 0 ] ; then
            return=$code
        fi
    done

    set -o errexit
    return $return
}

lint_po()
{
    local code=0
    local return=0
    set +o errexit

    for file in $(find -L "${__APP__}/Plugin/LibricielBootstrap3" -type f -name "*.po" -not -path "*/docker/*" | sort -t '/') ; do
        echo "${file}"
        msgfmt -v -c -o /dev/null "${file}"
        code=$?
        if [ $code -ne 0 ] ; then
            return=$code
        fi
    done

    set -o errexit
    return $return
}

lint_sh()
{
    local code=0
    local return=0
    set +o errexit

    for file in $(find -L "${__APP__}/Plugin/LibricielBootstrap3" -type f -name "*.sh" -not -path "*/docker/*" | sort -t '/') ; do
        echo "${file}"
        /bin/bash -n "${file}"
        code=$?
        if [ $code -ne 0 ] ; then
            return=$code
        fi
    done

    set -o errexit
    return $return
}

# Create and tail the log files of a CakePHP 2.x install
cake2_tail()
{
	local levels="emergency alert critical error warning notice info debug"

	cake2_check && \
		echo "Preparing to tail logs..."

	# @todo filter by user/group (apache|www-data|jenkins)
	for level in $levels ; do
		local file="app/tmp/logs/${level}.log"
		sudo touch "${file}"
		sudo chown www-data: "${file}"
	done
	tail -f app/tmp/logs/*.log

	return $?
}

phpcpd()
{
    ${__ROOT__}/vendors/bin/phpcpd \
        --fuzzy \
        --names=*.php,*.ctp \
        --exclude=docker \
        --progress \
        --no-interaction \
        --verbose \
        ${__APP__}/Plugin/LibricielBootstrap3
}

phpcbf()
{
    ${__ROOT__}/vendors/bin/phpcs \
        --config-set installed_paths ${__ROOT__}/vendors/cakephp/cakephp-codesniffer \
    && ${__ROOT__}/vendors/bin/phpcbf \
        --encoding=utf-8 \
        --extensions=php \
        --ignore=*/docker/* \
        -p \
        -s \
        --standard=${__ROOT__}/phpcs.xml \
        ${__APP__}/Plugin/LibricielBootstrap3
}

phpcs()
{
    ${__ROOT__}/vendors/bin/phpcs \
        --config-set installed_paths ${__ROOT__}/vendors/cakephp/cakephp-codesniffer \
    && ${__ROOT__}/vendors/bin/phpcs \
        --encoding=utf-8 \
        --extensions=php \
        --ignore=*/docker/* \
        -p \
        -s \
        --standard=${__ROOT__}/phpcs.xml \
        ${__APP__}/Plugin/LibricielBootstrap3
}

phpmd()
{
    ${__ROOT__}/vendors/bin/phpmd \
    ${__APP__}/Plugin/LibricielBootstrap3 \
    text \
    ${__ROOT__}/phpmd.xml \
    --exclude ${__APP__}/Test
}

pre_commit()
{
    set +o errexit
    (
        lint_php \
        && lint_sh \
        && lint_po \
        && tests "AllLibricielBootstrap3Tests" \
        && phpcpd \
        && phpcs \
        && phpmd \
    )
    code=$?
    set -o errexit

    if [ $code -ne 0 ] ; then
        >&2 echo "Erreur lors des vérifications de pré-commit ($code)"
    else
        echo "Succès lors des vérifications de pré-commit ($code)"
    fi

    return $code
}

# ----------------------------------------------------------------------------------------------------------------------
# Main function
# ----------------------------------------------------------------------------------------------------------------------

__main__()
{
    (
        opts=`getopt --longoptions filter:,help,xtrace -- hx "$@"` || ( >&2 __usage__ ; exit 1 )
        eval set -- "$opts"
        while true ; do
            case "$1" in
                --filter)
                    __FILTER__="${__FILTER__} --filter ${2}"
                    shift 2
                ;;
                -h|--help)
                    __usage__
                    exit 0
                ;;
                -x|--xtrace)
                    shift
                ;;
                --)
                    shift
                    break
                ;;
                *)
                    >&2 __usage__
                    exit 1
                ;;
            esac
        done

        case "${1:-}" in
            bake)
                shift
                bake "$@"
                exit $?
            ;;
            check)
                cake2_check
                exit $?
            ;;
            clear)
                cake2_clear
                exit $?
            ;;
            lint_css)
                lint_css
                exit $?
            ;;
            lint_js)
                lint_js
                exit $?
            ;;
            lint_php)
                lint_php
                exit $?
            ;;
            lint_po)
                lint_po
                exit $?
            ;;
            lint_sh)
                lint_sh
                exit $?
            ;;
            pre_commit)
                pre_commit
                exit $?
            ;;
            phpcbf)
                phpcbf
                exit $?
            ;;
            phpcpd)
                phpcpd
                exit $?
            ;;
            phpcs)
                phpcs
                exit $?
            ;;
            phpmd)
                phpmd
                exit $?
            ;;
            tail)
                cake2_tail
                exit $?
            ;;
            tests)
                tests "${2:-AllLibricielBootstrap3Tests}"
                exit $?
            ;;
            --)
                shift
                break
            ;;
            *)
                >&2 __usage__
                exit 1
            ;;
        esac

        exit 0
    )
}

__main__ "$@"
