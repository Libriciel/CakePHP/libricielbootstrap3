# Plugin LibricielBootstrap3 pour CakePHP 2.x

## Dépendances

- [plugin Database](https://gitlab.libriciel.fr/CakePHP/Database)
- [plugin Postgres](https://gitlab.libriciel.fr/CakePHP/Postgres)
- [plugin Translator](https://gitlab.libriciel.fr/CakePHP/Translator)

## Installation

### `app/Config/bootstrap.php`

```php
CakePlugin::load('Database', ['bootstrap' => true]);
CakePlugin::load('LibricielBootstrap3', ['bootstrap' => true]);
CakePlugin::load('Postgres');
CakePlugin::load('Translator', ['bootstrap' => true]);
```

### `app/Config/core.php`

```php
setLocale(LC_ALL, 'fra');
Configure::write( 'Config.language', 'fra' );
```

## Formatage des données

On peut formater les données des tableaux de résultats grâce à la classe `LibricielBootstrap3Data`
ou à toute autre classe implémentant l'interface `LibricielBootstrap3DataInterface`.

### Via la configuration

```php
Configure::write(
    'LibricielBootstrap3.LibricielBootstrap3TableHelper' => [
        'engine' => 'LibricielBootstrap3.LibricielBootstrap3Data'
    ]
);
```

### À la volée

```php
$this->LibricielBootstrap3Table->engine(['engine' => 'LibricielBootstrap3.LibricielBootstrap3Data']);
```


Par défaut, les 3 formats ci-dessous sont disponibles mais on peu en ajouter ou
surcharger ces formats dans le fichier `app/Config/bootstrap.php` (ou le fichier
`.inc` de l'application).

```php
Configure::write(
    'LibricielBootstrap3.LibricielBootstrap3Data.formats',
    [
        'phone' => [
            'in' => '%2d%2d%2d%2d%2d',
            'out' => '%02d %02d %02d %02d %02d'
        ],
        'siret' => [
            'in' => '%3d%3d%3d%5d',
            'out' => '%03d %03d %03d %05d'
        ],
        'vat' => [
            'in' => '%2s%2d%3d%3d%3d',
            'out' => '%2s %02d %03d %03d %03d'
        ]
    ]
);
```

Il faut spécifier le format d'entrée, `in` ([sscanf](http://php.net/sscanf)) et
le format de sortie associé, `out` ([sprintf](http://php.net/sprintf)).

Il est possible, à tout moment de configurer la classe `LibricielBootstrap3Data`.

- `LibricielBootstrap3Data::setup();`
- `LibricielBootstrap3Data::setup('LibricielBootstrap3.LibricielBootstrap3Data.formats');`
- `LibricielBootstrap3Data::setup(['phone' => ['in' => '%2d%2d%2d%2d%2d', 'out' => '%02d.%02d.%02d.%02d.%02d']);`

## Classes et icônes des boutons

Classes à ajouter pour les liens d'actions et les îcones suivant l'action du lien.
Le contrôleur et le plugin peuvent aussi être pris en compte (underscore).

### Via la configuration

```php
Configure::write(
    'LibricielBootstrap3.LibricielBootstrap3TableHelper' => [
        'classes' => [
            'table' => 'table table-hover table-striped table-condensed',
            'buttons' => [
                'add' => 'fa fa-lg fa-plus-circle',
                'view' => 'fa fa-lg fa-eye',
                'edit' => 'fa fa-lg fa-pencil',
                'delete' => 'fa fa-lg fa-trash'
            ],
            'icons' => [
                'add' => 'btn-success',
                'delete' => 'btn-danger',
                'edit' => 'btn-primary',
                'view' => 'btn-default'
            ]
        ]
    ]
);
```

### À la volée

```php
$this->LibricielBootstrap3Table->classes(['buttons' => ['groups:view' => 'fa fa-lg fa-user'], 'icons' => ['groups:view' => 'btn-info']]);
```

Par défaut, les classes ci-dessus sont disponibles mais on peu en ajouter ou
surcharger ces classes dans le fichier `app/Config/bootstrap.php` (ou le fichier
`.inc` de l'application).

```php
Configure::write(
    'LibricielBootstrap3.LibricielBootstrap3TableHelper' => [
        'classes' => [
            'buttons' => [
                'groups:view' => 'fa fa-lg fa-user'
            ],
            'icons' => [
                'groups:view' => 'btn-info'
            ]
        ]
    ]
);
```

## Génération du code

Par exemple, pour générer tout le code à partir des noms de modèles _Group_, _User_,
_Tag_ et _Post_, on écrira:

```bash
plugins/LibricielBootstrap3/cake_utils.sh cake_utils.sh bake Group User Tag Post
```

### Attention

##### Perte de données

Normalement, une confirmation est demandée, mais il y a toujours le risque d'écraser le contenu de fichiers.

#### Dépendances

L'ordre dans lequel on donne les noms de modèles a de l'importance: un nom de modèle
devra succéder à ceux dont il dépend.

Par exemple, _User_ `belongsTo` _Group_, donc la table `users` a une clé étrangère
__vers__ la table `groups`, l'ordre doit donc être: `cake_utils.sh bake __Group__ __User__`.

## Le script `cake_utils.sh`

```bash
NAME
  cake_utils.sh

DESCRIPTION
  Commandes courantes pour un projet CakePHP 2.x chez Libriciel SCOP"

SYNOPSIS
  cake_utils.sh [OPTION] [COMMAND]

COMMANDS
  bake  Génère le code CakePHP 2.x à partir des noms de modèle
  check Vérifie que l'on soit bien à la racine d'un projet CakePHP 2.x
  clear Vérifie l'installation (commande check) et nettoie le cache d'un projet CakePHP 2.x
  tail  Tail les fichiers de log de CakePHP 2.x (et les crée si besoin)
  tests Effectue les tests unitaires du plugin LibricielBootstrap3

OPTIONS
  -h    Affiche cette aide

EXEMPLES
  cake_utils.sh -h
  cake_utils.sh clear
  cake_utils.sh bake Group User Tag Post
  cake_utils.sh tests
```