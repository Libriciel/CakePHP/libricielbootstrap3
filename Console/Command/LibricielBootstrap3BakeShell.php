<?php
App::uses('BakeShell', 'Console/Command');

class LibricielBootstrap3BakeShell extends BakeShell
{
    protected function _getConfirmation($prompt, $default = 'n')
    {
        $accepted = ['y', 'n'];
        $choice = null;
        while (in_array($choice, $accepted, true) === false) {
            $choice = $this->in($prompt, $accepted, $default);
        }

        return $choice;
    }

    protected function _confirmFilesOverwrite(array $paths)
    {
        foreach ($paths as $key => $path) {
            $file = new File($path);
            if ($file->exists() === false) {
                unset($paths[$key]);
            }
        }

        if (empty($paths) === false) {
            $this->out(sprintf('<warning>%s</warning>', __d('cake_console', 'WARNING: the following files already exist and will be overwritten:')));
            foreach ($paths as $path) {
                $this->out(sprintf("\t- %s", preg_replace('/^' . preg_quote(rtrim(ROOT, DS) . DS, DS) . '/', '', $path)));
            }

            $choice = $this->_getConfirmation(__d('cake_console', 'Overwrite ?'));
            if ($choice === 'n') {
                return false;
            }
        }

        return true;
    }

    protected function _writeLocaleFile($language, $fileName, $messages)
    {
        $success = true;

        if (empty($messages) === false) {
            // @todo: Plural-Forms en fonction de la langue
            $header = <<<'EOD'
#
msgid ""
msgstr ""
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n>1);\n"

# ==============================================================================

EOD;
            $success = $this->createFile($fileName, $header . "\n" . $messages) !== false;
        }

        return $success;
    }

    protected function _generateControllerLocaleFile($language, $fileName, $model)
    {
        $controllerName = Inflector::pluralize($model->name);
        $messages = '';
        $actions = [
            'add',
            'delete' => ['confirm' => true],
            'edit',
            'view',
        ];

        foreach (Hash::normalize($actions) as $actionName => $params) {
            $defaults = ['title' => true, 'confirm' => false];
            $params = (array)$params + $defaults;
            $prefixes = [null];
            foreach (array_keys($defaults) as $key) {
                if ($params[$key] === true) {
                    $prefixes[] = $key;
                }
            }
            foreach ($prefixes as $prefix) {
                if ($actionName === 'add') {
                    $msgid = sprintf("/%s/%s%s", $controllerName, $actionName, $prefix === null ? null : ":{$prefix}");
                } else {
                    $msgid = sprintf("/%s/%s/#%s.%s#%s", $controllerName, $actionName, $model->name, $model->primaryKey, $prefix === null ? null : ":{$prefix}");
                }
                $messages .= sprintf("msgid \"%s\"\nmsgstr \"\"\n\n", $msgid);
            }
        }

        return $this->_writeLocaleFile($language, $fileName, $messages);
    }

    protected function _generateModelLocaleFile($language, $fileName, $model)
    {
        $messages = '';
        $fields = $model->schema();
        foreach (array_keys($fields) as $fieldName) {
            $messages .= sprintf("msgid \"%s.%s\"\nmsgstr \"\"\n\n", $model->name, $fieldName);
        }

        return $this->_writeLocaleFile($language, $fileName, $messages);
    }

    protected function _generateLocaleFiles($modelName)
    {
        $language = Configure::read('Config.language');
        $localeDir = APP . 'Locale' . DS . $language . DS . 'LC_MESSAGES' . DS;

        App::uses('AppModel', 'Model');
        App::uses($modelName, 'Model');
        if (class_exists($modelName)) {
            $model = new $modelName();
        } else {
            $model = new Model(['name' => $modelName, 'ds' => $this->connection]);
        }

        $controllerName = Inflector::pluralize($model->name);
        $paths = [
            'controller' => $localeDir . Inflector::underscore($controllerName) . '.po',
            'model' => $localeDir . Inflector::underscore($model->name) . '.po',
        ];

        if ($this->_confirmFilesOverwrite($paths) === true) {
            $this->_generateModelLocaleFile($language, $paths['model'], $model);
            $this->_generateControllerLocaleFile($language, $paths['controller'], $model);
        }
    }

    public function all()
    {
        $names = ['Model' => null, 'Controller' => null, 'View' => null];
        if (empty($this->args) === false && empty($this->args[0]) === false) {
            $modelName = $this->_modelName($this->args[0]);
        }

        if (empty($modelName) === false) {
            $this->_generateLocaleFiles($modelName);

            $paths = [
                APP . 'Model' . DS . $modelName . '.php',
                APP . 'Controller' . DS . Inflector::pluralize($modelName) . 'Controller.php',
                APP . 'View' . DS . Inflector::pluralize($modelName) . DS . 'add.ctp',
                APP . 'View' . DS . Inflector::pluralize($modelName) . DS . 'edit.ctp',
                APP . 'View' . DS . Inflector::pluralize($modelName) . DS . 'index.ctp',
                APP . 'View' . DS . Inflector::pluralize($modelName) . DS . 'view.ctp',
            ];

            if ($this->_confirmFilesOverwrite($paths) !== true) {
                $this->_stop();
            }
        }

        // @fixme: _stop... donc on ne peut pas supprimer la vue add depuis le shell
        return parent::all();
    }
}
