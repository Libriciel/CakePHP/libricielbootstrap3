<?php $controller = Inflector::camelize($pluralVar); ?>
<h1><?php echo "<?php echo __('{$pluralHumanName}'); ?>"; ?></h1>
<?php echo "<?php\n";?>
    echo $this->LibricielBootstrap3->actions(
        [
            '/<?php echo $controller;?>/add' => [
                'title' => true
            ]
        ]
    );

    echo $this->LibricielBootstrap3->index(
        <?php echo "\${$pluralVar}";?>,
        [<?php
                foreach ($fields as $field) {
                    $isKey = false;
                    if (!empty($associations['belongsTo'])) {
                        foreach ($associations['belongsTo'] as $alias => $details) {
                            if ($field === $details['foreignKey']) {
                                $isKey = true;
                                echo "\n\t\t\t'{$alias}.{$details['displayField']}',";
                                break;
                            }
                        }
                    }
                    if ($isKey !== true) {
                        echo "\n\t\t\t'{$modelClass}.{$field}',";
                    }
                }
                echo "\n\t\t\t";
            ?>'/<?php echo $controller;?>/view/#<?php echo "{$modelClass}.{$primaryKey}";?>#' => [
                'title' => true
            ],
            '/<?php echo $controller;?>/edit/#<?php echo "{$modelClass}.{$primaryKey}";?>#' => [
                'title' => true
            ],
            '/<?php echo $controller;?>/delete/#<?php echo "{$modelClass}.{$primaryKey}";?>#' => [
                'title' => true,
                'confirm' => true,
                'type' => 'postLink',
                'btn-group' => 'sensitive'
            ]
        ],
        [
            'group-actions' => true
        ]
    );
<?php echo "?>\n";?>
