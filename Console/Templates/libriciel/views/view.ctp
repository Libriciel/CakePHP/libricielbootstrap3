<h1><?php echo "<?php echo __('{$singularHumanName}'); ?>"; ?></h1>
<?php
    echo "<?php\n";
    echo "echo \$this->LibricielBootstrap3->view(\n";
    echo "\t\${$singularVar},\n";
    echo "\t[\n";
    foreach ($fields as $field) {
        $isKey = false;
        if (!empty($associations['belongsTo'])) {
            foreach ($associations['belongsTo'] as $alias => $details) {
                if ($field === $details['foreignKey']) {
                    $isKey = true;
                    echo "\t\t'{$alias}.{$details['displayField']}',\n";
                    break;
                }
            }
        }
        if ($isKey !== true) {
            echo "\t\t'{$modelClass}.{$field}',\n";
        }
    }
    echo "\t]\n";
    echo ");\n";
?>