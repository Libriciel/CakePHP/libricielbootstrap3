<h1><?php echo "<?php echo __('{$singularHumanName}'); ?>"; ?></h1>
<?php echo "<?php\n";?>
    echo $this->LibricielBootstrap3->form(
        [<?php
                foreach ($fields as $field) {
                    if (in_array($field, array('created', 'modified', 'updated')) === false) {
                        $isKey = false;
                        if (!empty($associations['belongsTo'])) {
                            foreach ($associations['belongsTo'] as $alias => $details) {
                                if ($field === $details['foreignKey']) {
                                    $isKey = true;
                                    echo "\n\t\t\t'{$modelClass}.{$field}' => [";
                                    echo "\n\t\t\t\t'empty' => true,";
                                    echo "\n\t\t\t\t'options' => \$options['$modelClass']['$field']";
                                    echo "\n\t\t\t],";
                                    break;
                                }
                            }
                        }
                        if ($isKey !== true) {
                            echo "\n\t\t\t'{$modelClass}.{$field}',";
                        }
                    }
                }
                // HABTM
                if (!empty($associations['hasAndBelongsToMany'])) {
                    foreach ($associations['hasAndBelongsToMany'] as $assocName => $assocData) {
                        echo "\n\t\t\t'{$assocName}.{$assocName}' => [";
                        echo "\n\t\t\t\t'options' => \$options['$assocName']['$assocName']";
                        echo "\n\t\t\t],";
                    }
                }
                echo "\n";
            ?>
        ]
    );
<?php echo "?>\n";?>
