    /**
     * <?php echo $admin ?>index method
     *
     * @return void
     */
	public function <?php echo $admin ?>index()
    {
        $query = [
            'contain' => [
<?php
    foreach (['belongsTo'] as $assoc):
        foreach ($modelObj->{$assoc} as $associationName => $relation):
            if (!empty($associationName)):
                $otherModelName = $this->_modelName($associationName);
                echo "\t\t\t\t\"{$otherModelName}.{\$this->{$currentModelName}->{$otherModelName}->primaryKey}\",\n";
                echo "\t\t\t\t\"{$otherModelName}.{\$this->{$currentModelName}->{$otherModelName}->displayField}\",\n";
             endif;
         endforeach;
    endforeach;
?>
            ]
        ];
        $this->Paginator->settings = $query;
        $<?php echo $pluralName ?> = $this->Paginator->paginate();
		$this->set(compact('<?php echo $pluralName ?>'));
	}

    /**
     * <?php echo $admin ?>view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
	public function <?php echo $admin ?>view($id = null)
    {
		$query = [
            'contain' => [
<?php
    foreach (['belongsTo'] as $assoc):
        foreach ($modelObj->{$assoc} as $associationName => $relation):
            if (!empty($associationName)):
                $otherModelName = $this->_modelName($associationName);
                echo "\t\t\t\t\"{$otherModelName}.{\$this->{$currentModelName}->{$otherModelName}->primaryKey}\",\n";
                echo "\t\t\t\t\"{$otherModelName}.{\$this->{$currentModelName}->{$otherModelName}->displayField}\",\n";
             endif;
         endforeach;
    endforeach;
?>
            ],
            'conditions' => [
                "<?php echo $currentModelName; ?>.{$this-><?php echo $currentModelName; ?>->primaryKey}" => $id
            ]
        ];
        $<?php echo $singularName; ?> = $this-><?php echo $currentModelName; ?>->find('first', $query);

		if (empty($<?php echo $singularName; ?>) === true) {
			throw new NotFoundException(__('Invalid <?php echo strtolower($singularHumanName); ?>'));
		}
		$this->set(compact('<?php echo $singularName; ?>'));
	}

<?php $compact = []; ?>
    /**
     * <?php echo $admin ?>add method
     *
     * @return void
     */
	public function <?php echo $admin ?>add()
    {
		$this->edit();
	}

<?php $compact = []; ?>
    /**
     * <?php echo $admin ?>edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
	public function <?php echo $admin; ?>edit($id = null)
    {
        $action = $this->request->params['action'];
		if ($action === 'edit' && $this-><?php echo $currentModelName; ?>->exists($id) === false) {
			throw new NotFoundException(__('Invalid <?php echo strtolower($singularHumanName); ?>'));
		}

		if ($this->request->is($action === 'add' ? 'post' : ['post', 'put'])) {
            if ($this->request->data['_action'] === 'Cancel') {
                return $this->redirect(['action' => 'index']);
            }

            $this-><?php echo $currentModelName; ?>->create($this->request->data);
			if ($this-><?php echo $currentModelName; ?>->save()) {
<?php if ($wannaUseSession): ?>
				$this->Flash->success(__('The <?php echo strtolower($singularHumanName); ?> has been saved.'));
				return $this->redirect(['action' => 'index']);
<?php else: ?>
				return $this->flash(__('The <?php echo strtolower($singularHumanName); ?> has been saved.'), ['action' => 'index']);
<?php endif; ?>
			} else {
<?php if ($wannaUseSession): ?>
				$this->Flash->error(__('The <?php echo strtolower($singularHumanName); ?> could not be saved. Please, try again.'));
<?php else: ?>
				$this->flash(__('The <?php echo strtolower($singularHumanName); ?> could not be saved. Please, try again.'));
<?php endif; ?>
			}
		} elseif ($action === 'edit') {
            $query = [
                'conditions' => [
                    "<?php echo $currentModelName; ?>.{$this-><?php echo $currentModelName; ?>->primaryKey}" => $id
                ]
            ];
			$this->request->data = $this-><?php echo $currentModelName; ?>->find('first', $query);
		}
<?php
    foreach (['belongsTo', 'hasAndBelongsToMany'] as $assoc):
        foreach ($modelObj->{$assoc} as $associationName => $relation):
            if (!empty($associationName)):
                $otherModelName = $this->_modelName($associationName);
                if ($assoc === 'belongsTo'):
                    $compact[] = "\t\t\$options['{$currentModelName}']['{$relation['foreignKey']}'] = \$this->{$currentModelName}->{$otherModelName}->find('list');\n";
                else:
                    $compact[] = "\t\t\$options['{$otherModelName}']['{$otherModelName}'] = \$this->{$currentModelName}->{$otherModelName}->find('list');\n";
                endif;
            endif;
        endforeach;
    endforeach;
    if (!empty($compact)):
        echo "\t\t\$options = [];\n";
        echo implode($compact);
        echo "\t\t\$this->set(compact('options'));\n";
    endif;
?>
        $this->render('edit');
	}

    /**
     * <?php echo $admin ?>delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
	public function <?php echo $admin; ?>delete($id = null)
    {
		if ($this-><?php echo $currentModelName; ?>->exists($id) === false) {
			throw new NotFoundException(__('Invalid <?php echo strtolower($singularHumanName); ?>'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this-><?php echo $currentModelName; ?>->delete($id) === true) {
<?php if ($wannaUseSession): ?>
			$this->Flash->success(__('The <?php echo strtolower($singularHumanName); ?> has been deleted.'));
		} else {
			$this->Flash->error(__('The <?php echo strtolower($singularHumanName); ?> could not be deleted. Please, try again.'));
		}
		return $this->redirect(['action' => 'index']);
<?php else: ?>
			return $this->flash(__('The <?php echo strtolower($singularHumanName); ?> has been deleted.'), ['action' => 'index']);
		} else {
			return $this->flash(__('The <?php echo strtolower($singularHumanName); ?> could not be deleted. Please, try again.'), ['action' => 'index']);
		}
<?php endif; ?>
	}
