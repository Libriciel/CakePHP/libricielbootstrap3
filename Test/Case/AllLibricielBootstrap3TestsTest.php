<?php
    /**
     * AllLibricielBootstrap3Tests file
     *
     * PHP 5.3
     *
     * @package LibricielBootstrap3
     * @subpackage Test.Case
     */
    CakePlugin::load('Translator');

/**
 * AllLibricielBootstrap3Tests class
 *
 * This test group will run all tests.
 *
 * @package LibricielBootstrap3
 * @subpackage Test.Case
 */
class AllLibricielBootstrap3Tests extends PHPUnit_Framework_TestSuite
{
    /**
     * Test suite with all test case files.
     */
    public static function suite()
    {
        $suite = new CakeTestSuite('All LibricielBootstrap3 tests');
        $suite->addTestDirectoryRecursive(dirname(__FILE__) . DS . '..' . DS . 'Case' . DS);

        return $suite;
    }
}
