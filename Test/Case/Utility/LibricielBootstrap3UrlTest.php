<?php
/**
 * Code source de la classe LibricielBootstrap3UrlTest.
 *
 * @package LibricielBootstrap3
 * @subpackage Test.Case.Utility
 */
App::uses('LibricielBootstrap3Url', 'LibricielBootstrap3.Utility');

/**
 * La classe DataTest ...
 *
 * @covers LibricielBootstrap3Url
 *
 * @package LibricielBootstrap3
 * @subpackage Test.Case.Utility
 */
class LibricielBootstrap3UrlTest extends CakeTestCase
{
    /**
     * Test de la méthode LibricielBootstrap3Url::parse().
     */
    public function testParse()
    {
        $result = LibricielBootstrap3Url::parse('/Foos/bar');
        $expected = [
            'plugin' => null,
            'controller' => 'foos',
            'action' => 'bar',
            'named' => [],
        ];
        $this->assertEquals($expected, $result);
    }

    /**
     * Test de la méthode LibricielBootstrap3Url::parse().
     */
    public function testParseHash()
    {
        $result = LibricielBootstrap3Url::parse('/Users/edit/123#main');
        $expected = [
            'plugin' => null,
            'controller' => 'users',
            'action' => 'edit',
            123,
            'named' => [],
            '#' => 'main',
        ];
        $this->assertEquals($expected, $result);
    }

    /**
     * Test de la méthode LibricielBootstrap3Url::parse().
     */
    public function testParsePlugin()
    {
        $result = LibricielBootstrap3Url::parse('/libriciel_bootstrap3/Users/edit/123');
        $expected = [
            'plugin' => 'libriciel_bootstrap3',
            'controller' => 'users',
            'action' => 'edit',
            '123',
            'named' => [],
        ];
        $this->assertEquals($expected, $result);
    }

    /**
     * Test de la méthode LibricielBootstrap3Url::parse() avec une chaîne de
     * caractères qui n'est pas convertie.
     */
    public function testParseUnknownString()
    {
        $result = LibricielBootstrap3Url::parse('foos/bar');
        $this->assertEquals('foos/bar', $result);
    }

    /**
     * Test de la méthode LibricielBootstrap3Url::parse() avec un array.
     */
    public function testParseArray()
    {
        $url = ['controller' => 'foos', 'acion' => 'bar'];
        $result = LibricielBootstrap3Url::parse($url);
        $this->assertEquals($url, $result);
    }
}
