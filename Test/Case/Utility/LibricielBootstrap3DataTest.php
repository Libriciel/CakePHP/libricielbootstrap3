<?php
/**
 * Code source de la classe LibricielBootstrap3DataTest.
 *
 * @package LibricielBootstrap3
 * @subpackage Test.Case.Utility
 */
App::uses('LibricielBootstrap3AbstractTestCase', 'LibricielBootstrap3.Test/Case');
App::uses('LibricielBootstrap3Data', 'LibricielBootstrap3.Utility');

/**
 * La classe DataTest ...
 *
 * @covers LibricielBootstrap3Data
 *
 * @package LibricielBootstrap3
 * @subpackage Test.Case.Utility
 */
class LibricielBootstrap3DataTest extends LibricielBootstrap3AbstractTestCase
{
    /**
     * Test de la méthode LibricielBootstrap3Data::biginteger().
     */
    public function testBiginteger()
    {
        $result = LibricielBootstrap3Data::biginteger(123456789.123);
        $this->assertEquals('123 456 789', $result);
    }

    /**
     * Test de la méthode LibricielBootstrap3Data::boolean().
     */
    public function testBoolean()
    {
        $this->assertEquals('Oui', LibricielBootstrap3Data::boolean(true));
        $this->assertEquals('Non', LibricielBootstrap3Data::boolean(false));
    }

    /**
     * Test de la méthode LibricielBootstrap3Data::date().
     */
    public function testDate()
    {
        $result = LibricielBootstrap3Data::date('2018-06-28 01:09:15');
        $this->assertEquals('28/06/2018', $result);
    }

    /**
     * Test de la méthode LibricielBootstrap3Data::datetime().
     */
    public function testDatetime()
    {
        $result = LibricielBootstrap3Data::datetime('2018-06-28 01:09:15');
        $this->assertEquals('28/06/2018 à 01:09:15', $result);
    }

    /**
     * Test de la méthode LibricielBootstrap3Data::decimal().
     */
    public function testDecimal()
    {
        $result = LibricielBootstrap3Data::decimal(123456.789);
        $this->assertEquals('123 456,79', $result);
    }

    /**
     * Test de la méthode LibricielBootstrap3Data::float().
     */
    public function testFloat()
    {
        $result = LibricielBootstrap3Data::float(123456.789);
        $this->assertEquals('123 456,79', $result);
    }

    /**
     * Test de la méthode LibricielBootstrap3Data::integer().
     */
    public function testInteger()
    {
        $result = LibricielBootstrap3Data::integer(123456789.123);
        $this->assertEquals('123 456 789', $result);
    }

    /**
     * Test de la méthode LibricielBootstrap3Data::evaluate().
     */
    public function testEvaluateString()
    {
        $data = [
            'Group' => [
                'id' => 7,
                'name' => 'Administrateurs',
            ],
        ];
        $msgstr = 'Réellement supprimer le group « #Group.name# » (##Group.id#) ?';
        $result = LibricielBootstrap3Data::evaluate($data, $msgstr);
        $expected = 'Réellement supprimer le group « Administrateurs » (#7) ?';
        $this->assertEquals($expected, $result);
    }

    /**
     * Test de la méthode LibricielBootstrap3Data::evaluate().
     */
    public function testEvaluateArray()
    {
        // 1°
        $data = [
            'Group' => [
                'id' => 7,
                'name' => 'Administrateurs',
                'description' => 'Les Dieux',
                'string' => 'D\'"anges" ?',
            ],
        ];
        $messages = [
            'Description: "#Group.description#"',
            'Description: \'#Group.description#\'',
            '/Groups/index/#Group.id##foo',
            '#Group.string#',
            '#Group.id#' => 'Réellement supprimer le group « #Group.name# » ?',
        ];
        $result = LibricielBootstrap3Data::evaluate($data, $messages);
        $expected = [
            'Description: "Les Dieux"',
            'Description: \'Les Dieux\'',
            '/Groups/index/7#foo',
            'D\'"anges" ?',
            '7' => 'Réellement supprimer le group « Administrateurs » ?',
        ];
        $this->assertEquals($expected, $result);

        // 2°
        $data = [
            'Address' => [
                'id' => 1,
                'name' => 'Résidence "Les terrasses du Peyrou"',
                'description' => 'Là-bas s\'exprime toute la séduction Montpelliéraine.',
            ],
        ];

        $result = LibricielBootstrap3Data::evaluate($data, '\'#Address.name#\' == \'Résidence "Les terrasses du Peyrou"\'');
        $expected = '\'Résidence "Les terrasses du Peyrou"\' == \'Résidence "Les terrasses du Peyrou"\'';
        $this->assertEquals($expected, $result, $result);

        // 3°
        $result = LibricielBootstrap3Data::evaluate($data, '"#Address.description#" == "Là-bas s\'exprime toute la séduction Montpelliéraine."');
        $expected = '"Là-bas s\'exprime toute la séduction Montpelliéraine." == "Là-bas s\'exprime toute la séduction Montpelliéraine."';
        $this->assertEquals($expected, $result, $result);
    }

    /**
     * Test de la méthode LibricielBootstrap3Data::time().
     */
    public function testTime()
    {
        $result = LibricielBootstrap3Data::time('2018-06-28 01:09:15');
        $this->assertEquals('01:09:15', $result);
    }

    /**
     * Test de la méthode LibricielBootstrap3Data::format() pour les numéros de
     * téléphone.
     */
    public function testFormatPhone()
    {
        $result = LibricielBootstrap3Data::format('0467659644', 'phone');
        $this->assertEquals('04 67 65 96 44', $result);
    }

    /**
     * Test de la méthode LibricielBootstrap3Data::format() pour les numéros de
     * téléphone avec une valeur erronée.
     */
    public function testFormatPhoneWithError()
    {
        $result = LibricielBootstrap3Data::format('foo', 'phone');
        $this->assertEquals('foo', $result);
    }

    /**
     * Test de la méthode LibricielBootstrap3Data::format() pour les numéros de
     * SIRET avec une valeur erronée.
     */
    public function testFormatSiretWithError()
    {
        $result = LibricielBootstrap3Data::format('123456', 'siret');
        $this->assertEquals('123456', $result);
    }

    /**
     * Test de la méthode LibricielBootstrap3Data::format() pour les numéros de
     * SIRET.
     */
    public function testFormatSiret()
    {
        $result = LibricielBootstrap3Data::format('49101169800025', 'siret');
        $this->assertEquals('491 011 698 00025', $result);
    }

    /**
     * Test de la méthode LibricielBootstrap3Data::format() pour les numéros de
     * TVA.
     */
    public function testFormatVat()
    {
        $result = LibricielBootstrap3Data::format('FR76491011698', 'vat');
        $this->assertEquals('FR 76 491 011 698', $result);
    }

    /**
     * Test de la méthode LibricielBootstrap3Data::format() pour les numéros de
     * téléphone avec un reparamétrage via Configure::write.
     */
    public function testFormatPhoneAnotherConfigWithConfigureWrite()
    {
        $config = [
            'phone' => [
                'in' => '%2d%2d%2d%2d%2d',
                'out' => '%02d.%02d.%02d.%02d.%02d',
            ],
        ];
        Configure::write('LibricielBootstrap3.LibricielBootstrap3Data.formats', $config);
        LibricielBootstrap3Data::setup();

        $result = LibricielBootstrap3Data::format('0467659644', 'phone');
        $this->assertEquals('04.67.65.96.44', $result);
    }

    /**
     * Test de la méthode LibricielBootstrap3Data::format() pour les numéros de
     * téléphone avec un reparamétrage via .
     */
    public function testFormatPhoneAnotherConfigWithSetup()
    {
        $config = [
            'phone' => [
                'in' => '%2d%2d%2d%2d%2d',
                'out' => '%02d.%02d.%02d.%02d.%02d',
            ],
        ];
        LibricielBootstrap3Data::setup($config);

        $result = LibricielBootstrap3Data::format('0467659644', 'phone');
        $this->assertEquals('04.67.65.96.44', $result);
    }

    /**
     * Test de la méthode LibricielBootstrap3Data::format() avec une erreur car
     * le nom du format n'existe pas.
     *
     * @expectedException        PHPUnit_Framework_Error_Notice
     * @todo: count(): Parameter must be an array or an object that implements Countable
     * @ expectedExceptionMessage LibricielBootstrap3Data::format, unknown format parameter "foo".
     */
    public function testFormatWithUnknownFormat()
    {
        LibricielBootstrap3Data::format('0467659644', 'foo');
    }
}
