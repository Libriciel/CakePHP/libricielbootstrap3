<?php
App::uses('CakeRequest', 'Network');

abstract class LibricielBootstrap3AbstractTestCase extends CakeTestCase
{
    public static $requestsParams = [
        'page_1_of_1' => [
            'paging' => [
                'Apple' => [
                    'page' => 1,
                    'current' => 2,
                    'count' => 2,
                    'prevPage' => false,
                    'nextPage' => false,
                    'pageCount' => 1,
                    'order' => null,
                    'limit' => 20,
                    'options' => [
                        'page' => 1,
                        'conditions' => [ ],
                    ],
                    'paramType' => 'named',
                ],
            ],
            'controller' => 'apples',
            'action' => 'index',
        ],
        'page_2_of_7' => [
            'paging' => [
                'Apple' => [
                    'page' => 2,
                    'current' => 9,
                    'count' => 62,
                    'prevPage' => false,
                    'nextPage' => true,
                    'pageCount' => 7,
                    'order' => null,
                    'limit' => 20,
                    'options' => [
                        'page' => 1,
                        'conditions' => [ ],
                    ],
                    'paramType' => 'named',
                ],
            ],
            'controller' => 'apples',
            'action' => 'index',
        ],
    ];

    /**
     * Préparation du test.
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();

        //--------------------------------------------------------------------------------------------------------------
        Cache::clear();
        setLocale(LC_ALL, 'fra');
        Configure::write('Config.language', 'fra');

        $testAppDir = CakePlugin::path('LibricielBootstrap3') . 'Test' . DS . 'test_app';
        App::build(
            [
                'Locale' => [$testAppDir . DS . 'Locale' . DS],
                'Model' => [$testAppDir . DS . 'Model' . DS],
            ],
            App::RESET
        );
        //--------------------------------------------------------------------------------------------------------------

        Configure::write(
            'LibricielBootstrap3',
            [
                'LibricielBootstrap3Data' => [
                    'formats' => [
                        'phone' => [
                            'in' => '%2d%2d%2d%2d%2d',
                            'out' => '%02d %02d %02d %02d %02d',
                        ],
                        'siret' => [
                            'in' => '%3d%3d%3d%5d',
                            'out' => '%03d %03d %03d %05d',
                        ],
                        'vat' => [
                            'in' => '%2s%2d%3d%3d%3d',
                            'out' => '%2s %02d %03d %03d %03d',
                        ],
                    ],
                ],
                'LibricielBootstrap3TableHelper' => [
                    'engine' => 'LibricielBootstrap3.LibricielBootstrap3Data',
                    'actions' => [
                        'table' => 'table table-hover table-striped table-condensed',
                        'buttons' => [
                            'add' => 'fa fa-lg fa-plus-circle',
                            'view' => 'fa fa-lg fa-eye',
                            'edit' => 'fa fa-lg fa-pencil',
                            'delete' => 'fa fa-lg fa-trash',
                        ],
                        'icons' => [
                            'add' => 'btn-success',
                            'delete' => 'btn-danger',
                            'edit' => 'btn-primary',
                            'view' => 'btn-default',
                        ],
                    ],
                ],
            ]
        );

        LibricielBootstrap3Data::setup(Configure::read('LibricielBootstrap3.LibricielBootstrap3Data.formats'));
    }

    /**
     * Definit une url fictive
     *
     * @param array $params
     * @return CakeRequest
     */
    protected function _getCakeRequest(array $params = [])
    {
        $default = [
            'plugin' => null,
            'controller' => 'apples',
            'action' => 'index',
        ];

        $params = Hash::merge($default, $params);
        $domains = [
            ltrim("{$params['plugin']}_{$params['controller']}_{$params['action']}", "_"),
            "{$params['controller']}_{$params['action']}",
            ltrim("{$params['plugin']}_{$params['controller']}", "_"),
            $params['controller'],
            'default',
        ];
        Translator::domains(array_unique($domains));

        Router::reload();
        $request = new CakeRequest();

        $request->addParams($params);

        Router::setRequestInfo($request);

        return $request;
    }

    protected static function _normalizeXhtml($xhtml)
    {
        $xhtml = preg_replace("/([[:space:]]|\n)+/m", ' ', $xhtml);
        $xhtml = str_replace('> <', '><', $xhtml);

        return trim($xhtml);
    }

    public static function assertEquals($expected, $result, $message = '', $delta = 0, $maxDepth = 10, $canonicalize = false, $ignoreCase = false)
    {
        if (empty($message)) {
            $message = var_export($result, true);
        }

        return parent::assertEquals(
            $expected,
            $result,
            $message,
            $delta,
            $maxDepth,
            $canonicalize,
            $ignoreCase
        );
    }

    public static function assertEqualsXhtml($expected, $result, $message = '', $delta = 0, $maxDepth = 10, $canonicalize = false, $ignoreCase = false)
    {
        return self::assertEquals(
            self::_normalizeXhtml($expected),
            self::_normalizeXhtml($result),
            $message,
            $delta,
            $maxDepth,
            $canonicalize,
            $ignoreCase
        );
    }

    public static function cakephpDateOptionsDay($current = null)
    {
        $current = $current ?? (int)date('j');
        $result = '';

        for ($day = 1; $day <= 31; $day++) {
            $selected = $current === $day ? ' selected="selected"' : '';
            $result .= sprintf("<option value=\"%02d\"%s>%d</option>\n", $day, $selected, $day);
        }

        return $result;
    }

    public static function cakephpDateOptionsMonth($current = null)
    {
        $current = $current ?? (int)date('n');
        $result = '';

        for ($month = 1; $month <= 12; $month++) {
            $selected = $current === $month ? ' selected="selected"' : '';
            $label = __d('cake', date('F', strtotime(sprintf("1970-%02d-01", $month))));
            $result .= sprintf("<option value=\"%02d\"%s>%s</option>\n", $month, $selected, $label);
        }

        return $result;
    }

    public static function cakephpDateOptionsYear($start = null, $stop = null, $current = null)
    {
        $start = $start ?? (int)date('Y') - 20;
        $stop = $stop ?? (int)date('Y') + 20;
        $current = $current ?? (int)date('Y');
        $result = '';

        for ($year = $stop; $year >= $start; $year--) {
            $selected = $current === $year ? ' selected="selected"' : '';
            $result .= sprintf("<option value=\"%02d\"%s>%d</option>\n", $year, $selected, $year);
        }

        return $result;
    }

    public static function cakephpTimeOptionsHour($current = null)
    {
        $current = $current ?? (int)date('H');
        $result = '';

        for ($hour = 0; $hour <= 23; $hour++) {
            $selected = $current === $hour ? ' selected="selected"' : '';
            $result .= sprintf("<option value=\"%02d\"%s>%d</option>\n", $hour, $selected, $hour);
        }

        return $result;
    }

    public static function cakephpTimeOptionsMinute($current = null, $interval = 1)
    {
        $current = $current ?? (int)ltrim(date('i'), '0');
        $result = '';

        for ($hour = 0; $hour <= 59; $hour += $interval) {
            $selected = $current === $hour ? ' selected="selected"' : '';
            $result .= sprintf("<option value=\"%02d\"%s>%02d</option>\n", $hour, $selected, $hour);
        }

        return $result;
    }
}
