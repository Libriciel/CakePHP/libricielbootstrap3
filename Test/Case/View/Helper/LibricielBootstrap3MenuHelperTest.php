<?php
/**
 * Code source de la classe LibricielBootstrap3MenuHelperTest.
 *
 * @package LibricielBootstrap3
 * @subpackage Test.Case.View.Helper
 */
App::uses('View', 'View');
App::uses('AppHelper', 'View/Helper');
App::uses('CakeRequest', 'Network');
App::uses('LibricielBootstrap3AbstractTestCase', 'LibricielBootstrap3.Test/Case');
App::uses('LibricielBootstrap3Data', 'LibricielBootstrap3.Utility');
App::uses('LibricielBootstrap3MenuHelper', 'LibricielBootstrap3.View/Helper');
App::uses('Translator', 'Translator.Utility');

/**
 * La classe LibricielBootstrap3MenuHelperTest ...
 *
 * @covers LibricielBootstrap3MenuHelper
 *
 * @package LibricielBootstrap3
 * @subpackage Test.Case.View.Helper
 */
class LibricielBootstrap3MenuHelperTest extends LibricielBootstrap3AbstractTestCase
{
    /**
     * Préparation du test.
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();

        $controller = null;
        $this->View = new View($controller);
        $this->LibricielBootstrap3Menu = new LibricielBootstrap3MenuHelper($this->View);

        $this->LibricielBootstrap3Menu->request = new CakeRequest('apples/index', false);
        $this->LibricielBootstrap3Menu->request->here = '/apples/index';
        $this->LibricielBootstrap3Menu->request['controller'] = 'apples';
        $this->LibricielBootstrap3Menu->request['action'] = 'index';
        $this->LibricielBootstrap3Menu->request->webroot = '';
        $this->LibricielBootstrap3Menu->request->base = '';
    }

    /**
     * Nettoyage postérieur au test.
     *
     * @return void
     */
    public function tearDown()
    {
        parent::tearDown();
        unset($this->View, $this->LibricielBootstrap3Menu, $this->DataTest, $this->Apple);
    }

    /**
     * Test de la méthode LibricielBootstrap3MenuHelper::main().
     *
     * @return void
     */
    public function testMenu()
    {
        $root = Router::url('/');

        $tree = [
            'Superadmin - Libriciel SCOP' => [
                'type' => 'text',
                'class' => 'fa-foo',
            ],
            'Administration' => [
                'This is a title',
                'Groups and users' => [
                    'class' => 'fa-user-cog',
                    'title' => 'Manage groups and users',
                    'Groups' => [
                        'disabled' => true,
                        'class' => 'fa-users',
                        'url' => '/Groups',
                    ],
                    'Users' => [
                        'data-foo' => 'bar',
                        'class' => 'fa-user',
                        'url' => '/Users',
                        'title' => 'Users list',
                    ],
                ],
                'divider',
                'Posts' => [
                    'class' => 'fa-comment',
                    'url' => '/Posts',
                    'title' => 'Posts list',
                ],
                'Tags' => [
                    'class' => 'fa-tag',
                    'url' => '/Tags',
                ],
            ],
        ];

        $actual = $this->LibricielBootstrap3Menu->main($tree);
        $expected =
        '<ul class="nav navbar-nav " role="menubar">
            <li class="navbar-text fa-foo" role="presentation">Superadmin - Libriciel SCOP</li>
            <li class="dropdown" role="none">
                <a href="#" role="menuitem" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle" data-toggle="dropdown">Administration<span class="caret"><!-- --></span></a>
                <ul class="dropdown-menu" role="menu">
                    <li class="dropdown-header" role="none">This is a title</li>
                    <li class="dropdown-submenu" role="none">
                        <a title="Manage groups and users" href="#" role="menuitem" aria-haspopup="true" aria-expanded="false">Groups and users</a>
                        <ul class="dropdown-menu" role="menu">
                            <li class="" role="none">
                                <a href="/users/index" data-foo="bar" title="Users list" role="menuitem"><span class="fa fa-fw fa-user"><!-- --></span> Users</a>
                            </li>
                        </ul>
                    </li>
                    <li class="divider" role="separator"><!-- --></li>
                    <li class="" role="none">
                        <a href="/posts/index" title="Posts list" role="menuitem"><span class="fa fa-fw fa-comment"><!-- --></span> Posts</a>
                    </li>
                    <li class="" role="none">
                        <a href="/tags/index" role="menuitem"><span class="fa fa-fw fa-tag"><!-- --></span> Tags</a>
                    </li>
                </ul>
            </li>
        </ul>';
        $this->assertEqualsXhtml($expected, $actual);
    }
}
