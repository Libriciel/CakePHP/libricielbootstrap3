<?php
/**
 * Code source de la classe LibricielBootstrap3PaginatorHelperTest.
 *
 * @package LibricielBootstrap3
 * @subpackage Test.Case.View.Helper
 */
App::uses('View', 'View');
App::uses('AppHelper', 'View/Helper');
App::uses('CakeRequest', 'Network');
App::uses('LibricielBootstrap3AbstractTestCase', 'LibricielBootstrap3.Test/Case');
App::uses('LibricielBootstrap3Data', 'LibricielBootstrap3.Utility');
App::uses('LibricielBootstrap3PaginatorHelper', 'LibricielBootstrap3.View/Helper');
App::uses('Translator', 'Translator.Utility');

/**
 * La classe LibricielBootstrap3PaginatorHelperTest ...
 *
 * @covers LibricielBootstrap3PaginatorHelper
 *
 * @package LibricielBootstrap3
 * @subpackage Test.Case.View.Helper
 */
class LibricielBootstrap3PaginatorHelperTest extends LibricielBootstrap3AbstractTestCase
{
    /**
     * Fixtures utilisés par ces tests unitaires.
     *
     * @var array
     */
    public $fixtures = [
        'core.Apple',
        'core.DataTest',
    ];

    /**
     * Modèle Apple
     *
     * @var Model
     */
    public $Apple = null;

    /**
     * Modèle DataTest
     *
     * @var Model
     */
    public $DataTest = null;

    /**
     * Definit une url fictive
     *
     * @param array $params
     */
    protected function _setRequest(array $params = [])
    {
        $request = $this->_getCakeRequest($params);
        $this->LibricielBootstrap3Paginator->request = $request;
    }

    /**
     * Préparation du test.
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();

        $this->DataTest = ClassRegistry::init('DataTest');
        $this->Apple = ClassRegistry::init('Apple');

        $controller = null;
        $this->View = new View($controller);
        $this->LibricielBootstrap3Paginator = new LibricielBootstrap3PaginatorHelper($this->View);

        $this->LibricielBootstrap3Paginator->request = new CakeRequest();
    }

    /**
     * Nettoyage postérieur au test.
     *
     * @return void
     */
    public function tearDown()
    {
        parent::tearDown();
        unset($this->View, $this->LibricielBootstrap3Paginator, $this->DataTest, $this->Apple);
    }

    /**
     * Test de la méthode LibricielBootstrap3PaginatorHelper::first().
     */
    public function testPage1Of1()
    {
        $this->_setRequest(static::$requestsParams['page_1_of_1']);

        $results = [
            '<li class="first disabled"><span>&lt;&lt; first</span></li>'
                => $this->LibricielBootstrap3Paginator->first(),
            '<li class="disabled"><span>&lt;&lt; Previous</span></li>'
                => $this->LibricielBootstrap3Paginator->prev(),
            '<li class="disabled"><span>Next &gt;&gt;</span></li>'
                => $this->LibricielBootstrap3Paginator->next(),
            '<li class="last disabled"><span>last &gt;&gt;</span></li>'
                => $this->LibricielBootstrap3Paginator->last(),
        ];

        foreach ($results as $expected => $result) {
            $this->assertEqualsXhtml($expected, $result, var_export($result, true));
        }
    }

    /**
     * Test de la méthode LibricielBootstrap3PaginatorHelper::numbers()
     * avec une seule page de résultats.
     */
    public function testNumbersPage1Of1()
    {
        $this->_setRequest(static::$requestsParams['page_1_of_1']);
        $result = $this->LibricielBootstrap3Paginator->numbers();
        $this->assertEqualsXhtml('', $result, var_export($result, true));
    }

    /**
     * Test de la méthode LibricielBootstrap3PaginatorHelper::numbers()
     * en étant sur la seconde page sur 7 pages de résultats.
     */
    public function testNumbersPage2Of7()
    {
        $this->_setRequest(static::$requestsParams['page_2_of_7']);
        $result = $this->LibricielBootstrap3Paginator->numbers();
        $root = Router::url('/');
        $expected = '<span><a href="' . $root . 'apples">1</a></span>
            | <span class="current">2</span>
            | <span><a href="' . $root . 'apples/index/page:3">3</a></span>
            | <span><a href="' . $root . 'apples/index/page:4">4</a></span>
            | <span><a href="' . $root . 'apples/index/page:5">5</a></span>
            | <span><a href="' . $root . 'apples/index/page:6">6</a></span>
            | <span><a href="' . $root . 'apples/index/page:7">7</a></span>';
        $this->assertEqualsXhtml($expected, $result, var_export($result, true));
    }

    /**
     * Test de la méthode LibricielBootstrap3PaginatorHelper::numbers()
     * en étant sur la seconde page sur 7 pages de résultats, avec les titres des
     * liens.
     */
    public function testNumbersPage2Of7WithTitles()
    {
        $this->_setRequest(static::$requestsParams['page_2_of_7']);
        $result = $this->LibricielBootstrap3Paginator->numbers(['title' => true]);
        $root = Router::url('/');
        $expected = '<span><a href="' . $root . 'apples" title="Page 1">1</a></span>
            | <span class="current">2</span>
            | <span><a href="' . $root . 'apples/index/page:3" title="Page 3">3</a></span>
            | <span><a href="' . $root . 'apples/index/page:4" title="Page 4">4</a></span>
            | <span><a href="' . $root . 'apples/index/page:5" title="Page 5">5</a></span>
            | <span><a href="' . $root . 'apples/index/page:6" title="Page 6">6</a></span>
            | <span><a href="' . $root . 'apples/index/page:7" title="Page 7">7</a></span>';
        $this->assertEqualsXhtml($expected, $result, var_export($result, true));
    }
}
