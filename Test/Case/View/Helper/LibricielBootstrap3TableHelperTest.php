<?php
/**
 * Code source de la classe LibricielBootstrap3TableHelperTest.
 *
 * @package LibricielBootstrap3
 * @subpackage Test.Case.View.Helper
 */
App::uses('View', 'View');
App::uses('AppHelper', 'View/Helper');
App::uses('LibricielBootstrap3AbstractTestCase', 'LibricielBootstrap3.Test/Case');
App::uses('LibricielBootstrap3TableHelper', 'LibricielBootstrap3.View/Helper');
App::uses('Translator', 'Translator.Utility');
App::uses('LibricielBootstrap3Data', 'LibricielBootstrap3.Utility');

class AlternateDataEngine extends LibricielBootstrap3Data
{
    public static function format($value, $format)
    {
        return parent::format($value, $format) . ' (extended)';
    }
}

/**
 * La classe LibricielBootstrap3TableHelperTest ...
 *
 * @covers LibricielBootstrap3TableHelper
 *
 * @package LibricielBootstrap3
 * @subpackage Test.Case.View.Helper
 */
class LibricielBootstrap3TableHelperTest extends LibricielBootstrap3AbstractTestCase
{
    /**
     * Fixtures utilisés par ces tests unitaires.
     *
     * @var array
     */
    public $fixtures = [
        'core.Apple',
        'core.DataTest',
    ];

    /**
     * Modèle Apple
     *
     * @var Model
     */
    public $Apple = null;

    /**
     * Modèle DataTest
     *
     * @var Model
     */
    public $DataTest = null;

    public static $options = [
        'Apple' => [
            'color' => [
                'red' => 'Rouge',
                'green' => 'Verte',
                'yellow' => 'Jaune',
            ],
        ],
    ];

    public static $records = [
        [
            'Apple' => [
                'id' => 3,
                'name' => 'Braeburn',
                'color' => 'red',
            ],
            'User' => [
                'id' => 2,
                'username' => 'cjerome',
                'phone' => '0605040102',
            ],
        ],
        [
            'Apple' => [
                'id' => 7,
                'name' => 'J\'ai des quotes "échappées"',
                'color' => 'green',
            ],
            'User' => [
                'id' => 5,
                'username' => 'cjerome',
                'phone' => null,
            ],
        ],
        [
            'User' => [
                'id' => 6,
                'username' => 'cjerome',
                'phone' => 'foo',
            ],
        ],
    ];

    /**
     * Definit une url fictive
     *
     * @param array $params
     */
    protected function _setRequest(array $params = [])
    {
        $request = $this->_getCakeRequest($params);
        $this->LibricielBootstrap3Table->request = $request;
    }

    /**
     * Préparation du test.
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();

        $this->DataTest = ClassRegistry::init('DataTest');
        $this->Apple = ClassRegistry::init('Apple');

        $controller = null;
        $this->View = new View($controller);
        $this->LibricielBootstrap3Table = new LibricielBootstrap3TableHelper($this->View);

        $this->LibricielBootstrap3Table->request = new CakeRequest();
        $this->LibricielBootstrap3Table->request->here = '/apples/add';
        $this->LibricielBootstrap3Table->request['controller'] = 'apples';
        $this->LibricielBootstrap3Table->request['action'] = 'add';
        $this->LibricielBootstrap3Table->request->webroot = '';
        $this->LibricielBootstrap3Table->request->base = '';
    }

    /**
     * Nettoyage postérieur au test.
     *
     * @return void
     */
    public function tearDown()
    {
        parent::tearDown();
        unset($this->View, $this->LibricielBootstrap3Table, $this->DataTest, $this->Apple);
    }

    /**
     * Test de la méthode LibricielBootstrap3TableHelper::table().
     */
    public function testTable()
    {
        $this->_setRequest(static::$requestsParams['page_1_of_1']);
        $root = Router::url('/');

        $fields = [
            'Apple.id' => [
                'sort' => false,
            ],
            'Apple.name',
            '/Apples/view/#Apple.id#' => [
                'title' => true,
            ],
            '/Apples/edit/#Apple.id#' => [
                'title' => true,
            ],
            '/Apples/delete/#Apple.id#' => [
                'title' => true,
                'confirm' => true,
            ],
        ];
        $result = $this->LibricielBootstrap3Table->table([static::$records[0]], $fields);
        $expected =
        '<table class="table table-hover table-striped table-condensed">
            <thead>
                <tr>
                    <th>Id</th>
                    <th><a href="' . $root . 'apples/index/sort:Apple.name/direction:asc">Variété</a></th>
                    <th colspan="3" class="actions">Actions</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="data integer positive">3</td>
                    <td class="data string">Braeburn</td>
                    <td class="action apples view">
                        <a href="' . $root . 'apples/view/3" title="/Apples/view/3:title" class="btn btn-sm btn-default controller-apples action-view"><span class="fa fa-lg fa-eye"></span><span class="text">Voir</span></a>
                    </td>
                    <td class="action apples edit">
                        <a href="' . $root . 'apples/edit/3" title="/Apples/edit/3:title" class="btn btn-sm btn-primary controller-apples action-edit"><span class="fa fa-lg fa-pencil"></span><span class="text">Modifier</span></a>
                    </td>
                    <td class="action apples delete">
                        <a href="' . $root . 'apples/delete/3" title="/Apples/delete/3:title" class="btn btn-sm btn-danger controller-apples action-delete" onclick="if (confirm(&quot;\\/Apples\\/delete\\/3:confirm&quot;)) { return true; } return false;"><span class="fa fa-lg fa-trash"></span><span class="text">Supprimer</span></a>
                    </td>
                </tr>
            </tbody>
        </table>';
        $this->assertEqualsXhtml($expected, $result, var_export($result, true));
    }

    /**
     * Test de la méthode LibricielBootstrap3TableHelper::table() avec des formats
     * pour certaines cellules.
     */
    public function testTableWithFormats()
    {
        $this->_setRequest(static::$requestsParams['page_1_of_1']);
        $root = Router::url('/');

        $fields = [
            'User.phone' => [
                'data-format' => 'phone',
            ],
            'Apple.name',
            '/Apples/view/#Apple.id#' => [
                'title' => true,
            ],
            '/Apples/edit/#Apple.id#' => [
                'title' => true,
            ],
            '/Apples/delete/#Apple.id#' => [
                'title' => true,
                'confirm' => true,
            ],
        ];
        $result = $this->LibricielBootstrap3Table->table([static::$records[0]], $fields);
        $expected =
            '<table class="table table-hover table-striped table-condensed">
            <thead>
                <tr>
                    <th><a href="' . $root . 'apples/index/sort:User.phone/direction:asc">User.phone</a></th>
                    <th><a href="' . $root . 'apples/index/sort:Apple.name/direction:asc">Variété</a></th>
                    <th colspan="3" class="actions">Actions</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="data string phone">06 05 04 01 02</td>
                    <td class="data string">Braeburn</td>
                    <td class="action apples view">
                        <a href="' . $root . 'apples/view/3" title="/Apples/view/3:title" class="btn btn-sm btn-default controller-apples action-view"><span class="fa fa-lg fa-eye"></span><span class="text">Voir</span></a>
                    </td>
                    <td class="action apples edit">
                        <a href="' . $root . 'apples/edit/3" title="/Apples/edit/3:title" class="btn btn-sm btn-primary controller-apples action-edit"><span class="fa fa-lg fa-pencil"></span><span class="text">Modifier</span></a>
                    </td>
                    <td class="action apples delete">
                        <a href="' . $root . 'apples/delete/3" title="/Apples/delete/3:title" class="btn btn-sm btn-danger controller-apples action-delete" onclick="if (confirm(&quot;\\/Apples\\/delete\\/3:confirm&quot;)) { return true; } return false;"><span class="fa fa-lg fa-trash"></span><span class="text">Supprimer</span></a>
                    </td>
                </tr>
            </tbody>
        </table>';
        $this->assertEqualsXhtml($expected, $result, var_export($result, true));
    }

    /**
     * Test de la méthode LibricielBootstrap3TableHelper::table().
     */
    public function testTableResponsive()
    {
        $this->_setRequest(static::$requestsParams['page_1_of_1']);
        $root = Router::url('/');

        $fields = [
            'Apple.id' => [
                'sort' => false,
            ],
            'Apple.name',
            '/Apples/view/#Apple.id#' => [
                'title' => true,
            ],
        ];
        $result = $this->LibricielBootstrap3Table->table([static::$records[0]], $fields, ['responsive' => true]);
        $expected =
            '<div class="table-responsive">
                <table class="table table-hover table-striped table-condensed">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th><a href="' . $root . 'apples/index/sort:Apple.name/direction:asc">Variété</a></th>
                            <th colspan="1" class="actions">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="data integer positive">3</td>
                            <td class="data string">Braeburn</td>
                            <td class="action apples view">
                                <a href="' . $root . 'apples/view/3" title="/Apples/view/3:title" class="btn btn-sm btn-default controller-apples action-view"><span class="fa fa-lg fa-eye"></span><span class="text">Voir</span></a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>';
        $this->assertEqualsXhtml($expected, $result, var_export($result, true));
    }

    /**
     * Test de la méthode LibricielBootstrap3TableHelper::table() sans résultat.
     */
    public function testTableEmpty()
    {
        $this->_setRequest(static::$requestsParams['page_1_of_1']);
        $result = $this->LibricielBootstrap3Table->table([], []);
        $expected = null;
        $this->assertEqualsXhtml($expected, $result, var_export($result, true));
    }

    /**
     * Test de la méthode LibricielBootstrap3TableHelper::table() sans résultat mais avec l'option responsive.
     */
    public function testTableEmptyResponsive()
    {
        $this->_setRequest(static::$requestsParams['page_1_of_1']);
        $result = $this->LibricielBootstrap3Table->table([], [], ['responsive' => true]);
        $expected = null;
        $this->assertEqualsXhtml($expected, $result, var_export($result, true));
    }

    /**
     * Test de la méthode LibricielBootstrap3TableHelper::td() avec des données
     * et l'ajout d'une classe évaluée dynamiquement.
     */
    public function testTdDataEvaluatedClass()
    {
        $this->_setRequest(static::$requestsParams['page_1_of_1']);

        $result = $this->LibricielBootstrap3Table->td(static::$records[0], 'Apple.id', ['class' => 'apple-id-#Apple.id#']);
        $expected = '<td class="apple-id-3 data integer positive">3</td>';
        $this->assertEqualsXhtml($expected, $result, var_export($result, true));
    }

    /**
     * Test de la méthode LibricielBootstrap3TableHelper::td() avec une action
     * et l'ajout d'une classe évaluée dynamiquement.
     */
    public function testTdActionEvaluatedClass()
    {
        $this->_setRequest(static::$requestsParams['page_1_of_1']);

        $result = $this->LibricielBootstrap3Table->td(static::$records[0], '/Apples/view/#Apple.id#', ['class' => 'apple-id-#Apple.id#']);
        $root = Router::url('/');
        $expected = '<td class="apple-id-3 action apples view"><a href="' . $root . 'apples/view/3" class="btn btn-sm btn-default controller-apples action-view"><span class="fa fa-lg fa-eye"> </span> <span class="text">Voir</span></a></td>';
        $this->assertEqualsXhtml($expected, $result, var_export($result, true));
    }

    /**
     * Test de la méthode LibricielBootstrap3TableHelper::td() avec des données
     * et l'ajout d'une icône évaluée dynamiquement.
     */
    public function testTdDataEvaluatedIcon()
    {
        $this->_setRequest(static::$requestsParams['page_1_of_1']);

        $result = $this->LibricielBootstrap3Table->td(static::$records[0], 'Apple.id', ['icon' => 'apple-id-#Apple.id#']);
        $expected = '<td class="data integer positive"><span class="apple-id-3"> </span> 3</td>';
        $this->assertEqualsXhtml($expected, $result, var_export($result, true));
    }

    /**
     * Test de la méthode LibricielBootstrap3TableHelper::td() avec des données
     * et l'ajout d'une icône évaluée dynamiquement avec des conditions.
     */
    public function testTdDataEvaluatedIconConditions()
    {
        $this->_setRequest(static::$requestsParams['page_1_of_1']);

        $params = ['icon' => '#Apple.id# % 2 ? "apple-id-odd-#Apple.id#" : "apple-id-even-#Apple.id#"'];
        $result = $this->LibricielBootstrap3Table->td(static::$records[0], 'Apple.id', $params);
        $expected = '<td class="data integer positive"><span class="apple-id-odd-3"> </span> 3</td>';
        $this->assertEqualsXhtml($expected, $result, var_export($result, true));
    }

    /**
     * Test de la méthode LibricielBootstrap3TableHelper::td() avec des données
     * et une erreur lors de l'évaluation dynamique.
     */
    public function testTdDataEvaluatedIconConditionsErrors()
    {
        $this->_setRequest(static::$requestsParams['page_1_of_1']);

        $params = ['icon' => '#Apple.id# % 2 ? "apple-id-odd-#Apple.id#" ? "apple-id-even-#Apple.id#"'];
        $result = $this->LibricielBootstrap3Table->td(static::$records[0], 'Apple.id', $params);
        $expected = '<td class="data integer positive"><span class="3 % 2 ? &quot;apple-id-odd-3&quot; ? &quot;apple-id-even-3&quot;"></span> 3</td>';
        $this->assertEqualsXhtml($expected, $result, var_export($result, true));
    }

    /**
     * Test de la méthode LibricielBootstrap3TableHelper::td() avec des données
     * et une condition contenant des quotes.
     */
    public function testTdDataEvaluatedIconConditionsQuotes()
    {
        $this->_setRequest(static::$requestsParams['page_1_of_1']);

        $params = ['icon' => '#Apple.id# % 2 ? "odd: #Apple.name#" : "even: #Apple.name#"'];
        $result = $this->LibricielBootstrap3Table->td(static::$records[1], 'Apple.id', $params);
        $expected = '<td class="data integer positive"><span class="7 % 2 ? &quot;odd: J&#039;ai des quotes &quot;échappées&quot;&quot; : &quot;even: J&#039;ai des quotes &quot;échappées&quot;&quot;"></span> 7</td>';
        $this->assertEqualsXhtml($expected, $result, var_export($result, true));
    }

    /**
     * Test de la méthode LibricielBootstrap3TableHelper::td() avec un type de
     * données explicite et une valeur négative
     */
    public function testTdDataExplicitTypeNegativeIntegerValue()
    {
        $record = [
            'Apple' => [
                'popularity' => -666,
            ],
        ];

        $result = $this->LibricielBootstrap3Table->td($record, 'Apple.popularity', ['type' => 'integer']);
        $expected = '<td class="data integer negative">-666</td>';
        $this->assertEqualsXhtml($expected, $result, var_export($result, true));
    }

    /**
     * Test de la méthode LibricielBootstrap3TableHelper::td() avec des données à échapper.
     */
    public function testTdDataEscaped()
    {
        $record = [
            'Apple' => [
                'name' => 'M&M',
            ],
        ];

        $result = $this->LibricielBootstrap3Table->td($record, 'Apple.name', ['type' => 'string']);
        $expected = '<td class="data string">M&amp;M</td>';
        $this->assertEqualsXhtml($expected, $result, var_export($result, true));
    }

    /**
     * Test de la méthode LibricielBootstrap3TableHelper::data()...
     */
    public function testDataFormatPhone()
    {
        $this->_setRequest(static::$requestsParams['page_1_of_1']);

        // 1. Avec une valeur
        $result = $this->LibricielBootstrap3Table->data('User.phone', static::$records[0], ['data-format' => 'phone']);
        $expected = '06 05 04 01 02';
        $this->assertEqualsXhtml($expected, $result, var_export($result, true));

        // 2. Sans valeur
        $result = $this->LibricielBootstrap3Table->data('User.phone', static::$records[1], ['data-format' => 'phone']);
        $expected = '';
        $this->assertEqualsXhtml($expected, $result, var_export($result, true));

        // 3. Avec une valeur erronée
        $result = $this->LibricielBootstrap3Table->data('User.phone', static::$records[2], ['data-format' => 'phone']);
        $expected = 'foo';
        $this->assertEqualsXhtml($expected, $result, var_export($result, true));
    }

    /**
     * Test de la méthode LibricielBootstrap3TableHelper::data() avec utilisation
     * d'une autre classe de formatage des données.
     */
    public function testDataFormatPhoneAlternateEngine()
    {
        $this->_setRequest(static::$requestsParams['page_1_of_1']);

        $this->LibricielBootstrap3Table->engine(['engine' => 'AlternateDataEngine']);

        // 1. Avec une valeur
        $result = $this->LibricielBootstrap3Table->data('User.phone', static::$records[0], ['data-format' => 'phone']);
        $expected = '06 05 04 01 02 (extended)';
        $this->assertEqualsXhtml($expected, $result, var_export($result, true));

        // 2. Sans valeur
        $result = $this->LibricielBootstrap3Table->data('User.phone', static::$records[1], ['data-format' => 'phone']);
        $expected = '';
        $this->assertEqualsXhtml($expected, $result, var_export($result, true));

        // 3. Avec une valeur erronée
        $result = $this->LibricielBootstrap3Table->data('User.phone', static::$records[2], ['data-format' => 'phone']);
        $expected = 'foo (extended)';
        $this->assertEqualsXhtml($expected, $result, var_export($result, true));
    }

    /**
     * Test de la méthode LibricielBootstrap3TableHelper::data() avec spécification
     * de la valeur.
     */
    public function testDataValue()
    {
        $this->_setRequest(static::$requestsParams['page_1_of_1']);

        // 1. En spécifiant la valeur en dur
        $result = $this->LibricielBootstrap3Table->data('User.phone', static::$records[0], ['value' => 'Foo!!']);
        $expected = 'Foo!!';
        $this->assertEqualsXhtml($expected, $result, var_export($result, true));

        // 2. En spécifiant une valeur venant de deux autres champs
        $result = $this->LibricielBootstrap3Table->data('User.phone', static::$records[1], ['value' => '#User.id# - #User.username#']);
        $expected = '5 - cjerome';
        $this->assertEqualsXhtml($expected, $result, var_export($result, true));

        // 3. En spécifiant une valeur venant d'un calcul sur un autre champ
        $result = $this->LibricielBootstrap3Table->data('User.phone', static::$records[2], ['value' => 'pow(#User.id#, 2)']);
        $expected = '36';
        $this->assertEqualsXhtml($expected, $result, var_export($result, true));
    }

    /**
     * Test de la méthode LibricielBootstrap3TableHelper::td() avec des données
     * et une traduction automatique de l'option.
     */
    public function testTdDataTranslatedOption()
    {
        $this->_setRequest(static::$requestsParams['page_1_of_1']);

        $result = $this->LibricielBootstrap3Table->td(static::$records[0], 'Apple.color', ['options' => static::$options]);
        $expected = '<td class="data string">Rouge</td>';
        $this->assertEqualsXhtml($expected, $result, var_export($result, true));

        $result = $this->LibricielBootstrap3Table->td(static::$records[0], 'Apple.color', ['options' => static::$options['Apple']['color']]);
        $expected = '<td class="data string">Rouge</td>';
        $this->assertEqualsXhtml($expected, $result, var_export($result, true));
    }

    /**
     * Test de la méthode LibricielBootstrap3TableHelper::action() avec utilisation
     * d'une configuration surchargée.
     */
    public function testAction()
    {
        $this->_setRequest(static::$requestsParams['page_1_of_1']);
        $root = Router::url('/');

        $classes = [
            'buttons' => [
                'edit' => 'fa fa-lg fa-foo',
                'apples:delete' => 'fa fa-lg fa-bar',
                'libriciel_bootstrap3.apples:view' => 'fa fa-lg fa-bar',
                'libriciel_bootstrap3.apples:view.crt' => 'fa fa-lg fa-boz',
                'http://pki.libriciel.fr/ac-libriciel-racine-g1.crl' => 'fa fa-lg fa-pki',
            ],
            'icons' => [
                'edit' => 'btn-foo',
                'apples:delete' => 'btn-bar',
                'libriciel_bootstrap3.apples:view' => 'btn-baz',
                'libriciel_bootstrap3.apples:view.crt' => 'btn-boz',
                'http://pki.libriciel.fr/ac-libriciel-racine-g1.crl' => 'btn-pki',
            ],
        ];
        $this->LibricielBootstrap3Table->classes($classes);

        // 1. classes par défaut
        $result = $this->LibricielBootstrap3Table->action('/Apples/view/#Apple.id#', static::$records[0]);
        $expected = '<a href="' . $root . 'apples/view/3" class="btn btn-sm btn-default controller-apples action-view"><span class="fa fa-lg fa-eye"> </span> <span class="text">Voir</span></a>';
        $this->assertEqualsXhtml($expected, $result, var_export($result, true));

        // 2. suracharge de l'action edit
        $result = $this->LibricielBootstrap3Table->action('/Apples/edit/#Apple.id#', static::$records[0]);
        $expected = '<a href="' . $root . 'apples/edit/3" class="btn btn-sm btn-foo controller-apples action-edit"><span class="fa fa-lg fa-foo"></span><span class="text">Modifier</span></a>';
        $this->assertEqualsXhtml($expected, $result, var_export($result, true));

        // 3. précision pour l'action ApplesController::delete
        $result = $this->LibricielBootstrap3Table->action('/Apples/delete/#Apple.id#', static::$records[0]);
        $expected = '<a href="' . $root . 'apples/delete/3" class="btn btn-sm btn-bar controller-apples action-delete"><span class="fa fa-lg fa-bar"></span><span class="text">Supprimer</span></a>';
        $this->assertEqualsXhtml($expected, $result, var_export($result, true));

        // 4. Avec une URL dans un plugin
        $result = $this->LibricielBootstrap3Table->action('/LibricielBootstrap3.Apples/view/#Apple.id#', static::$records[0]);
        $expected = '<a href="' . $root . 'libriciel_bootstrap3/apples/view/3" class="btn btn-sm btn-baz plugin-libriciel_bootstrap3 controller-apples action-view"><span class="fa fa-lg fa-bar"></span><span class="text">/LibricielBootstrap3.Apples/view/#Apple.id#</span></a>';
        $this->assertEqualsXhtml($expected, $result, var_export($result, true));

        // 5. Disabled
        $result = $this->LibricielBootstrap3Table->action('/Apples/view/#Apple.id#', static::$records[0], ['disabled' => '"#Apple.id#" == 3']);
        $expected = '<a href="#"  class="btn btn-sm btn-default controller-apples action-view disabled"><span class="fa fa-lg fa-eye"> </span> <span class="text">Voir</span></a>';
        $this->assertEqualsXhtml($expected, $result, var_export($result, true));

        // 6. Disabled pour un postLink
        $result = $this->LibricielBootstrap3Table->action('/Apples/view/#Apple.id#', static::$records[0], ['disabled' => '"#Apple.id#" == 3', 'type' => 'postLink']);
        $result = preg_replace('/post_[a-z0-9]+/', 'post_5bd837ce6087e745955361', $result);
        $expected = '<form action="#" name="post_5bd837ce6087e745955361" id="post_5bd837ce6087e745955361" style="display:none;" method="post"><input type="hidden" name="_method" value="POST"/></form><a href="#" class="btn btn-sm btn-default controller-apples action-view disabled" onclick="document.post_5bd837ce6087e745955361.submit(); event.returnValue = false; return false;"><span class="fa fa-lg fa-eye"></span><span class="text">Voir</span></a>';
        $this->assertEqualsXhtml($expected, $result, var_export($result, true));

        // 7. Avec une extension
        $result = $this->LibricielBootstrap3Table->action('/LibricielBootstrap3.Apples/view/#Apple.id#.crt', static::$records[0]);
        $expected = '<a href="' . $root . 'libriciel_bootstrap3/apples/view/3.crt" class="btn btn-sm btn-boz plugin-libriciel_bootstrap3 controller-apples action-view"><span class="fa fa-lg fa-boz"></span><span class="text">/LibricielBootstrap3.Apples/view/#Apple.id#.crt</span></a>';
        $this->assertEqualsXhtml($expected, $result, var_export($result, true));

        // 8. Avec une URL externe
        $result = $this->LibricielBootstrap3Table->action('/Apples/view/#Apple.id#', static::$records[0], ['value' => 'http://pki.libriciel.fr/ac-libriciel-racine-g1.crl']);
        $expected = '<a href="http://pki.libriciel.fr/ac-libriciel-racine-g1.crl" value="http://pki.libriciel.fr/ac-libriciel-racine-g1.crl" class="btn btn-sm btn-pki"><span class="fa fa-lg fa-pki"></span><span class="text">Voir</span></a>';
        $this->assertEqualsXhtml($expected, $result, var_export($result, true));

        // 9. Interpolation et échappement de title et confirm
        $result = $this->LibricielBootstrap3Table->action('/Apples/view/#Apple.id#', static::$records[0], ['confirm' => 'Is it "#Apple.id#" ?', 'title' => 'It\'s "#Apple.id#"']);
        $expected = '<a href="' . $root . 'apples/view/3" title="It&#039;s &quot;3&quot;" class="btn btn-sm btn-default controller-apples action-view" onclick="if (confirm(&quot;Is it \&quot;3\&quot; ?&quot;)) { return true; } return false;"><span class="fa fa-lg fa-eye"> </span> <span class="text">Voir</span></a>';
        $this->assertEqualsXhtml($expected, $result, var_export($result, true));
    }

    /**
     * Test de la méthode LibricielBootstrap3TableHelper::table() avec des actions
     * cachées.
     */
    public function testTableHiddenActions()
    {
        $this->_setRequest(static::$requestsParams['page_1_of_1']);
        $root = Router::url('/');

        $fields = [
            'Apple.id',
            'Apple.name',
            '/Apples/view/#Apple.id#' => [
                'title' => true,
                'hidden' => '"#Apple.id#" % 2 == "0" ? false : true',
            ],
            '/Apples/edit/#Apple.id#' => [
                'title' => true,
                'hidden' => '"#Apple.id#" % 2 == "1" ? false : true',
            ],
        ];
        $result = $this->LibricielBootstrap3Table->table(static::$records, $fields, ['group-actions' => true]);
        $expected =
            '<table class="table table-hover table-striped table-condensed">
            <thead>
                <tr>
                    <th><a href="' . $root . 'apples/index/sort:Apple.id/direction:asc">Id</a></th>
                    <th><a href="' . $root . 'apples/index/sort:Apple.name/direction:asc">Variété</a></th>
                    <th colspan="1" class="actions">Actions</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="data integer positive">3</td>
                    <td class="data string">Braeburn</td>
                    <td class="actions">
                        <div class="btn-group" role="group">
                            <a href="' . $root . 'apples/edit/3" title="/Apples/edit/3:title" class="btn btn-sm btn-primary controller-apples action-edit">
                                <span class="fa fa-lg fa-pencil"></span><span class="text sr-only">Modifier</span>
                            </a>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="data integer positive">7</td>
                    <td class="data string">J&#039;ai des quotes &quot;échappées&quot;</td>
                    <td class="actions">
                        <div class="btn-group" role="group">
                            <a href="' . $root . 'apples/edit/7" title="/Apples/edit/7:title" class="btn btn-sm btn-primary controller-apples action-edit">
                                <span class="fa fa-lg fa-pencil"> </span><span class="text sr-only">Modifier</span>
                            </a>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="data integer null">
                    <td class="data string null">
                    <td class="actions">
                    </td>
                </tr>
            </tbody>
        </table>';
        $this->assertEqualsXhtml($expected, $result, var_export($result, true));
    }

    /**
     * Test de la méthode LibricielBootstrap3TableHelper::table() avec plusieurs
     * btn-group.
     */
    public function testTableWithActionsBtnGroups()
    {
        $this->_setRequest(static::$requestsParams['page_1_of_1']);
        $root = Router::url('/');

        $fields = [
            'Apple.id',
            'Apple.name',
            '/Apples/view/#Apple.id#' => [
                'title' => true,
            ],
            '/Apples/edit/#Apple.id#' => [
                'title' => true,
            ],
            '/Apples/delete/#Apple.id#' => [
                'title' => true,
                'confirm' => true,
                'type' => 'postLink',
                'btn-group' => 1,
            ],
        ];
        $result = $this->LibricielBootstrap3Table->table([static::$records[0]], $fields, ['group-actions' => true]);
        $result = preg_replace('/post_[a-z0-9]+/', 'post_5b4cb9fa305e2975865439', $result);
        $expected =
            '<table class="table table-hover table-striped table-condensed">
            <thead>
                <tr>
                    <th><a href="' . $root . 'apples/index/sort:Apple.id/direction:asc">Id</a></th>
                    <th><a href="' . $root . 'apples/index/sort:Apple.name/direction:asc">Variété</a></th>
                    <th colspan="1" class="actions">Actions</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="data integer positive">3</td>
                    <td class="data string">Braeburn</td>
                    <td class="actions">
                        <div class="btn-group" role="group">
                            <a href="' . $root . 'apples/view/3" title="/Apples/view/3:title" class="btn btn-sm btn-default controller-apples action-view">
                                <span class="fa fa-lg fa-eye"></span><span class="text sr-only">Voir</span>
                            </a>
                            <a href="' . $root . 'apples/edit/3" title="/Apples/edit/3:title" class="btn btn-sm btn-primary controller-apples action-edit">
                                <span class="fa fa-lg fa-pencil"></span><span class="text sr-only">Modifier</span>
                            </a>
                        </div>
                        <div class="btn-group" role="group">
                            <form action="' . $root . 'apples/delete/3" name="post_5b4cb9fa305e2975865439" id="post_5b4cb9fa305e2975865439" style="display:none;" method="post">
                                <input type="hidden" name="_method" value="POST"/>
                            </form>
                            <a href="#" title="/Apples/delete/3:title" class="btn btn-sm btn-danger controller-apples action-delete" onclick="if (confirm(&quot;\/Apples\/delete\/3:confirm&quot;)) { document.post_5b4cb9fa305e2975865439.submit(); } event.returnValue = false; return false;">
                                <span class="fa fa-lg fa-trash"></span><span class="text sr-only">Supprimer</span>
                            </a>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>';
        $this->assertEqualsXhtml($expected, $result, var_export($result, true));
    }

    /**
     * Test de la méthode LibricielBootstrap3TableHelper::engine() avec une classe
     * qui n'existe pas.
     *
     * @expectedException        RuntimeException
     * @expectedExceptionMessage Engine class "Foo" could not be found
     */
    public function testEngineClassNotFound()
    {
        $this->LibricielBootstrap3Table->engine(['engine' => 'Foo']);
    }

    /**
     * Test de la méthode LibricielBootstrap3TableHelper::engine() avec une classe
     * qui n'implémente pas l'interface LibricielBootstrap3DataInterface.
     *
     * @expectedException        RuntimeException
     * @expectedExceptionMessage Engine class "LibricielBootstrap3TableHelper" should implement LibricielBootstrap3DataInterface
     */
    public function testEngineClassNotImplementsLibricielBootstrap3DataInterface()
    {
        $this->LibricielBootstrap3Table->engine(['engine' => 'LibricielBootstrap3TableHelper']);
    }

    /**
     * Test de la méthode LibricielBootstrap3TableHelper::engine() ...
     */
    public function testClasses()
    {
        // 1. Sans que la clé soit configurée
        $result = $this->LibricielBootstrap3Table->classes('Foo.Bar.Baz');
        $expected = [
            'table' => 'table table-hover table-striped table-condensed',
            'buttons' => [
                'add' => 'fa fa-lg fa-plus-circle',
                'view' => 'fa fa-lg fa-eye',
                'edit' => 'fa fa-lg fa-pencil',
                'delete' => 'fa fa-lg fa-trash',
            ],
            'icons' => [
                'add' => 'btn-success',
                'delete' => 'btn-danger',
                'edit' => 'btn-primary',
                'view' => 'btn-default',
            ],
        ];
        $this->assertEquals($expected, $result, var_export($result, true));

        // 2. Lorsque la clé est configurée
        Configure::write('Foo.Bar.Baz', ['table' => 'table-libriciel', 'extra' => 'fa-extra']);
        $result = $this->LibricielBootstrap3Table->classes('Foo.Bar.Baz');
        $expected = [
            'table' => 'table-libriciel',
            'extra' => 'fa-extra',
            'buttons' => [
                'add' => 'fa fa-lg fa-plus-circle',
                'view' => 'fa fa-lg fa-eye',
                'edit' => 'fa fa-lg fa-pencil',
                'delete' => 'fa fa-lg fa-trash',
            ],
            'icons' => [
                'add' => 'btn-success',
                'delete' => 'btn-danger',
                'edit' => 'btn-primary',
                'view' => 'btn-default',
            ],
        ];
        $this->assertEquals($expected, $result, var_export($result, true));
    }
}
