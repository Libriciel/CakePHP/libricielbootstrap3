<?php
/**
 * Code source de la classe LibricielBootstrap3HelperTest.
 *
 * @package LibricielBootstrap3
 * @subpackage Test.Case.View.Helper
 */
App::uses('View', 'View');
App::uses('AppHelper', 'View/Helper');
App::uses('CakeRequest', 'Network');
App::uses('LibricielBootstrap3AbstractTestCase', 'LibricielBootstrap3.Test/Case');
App::uses('LibricielBootstrap3Data', 'LibricielBootstrap3.Utility');
App::uses('LibricielBootstrap3Helper', 'LibricielBootstrap3.View/Helper');
App::uses('Translator', 'Translator.Utility');

/**
 * La classe LibricielBootstrap3HelperTest ...
 *
 * @covers LibricielBootstrap3Helper
 *
 * @package LibricielBootstrap3
 * @subpackage Test.Case.View.Helper
 */
class LibricielBootstrap3HelperTest extends LibricielBootstrap3AbstractTestCase
{
    /**
     * Fixtures utilisés par ces tests unitaires.
     *
     * @var array
     */
    public $fixtures = [
        'core.Apple',
        'core.DataTest',
    ];

    /**
     * Modèle Apple
     *
     * @var Model
     */
    public $Apple = null;

    /**
     * Modèle DataTest
     *
     * @var Model
     */
    public $DataTest = null;

    public static $requestsParams = [
        'page_1_of_1' => [
            'paging' => [
                'Apple' => [
                    'page' => 1,
                    'current' => 2,
                    'count' => 2,
                    'prevPage' => false,
                    'nextPage' => false,
                    'pageCount' => 1,
                    'order' => null,
                    'limit' => 20,
                    'options' => [
                        'page' => 1,
                        'conditions' => [ ],
                    ],
                    'paramType' => 'named',
                ],
            ],
            'controller' => 'apples',
            'action' => 'index',
        ],
        'page_1_of_2' => [
            'paging' => [
                'Apple' => [
                    'page' => 1,
                    'current' => 9,
                    'count' => 21,
                    'prevPage' => false,
                    'nextPage' => true,
                    'pageCount' => 2,
                    'order' => null,
                    'limit' => 20,
                    'options' => [
                        'page' => 1,
                        'conditions' => [ ],
                    ],
                    'paramType' => 'named',
                ],
            ],
            'controller' => 'apples',
            'action' => 'index',
        ],
        'page_2_of_2' => [
            'paging' => [
                'Apple' => [
                    'page' => 2,
                    'current' => 21,
                    'count' => 21,
                    'prevPage' => true,
                    'nextPage' => false,
                    'pageCount' => 2,
                    'order' => null,
                    'limit' => 20,
                    'options' => [
                        'page' => 2,
                        'conditions' => [ ],
                    ],
                    'paramType' => 'named',
                ],
            ],
            'controller' => 'apples',
            'action' => 'index',
        ],
        'page_2_of_7' => [
            'paging' => [
                'Apple' => [
                    'page' => 2,
                    'current' => 9,
                    'count' => 62,
                    'prevPage' => false,
                    'nextPage' => true,
                    'pageCount' => 7,
                    'order' => null,
                    'limit' => 20,
                    'options' => [
                        'page' => 1,
                        'conditions' => [ ],
                    ],
                    'paramType' => 'named',
                ],
            ],
            'controller' => 'apples',
            'action' => 'index',
        ],
    ];

    /**
     * Definit une url fictive
     *
     * @param array $params
     */
    protected function _setRequest(array $params = [])
    {
        $request = $this->_getCakeRequest($params);
        $this->LibricielBootstrap3->request = $request;
        $this->LibricielBootstrap3->LibricielBootstrap3Paginator->request = $request;
    }

    /**
     * Préparation du test.
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();

        $this->DataTest = ClassRegistry::init('DataTest');
        $this->Apple = ClassRegistry::init('Apple');

        $controller = null;
        $this->View = new View($controller);
        $this->LibricielBootstrap3 = new LibricielBootstrap3Helper($this->View);

        $this->LibricielBootstrap3->request = new CakeRequest('apples/add', false);
        $this->LibricielBootstrap3->request->here = '/apples/add';
        $this->LibricielBootstrap3->request['controller'] = 'apples';
        $this->LibricielBootstrap3->request['action'] = 'add';
        $this->LibricielBootstrap3->request->webroot = '';
        $this->LibricielBootstrap3->request->base = '';

        $this->LibricielBootstrap3->request->data = [
            'Apple' => [
                'id' => 6,
                'name' => 'Pink Lady',
                'date' => '2014-03-17',
                'category' => 'red',
                'description' => "Line 1\nLine2",
            ],
        ];
    }

    /**
     * Nettoyage postérieur au test.
     *
     * @return void
     */
    public function tearDown()
    {
        parent::tearDown();
        unset($this->View, $this->LibricielBootstrap3, $this->DataTest, $this->Apple);
    }

    /**
     * Test de la méthode LibricielBootstrap3Helper::index().
     *
     * @return void
     */
    public function testIndexPage1Of7()
    {
        $this->_setRequest(static::$requestsParams['page_1_of_1']);
        $root = Router::url('/');

        $result = $this->LibricielBootstrap3->index(
            [
                [
                    'Apple' => [
                        'id' => 6,
                        'name' => 'Pink Lady',
                        'date' => '2014-03-17',
                        'category' => 'red',
                        'description' => "Line 1\nLine2",
                    ],
                ],
                [
                    'Apple' => [
                        'id' => 8,
                        'name' => 'Azeroli Anisé',
                        'date' => '2018-06-27',
                        'category' => 'green',
                        'description' => "Reinette de calibre moyen, à épiderme liégeux strié de pourpre.",
                    ],
                ],
            ],
            [
                'Apple.id',
                'Apple.name',
                'Apple.date',
                'Apple.category',
                'Apple.description',
                '/Apples/view/#Apple.id#',
                '/Apples/edit/#Apple.id#',
                '/Apples/delete/#Apple.id#',
            ]
        );
        $expected =
        '<div class="pagination-block">
            <p class="pagination-counter">Page 1 sur 1, 2 enregistrements sur un total de 2, de l\'enregistrement 1 à 2</p>
        </div>
        <table class="table table-hover table-striped table-condensed">
            <thead>
                <tr>
                    <th><a href="' . $root . 'apples/index/sort:Apple.id/direction:asc">Id</a></th>
                    <th><a href="' . $root . 'apples/index/sort:Apple.name/direction:asc">Variété</a></th>
                    <th><a href="' . $root . 'apples/index/sort:Apple.date/direction:asc">Date</a></th>
                    <th><a href="' . $root . 'apples/index/sort:Apple.category/direction:asc">Catégorie</a></th>
                    <th><a href="' . $root . 'apples/index/sort:Apple.description/direction:asc">Description</a></th>
                    <th colspan="3" class="actions">Actions</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="data integer positive">6</td>
                    <td class="data string">Pink Lady</td>
                    <td class="data date">17/03/2014</td>
                    <td class="data string">red</td>
                    <td class="data string">Line 1 Line2</td>
                    <td class="action apples view">
                        <a href="' . $root . 'apples/view/6" class="btn btn-sm btn-default controller-apples action-view"><span class="fa fa-lg fa-eye"></span><span class="text">Voir</span></a>
                    </td>
                    <td class="action apples edit">
                        <a href="' . $root . 'apples/edit/6" class="btn btn-sm btn-primary controller-apples action-edit"><span class="fa fa-lg fa-pencil"></span><span class="text">Modifier</span></a>
                    </td>
                    <td class="action apples delete">
                        <a href="' . $root . 'apples/delete/6" class="btn btn-sm btn-danger controller-apples action-delete"><span class="fa fa-lg fa-trash"></span><span class="text">Supprimer</span></a>
                    </td>
                </tr>
                <tr>
                    <td class="data integer positive">8</td>
                    <td class="data string">Azeroli Anisé</td>
                    <td class="data date">27/06/2018</td>
                    <td class="data string">green</td>
                    <td class="data string">Reinette de calibre moyen, à épiderme liégeux strié de pourpre.</td>
                    <td class="action apples view">
                        <a href="' . $root . 'apples/view/8" class="btn btn-sm btn-default controller-apples action-view"><span class="fa fa-lg fa-eye"></span><span class="text">Voir</span></a>
                    </td>
                    <td class="action apples edit">
                        <a href="' . $root . 'apples/edit/8" class="btn btn-sm btn-primary controller-apples action-edit"><span class="fa fa-lg fa-pencil"></span><span class="text">Modifier</span></a>
                    </td>
                    <td class="action apples delete">
                        <a href="' . $root . 'apples/delete/8" class="btn btn-sm btn-danger controller-apples action-delete"><span class="fa fa-lg fa-trash"></span><span class="text">Supprimer</span></a>
                    </td>
                </tr>
            </tbody>
        </table>
        <div class="pagination-block">
            <p class="pagination-counter">Page 1 sur 1, 2 enregistrements sur un total de 2, de l\'enregistrement 1 à 2</p>
        </div>';
        $this->assertEqualsXhtml($expected, $result);
    }

    /**
     * Test de la méthode LibricielBootstrap3Helper::index() avec le paramètre responsive.
     *
     * @return void
     */
    public function testIndexPage1Of7Responsive()
    {
        $this->_setRequest(static::$requestsParams['page_1_of_1']);
        $root = Router::url('/');

        $result = $this->LibricielBootstrap3->index(
            [
                [
                    'Apple' => [
                        'id' => 6,
                        'name' => 'Pink Lady',
                        'date' => '2014-03-17',
                        'category' => 'red',
                        'description' => "Line 1\nLine2",
                    ],
                ],
                [
                    'Apple' => [
                        'id' => 8,
                        'name' => 'Azeroli Anisé',
                        'date' => '2018-06-27',
                        'category' => 'green',
                        'description' => "Reinette de calibre moyen, à épiderme liégeux strié de pourpre.",
                    ],
                ],
            ],
            [
                'Apple.id',
                'Apple.name',
                '/Apples/view/#Apple.id#',
            ],
            [
                'responsive' => true,
            ]
        );
        $expected =
        '<div class="pagination-block">
            <p class="pagination-counter">Page 1 sur 1, 2 enregistrements sur un total de 2, de l\'enregistrement 1 à 2</p>
        </div>
        <div class="table-responsive">
            <table class="table table-hover table-striped table-condensed">
                <thead>
                    <tr>
                        <th><a href="' . $root . 'apples/index/sort:Apple.id/direction:asc">Id</a></th>
                        <th><a href="' . $root . 'apples/index/sort:Apple.name/direction:asc">Variété</a></th>
                        <th colspan="1" class="actions">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="data integer positive">6</td>
                        <td class="data string">Pink Lady</td>
                        <td class="action apples view">
                            <a href="' . $root . 'apples/view/6" class="btn btn-sm btn-default controller-apples action-view"><span class="fa fa-lg fa-eye"></span><span class="text">Voir</span></a>
                        </td>
                    </tr>
                    <tr>
                        <td class="data integer positive">8</td>
                        <td class="data string">Azeroli Anisé</td>
                        <td class="action apples view">
                            <a href="' . $root . 'apples/view/8" class="btn btn-sm btn-default controller-apples action-view"><span class="fa fa-lg fa-eye"></span><span class="text">Voir</span></a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="pagination-block">
            <p class="pagination-counter">Page 1 sur 1, 2 enregistrements sur un total de 2, de l\'enregistrement 1 à 2</p>
        </div>';
        $this->assertEqualsXhtml($expected, $result);
    }

    /**
     * Test de la méthode LibricielBootstrap3Helper::index().
     *
     * @return void
     */
    public function testIndexPage2Of7()
    {
        $this->_setRequest(static::$requestsParams['page_2_of_7']);
        $root = Router::url('/');

        $result = $this->LibricielBootstrap3->index(
            [
                [
                    'Apple' => [
                        'id' => 6,
                        'name' => 'Pink Lady',
                        'date' => '2014-03-17',
                        'category' => 'red',
                        'description' => "Line 1\nLine2",
                    ],
                ],
                [
                    'Apple' => [
                        'id' => 8,
                        'name' => 'Azeroli Anisé',
                        'date' => '2018-06-27',
                        'category' => 'green',
                        'description' => "Reinette de calibre moyen, à épiderme liégeux strié de pourpre.",
                    ],
                ],
            ],
            [
                'Apple.id',
                'Apple.name',
                'Apple.date',
                'Apple.category',
                'Apple.description',
                '/Apples/view/#Apple.id#',
                '/Apples/edit/#Apple.id#',
                '/Apples/delete/#Apple.id#',
            ]
        );
        $expected =
        '<div class="pagination-block">
            <p class="pagination-counter">Page 2 sur 7, 9 enregistrements sur un total de 62, de l\'enregistrement 21 à 40</p>
            <ul class="pagination">
                <li class="first"><a href="' . $root . 'apples" rel="first">&lt;&lt;</a></li>
                <li class="disabled"><span>&lt;</span></li>
                <li><a href="' . $root . 'apples">1</a></li>
                <li class="active"><span>2</span></li><li><a href="' . $root . 'apples/index/page:3">3</a></li>
                <li><a href="' . $root . 'apples/index/page:4">4</a></li>
                <li><a href="' . $root . 'apples/index/page:5">5</a></li>
                <li><a href="' . $root . 'apples/index/page:6">6</a></li>
                <li><a href="' . $root . 'apples/index/page:7">7</a></li>
                <li class="next"><a href="' . $root . 'apples/index/page:3" rel="next">&gt;</a></li>
                <li class="last"><a href="' . $root . 'apples/index/page:7" rel="last">&gt;&gt;</a></li>
            </ul>
        </div>
        <table class="table table-hover table-striped table-condensed">
            <thead>
                <tr>
                    <th><a href="' . $root . 'apples/index/sort:Apple.id/direction:asc">Id</a></th>
                    <th><a href="' . $root . 'apples/index/sort:Apple.name/direction:asc">Variété</a></th>
                    <th><a href="' . $root . 'apples/index/sort:Apple.date/direction:asc">Date</a></th>
                    <th><a href="' . $root . 'apples/index/sort:Apple.category/direction:asc">Catégorie</a></th>
                    <th><a href="' . $root . 'apples/index/sort:Apple.description/direction:asc">Description</a></th>
                    <th colspan="3" class="actions">Actions</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="data integer positive">6</td>
                    <td class="data string">Pink Lady</td>
                    <td class="data date">17/03/2014</td>
                    <td class="data string">red</td>
                    <td class="data string">Line 1 Line2</td>
                    <td class="action apples view">
                        <a href="' . $root . 'apples/view/6" class="btn btn-sm btn-default controller-apples action-view"><span class="fa fa-lg fa-eye"></span><span class="text">Voir</span></a>
                    </td>
                    <td class="action apples edit">
                        <a href="' . $root . 'apples/edit/6" class="btn btn-sm btn-primary controller-apples action-edit"><span class="fa fa-lg fa-pencil"></span><span class="text">Modifier</span></a>
                    </td>
                    <td class="action apples delete">
                        <a href="' . $root . 'apples/delete/6" class="btn btn-sm btn-danger controller-apples action-delete"><span class="fa fa-lg fa-trash"></span><span class="text">Supprimer</span></a>
                    </td>
                </tr>
                <tr>
                    <td class="data integer positive">8</td>
                    <td class="data string">Azeroli Anisé</td>
                    <td class="data date">27/06/2018</td>
                    <td class="data string">green</td>
                    <td class="data string">Reinette de calibre moyen, à épiderme liégeux strié de pourpre.</td>
                    <td class="action apples view">
                        <a href="' . $root . 'apples/view/8" class="btn btn-sm btn-default controller-apples action-view"><span class="fa fa-lg fa-eye"></span><span class="text">Voir</span></a>
                    </td>
                    <td class="action apples edit">
                        <a href="' . $root . 'apples/edit/8" class="btn btn-sm btn-primary controller-apples action-edit"><span class="fa fa-lg fa-pencil"></span><span class="text">Modifier</span></a>
                    </td>
                    <td class="action apples delete">
                        <a href="' . $root . 'apples/delete/8" class="btn btn-sm btn-danger controller-apples action-delete"><span class="fa fa-lg fa-trash"></span><span class="text">Supprimer</span></a>
                    </td>
                </tr>
            </tbody>
        </table>
        <div class="pagination-block">
            <p class="pagination-counter">Page 2 sur 7, 9 enregistrements sur un total de 62, de l\'enregistrement 21 à 40</p>
            <ul class="pagination">
                <li class="first"><a href="' . $root . 'apples" rel="first">&lt;&lt;</a></li>
                <li class="disabled"><span>&lt;</span></li>
                <li><a href="' . $root . 'apples">1</a></li>
                <li class="active"><span>2</span></li><li><a href="' . $root . 'apples/index/page:3">3</a></li>
                <li><a href="' . $root . 'apples/index/page:4">4</a></li>
                <li><a href="' . $root . 'apples/index/page:5">5</a></li>
                <li><a href="' . $root . 'apples/index/page:6">6</a></li>
                <li><a href="' . $root . 'apples/index/page:7">7</a></li>
                <li class="next"><a href="' . $root . 'apples/index/page:3" rel="next">&gt;</a></li>
                <li class="last"><a href="' . $root . 'apples/index/page:7" rel="last">&gt;&gt;</a></li>
            </ul>
        </div>';
        $this->assertEqualsXhtml($expected, $result);
    }

    /**
     * Test de la méthode LibricielBootstrap3Helper::index() avec aucun
     * enregistrement.
     *
     * @return void
     */
    public function testIndexNoResult()
    {
        $result = $this->LibricielBootstrap3->index(
            [],
            [
                'Apple.id',
                'Apple.name',
                'Apple.date',
                'Apple.category',
                'Apple.description',
                '/Apples/view/#Apple.id#',
                '/Apples/edit/#Apple.id#',
                '/Apples/delete/#Apple.id#',
            ]
        );
        $expected = '<div class="alert alert-info">Aucun résultat</div>';
        $this->assertEqualsXhtml($expected, $result);
    }

    /**
     * Test de la méthode LibricielBootstrap3Helper::index() avec aucun
     * enregistrement.
     *
     * @return void
     */
    public function testIndexNoResultResposive()
    {
        $result = $this->LibricielBootstrap3->index(
            [],
            [
                'Apple.id',
                'Apple.name',
                'Apple.date',
                'Apple.category',
                'Apple.description',
                '/Apples/view/#Apple.id#',
                '/Apples/edit/#Apple.id#',
                '/Apples/delete/#Apple.id#',
            ],
            [
                'responsive' => true,
            ]
        );
        $expected = '<div class="alert alert-info">Aucun résultat</div>';
        $this->assertEqualsXhtml($expected, $result);
    }

    /**
     * Test de la méthode LibricielBootstrap3Helper::subform().
     */
    public function testSubform()
    {
        $result = $this->LibricielBootstrap3->subform(
            [
                'Apple.id',
                'Apple.name',
                'Apple.date',
                'Apple.category',
                'Apple.description',
            ]
        );

        $expected =
        '<input type="hidden" name="data[Apple][id]" id="AppleId"/>
        <div class="input text required form-group required">
            <label for="AppleName" class="control-label col-sm-4">Variété <abbr title="Champ obligatoire" class="required">*</abbr></label>
            <div class="col-sm-8">
                <input name="data[Apple][name]" id="AppleName" maxlength="40" class="form-control" required="required" aria-required="true" type="text"/>
            </div>
        </div>
        <div class="input date form-group">
            <label for="AppleDate" class="control-label col-sm-4">Date</label>
            <div class="col-sm-8">
                <select name="data[Apple][date][day]" id="AppleDateDay" class="form-control">
                    <option value=""></option>
                    ' . $this->cakephpDateOptionsDay(false) . '
                </select>
            </div>-<div class="col-sm-8">
                <select name="data[Apple][date][month]" id="AppleDateMonth" class="form-control">
                    <option value=""></option>
                    ' . $this->cakephpDateOptionsMonth(false) . '
                </select>
            </div>-<div class="col-sm-8">
                <select name="data[Apple][date][year]" id="AppleDateYear" class="form-control">
                    <option value=""></option>
                    ' . $this->cakephpDateOptionsYear(null, null, false) . '
                </select>
            </div>
        </div>
        <div class="input text form-group">
            <label for="AppleCategory" class="control-label col-sm-4">Catégorie</label>
            <div class="col-sm-8">
                <input name="data[Apple][category]" id="AppleCategory" class="form-control" type="text"/>
            </div>
        </div>
        <div class="input text form-group">
            <label for="AppleDescription" class="control-label col-sm-4">Description</label>
            <div class="col-sm-8"><input name="data[Apple][description]" id="AppleDescription" class="form-control" type="text"/></div>
        </div>';
        $this->assertEqualsXhtml($expected, $result);
    }

    /**
     * Test de la méthode LibricielBootstrap3Helper::subform() avec des options.
     */
    public function testSubformWithOptions()
    {
        $result = $this->LibricielBootstrap3->subform(
            [
                'Apple.id',
                'Apple.name',
                'Apple.date',
                'Apple.category',
                'Apple.description',
            ],
            [
                'options' => [
                    'Apple' => [
                        'category' => [
                            1 => 'Catégorie 1',
                            2 => 'Catégorie 2',
                        ],
                    ],
                ],
            ]
        );

        $expected =
        '<input type="hidden" name="data[Apple][id]" id="AppleId"/>
        <div class="input text required form-group required">
            <label for="AppleName" class="control-label col-sm-4">Variété <abbr title="Champ obligatoire" class="required">*</abbr></label>
            <div class="col-sm-8">
                <input name="data[Apple][name]" id="AppleName" maxlength="40" class="form-control" required="required" aria-required="true" type="text"/>
            </div>
        </div>
        <div class="input date form-group">
            <label for="AppleDate" class="control-label col-sm-4">Date</label>
            <div class="col-sm-8">
                    <select name="data[Apple][date][day]" id="AppleDateDay" class="form-control">
                    <option value=""></option>
                    ' . $this->cakephpDateOptionsDay(false) . '
                </select>
            </div>-<div class="col-sm-8">
                <select name="data[Apple][date][month]" id="AppleDateMonth" class="form-control">
                    <option value=""></option>
                    ' . $this->cakephpDateOptionsMonth(false) . '
                </select>
            </div>-<div class="col-sm-8">
                <select name="data[Apple][date][year]" id="AppleDateYear" class="form-control">
                    <option value=""></option>
                    ' . $this->cakephpDateOptionsYear(null, null, false) . '
                </select>
            </div>
        </div>
        <div class="input select form-group">
            <label for="AppleCategory" class="control-label col-sm-4">Catégorie</label>
            <div class="col-sm-8">
                <select name="data[Apple][category]" id="AppleCategory" class="form-control">
                    <option value=""></option>
                    <option value="1">Catégorie 1</option>
                    <option value="2">Catégorie 2</option>
                </select>
            </div>
        </div>
        <div class="input text form-group">
            <label for="AppleDescription" class="control-label col-sm-4">Description</label>
            <div class="col-sm-8"><input name="data[Apple][description]" id="AppleDescription" class="form-control" type="text"/></div>
        </div>';
        $this->assertEqualsXhtml($expected, $result);
    }

    /**
     * Test de la méthode LibricielBootstrap3Helper::form().
     */
    public function testForm()
    {
        $result = $this->LibricielBootstrap3->form(
            [
                'Apple.id',
                'Apple.name',
                'Apple.date',
                'Apple.category',
                'Apple.description',
            ]
        );

        $expected =
        '<form action="/" novalidate="novalidate" class="form-horizontal" id="Form" method="post" accept-charset="utf-8">
            <div style="display:none;"><input type="hidden" name="_method" value="POST"/></div>
            <input type="hidden" name="data[Apple][id]" id="AppleId"/>
            <div class="input text required form-group required">
                <label for="AppleName" class="control-label col-sm-4">Variété <abbr title="Champ obligatoire" class="required">*</abbr></label>
                <div class="col-sm-8">
                    <input name="data[Apple][name]" id="AppleName" maxlength="40" class="form-control" required="required" aria-required="true" type="text"/>
                </div>
            </div>
            <div class="input date form-group">
                <label for="AppleDate" class="control-label col-sm-4">Date</label>
                <div class="col-sm-8">
                    <select name="data[Apple][date][day]" id="AppleDateDay" class="form-control">
                        <option value=""></option>
                        ' . $this->cakephpDateOptionsDay(false) . '
                    </select>
                </div>-<div class="col-sm-8">
                    <select name="data[Apple][date][month]" id="AppleDateMonth" class="form-control">
                        <option value=""></option>
                        ' . $this->cakephpDateOptionsMonth(false) . '
                    </select>
                </div>-<div class="col-sm-8">
                    <select name="data[Apple][date][year]" id="AppleDateYear" class="form-control">
                        <option value=""></option>
                        ' . $this->cakephpDateOptionsYear(null, null, false) . '
                    </select>
                </div>
            </div>
            <div class="input text form-group">
                <label for="AppleCategory" class="control-label col-sm-4">Catégorie</label>
                <div class="col-sm-8">
                    <input name="data[Apple][category]" id="AppleCategory" class="form-control" type="text"/>
                </div>
            </div>
            <div class="input text form-group">
                <label for="AppleDescription" class="control-label col-sm-4">Description</label>
                <div class="col-sm-8"><input name="data[Apple][description]" id="AppleDescription" class="form-control" type="text"/></div>
            </div>
            <div class="btn-group col-sm-6 col-sm-offset-4">
                <button name="_action" value="Cancel" class="btn btn-default" type="submit">
                    <span class="fa fa-lg fa-times-circle"></span> Annuler</button>
                <button name="_action" value="Save" class="btn btn-primary" type="submit">
                    <span class="fa fa-lg fa-floppy-o"></span> Enregistrer</button>
            </div>
        </form>';
        $this->assertEqualsXhtml($expected, $result);
    }

    /**
     * Test de la méthode LibricielBootstrap3Helper::actions().
     *
     * @return void
     */
    public function testActions()
    {
        $result = $this->LibricielBootstrap3->actions(
            [
                '/Apples/add' => ['title' => true],
            ]
        );
        $expected =
        '<div class="btn-group">
            <a href="/apples/add" title="Ajouter une pomme" class="btn btn-sm btn-success controller-apples action-add">
                <span class="fa fa-lg fa-plus-circle"></span><span class="text">Ajouter</span>
            </a>
        </div>';
        $this->assertEqualsXhtml($expected, $result);
    }

    /**
     * Test de la méthode LibricielBootstrap3Helper::actions() avec une
     * normalisation des actions.
     *
     * @return void
     */
    public function testActionsNormalized()
    {
        $result = $this->LibricielBootstrap3->actions(['/Apples/add']);
        $expected =
        '<div class="btn-group">
            <a href="/apples/add" class="btn btn-sm btn-success controller-apples action-add">
                <span class="fa fa-lg fa-plus-circle"></span><span class="text">Ajouter</span>
            </a>
        </div>';
        $this->assertEqualsXhtml($expected, $result);
    }

    /**
     * Test de la méthode LibricielBootstrap3Helper::actions() sans action.
     *
     * @return void
     */
    public function testActionsNone()
    {
        $result = $this->LibricielBootstrap3->actions([]);
        $expected = null;
        $this->assertEqualsXhtml($expected, $result);
    }

    /**
     * Test de la méthode LibricielBootstrap3Helper::view().
     *
     * @return void
     */
    public function testView()
    {
        $record = [
            'Producer' => [
                'id' => 123456,
                'name' => 'L\'Alhambra de "Germania"',
                'birthday' => '1975-05-04',
                'postcode' => '012345',
                'phone' => '0102030405',
                'email' => 'info@alhabmra-de-germania.com',
                'url' => 'http://www.alhabmra-de-germania.com/',
                'vat' => 'FR76491011698',
                'description' => 'Lorem ipsum...',
            ],
        ];
        $fields = [
            'Producer.id' => [
                'data-format' => 'integer',
            ],
            'Producer.name',
            'Producer.birthday' => [
                'type' => 'date',
                'data-format' => 'date',
            ],
            'Producer.phone' => [
                'data-format' => 'phone',
            ],
            'Producer.email',
            'Producer.url',
            'Producer.vat' => [
                'data-format' => 'vat',
            ],
            'Producer.description',
        ];
        $params = [];
        $result = $this->LibricielBootstrap3->view($record, $fields, $params);
        $expected =
        '<div class="form-horizontal">
            <div class="input staticctrl form-group">
                <p class="control-label col-sm-4"><strong>Producer.id</strong></p>
                <p class="col-sm-8 form-control-static text">123 456</p>
            </div>
            <div class="input staticctrl form-group">
                <p class="control-label col-sm-4"><strong>Nom</strong></p>
                <p class="col-sm-8 form-control-static text">L&#039;Alhambra de &quot;Germania&quot;</p>
            </div>
            <div class="input staticctrl form-group">
                <p class="control-label col-sm-4"><strong>Producer.birthday</strong></p>
                <p class="col-sm-8 form-control-static text">04/05/1975</p>
            </div>
            <div class="input staticctrl form-group">
                <p class="control-label col-sm-4"><strong>Producer.phone</strong></p>
                <p class="col-sm-8 form-control-static tel">01 02 03 04 05</p>
            </div>
            <div class="input staticctrl form-group">
                <p class="control-label col-sm-4"><strong>Producer.email</strong></p>
                <p class="col-sm-8 form-control-static email">info@alhabmra-de-germania.com</p>
            </div>
            <div class="input staticctrl form-group">
                <p class="control-label col-sm-4"><strong>Producer.url</strong></p>
                <p class="col-sm-8 form-control-static text">http://www.alhabmra-de-germania.com/</p>
            </div>
            <div class="input staticctrl form-group">
                <p class="control-label col-sm-4"><strong>Producer.vat</strong></p>
                <p class="col-sm-8 form-control-static text">FR 76 491 011 698</p>
            </div>
            <div class="input staticctrl form-group">
                <p class="control-label col-sm-4"><strong>Producer.description</strong></p>
                <p class="col-sm-8 form-control-static text">Lorem ipsum...</p>
            </div>
        </div>';
        $this->assertEqualsXhtml($expected, $result);
    }

    public function testSubview()
    {
        $record = [
            'Apple' => [
                'id' => 6,
                'name' => 'Pink Lady',
                'date' => '2014-03-17',
                'category' => 'red',
                'description' => "Line 1\nLine2",
            ],
        ];
        $fields = [
            'Apple.id',
            'Apple.name',
            'Apple.date',
            'Apple.category',
            'Apple.description' => ['class' => 'required'],
        ];
        $params = [];
        $result = $this->LibricielBootstrap3->view($record, $fields, $params);
        $expected =
        '<div class="form-horizontal">
            <div class="input staticctrl form-group">
                <p class="control-label col-sm-4"><strong>Id</strong></p>
                <p class="col-sm-8 form-control-static text">6</p>
            </div>
            <div class="input staticctrl form-group">
                <p class="control-label col-sm-4"><strong>Variété</strong></p>
                <p class="col-sm-8 form-control-static text">Pink Lady</p>
            </div>
            <div class="input staticctrl form-group">
                <p class="control-label col-sm-4"><strong>Date</strong></p>
                <p class="col-sm-8 form-control-static date">17/03/2014</p>
            </div>
            <div class="input staticctrl form-group">
                <p class="control-label col-sm-4"><strong>Catégorie</strong></p>
                <p class="col-sm-8 form-control-static text">red</p>
            </div>
            <div class="input staticctrl form-group">
                <p class="control-label col-sm-4"><strong>Description</strong></p>
                <p class="required col-sm-8 form-control-static text">Line 1 Line2</p>
            </div>
        </div>';
        $this->assertEqualsXhtml($expected, $result);
    }

    /**
     * Test de la méthode LibricielBootstrap3Helper::pagination() en étant sur
     * la seconde page sur 7 pages de résultats..
     *
     * @return void
     */
    public function testPaginationPage1Of1()
    {
        $this->_setRequest(static::$requestsParams['page_1_of_1']);
        $root = Router::url('/');

        $result = $this->LibricielBootstrap3->pagination();
        $expected = '
        <div class="pagination-block">
            <p class="pagination-counter">Page 1 sur 1, 2 enregistrements sur un total de 2, de l\'enregistrement 1 à 2</p>
        </div>';
        $this->assertEqualsXhtml($expected, $result);
    }

    /**
     * Test de la méthode LibricielBootstrap3Helper::pagination() en étant sur
     * la seconde page sur 7 pages de résultats.
     *
     * @return void
     */
    public function testPaginationPage2Of7()
    {
        $this->_setRequest(static::$requestsParams['page_2_of_7']);
        $root = Router::url('/');

        $result = $this->LibricielBootstrap3->pagination();
        $expected = '
        <div class="pagination-block">
            <p class="pagination-counter">Page 2 sur 7, 9 enregistrements sur un total de 62, de l\'enregistrement 21 à 40</p>
            <ul class="pagination">
                <li class="first"><a href="' . $root . 'apples" rel="first">&lt;&lt;</a></li>
                <li class="disabled"><span>&lt;</span></li><li><a href="' . $root . 'apples">1</a></li>
                <li class="active"><span>2</span></li><li><a href="' . $root . 'apples/index/page:3">3</a></li>
                <li><a href="' . $root . 'apples/index/page:4">4</a></li>
                <li><a href="' . $root . 'apples/index/page:5">5</a></li>
                <li><a href="' . $root . 'apples/index/page:6">6</a></li>
                <li><a href="' . $root . 'apples/index/page:7">7</a></li>
                <li class="next"><a href="' . $root . 'apples/index/page:3" rel="next">&gt;</a></li>
                <li class="last"><a href="' . $root . 'apples/index/page:7" rel="last">&gt;&gt;</a></li>
            </ul>
        </div>';
        $this->assertEqualsXhtml($expected, $result);
    }

    /**
     * Test de la méthode LibricielBootstrap3Helper::pagination() en étant sur
     * la seconde page sur 7 pages de résultats, avec les titres des liens.
     *
     * @return void
     */
    public function testPaginationPage2Of7WithTitles()
    {
        $this->_setRequest(static::$requestsParams['page_2_of_7']);
        $root = Router::url('/');

        $result = $this->LibricielBootstrap3->pagination(['title' => true]);
        $expected = '
        <div class="pagination-block">
            <p class="pagination-counter">Page 2 sur 7, 9 enregistrements sur un total de 62, de l\'enregistrement 21 à 40</p>
            <ul class="pagination">
                <li class="first"><a href="' . $root . 'apples" title="Première page des résultats" rel="first">&lt;&lt;</a></li>
                <li title="Page précédente des résultats" class="disabled"><span>&lt;</span></li>
                <li><a href="' . $root . 'apples" title="Page 1">1</a></li>
                <li class="active"><span>2</span></li>
                <li><a href="' . $root . 'apples/index/page:3" title="Page 3">3</a></li>
                <li><a href="' . $root . 'apples/index/page:4" title="Page 4">4</a></li>
                <li><a href="' . $root . 'apples/index/page:5" title="Page 5">5</a></li>
                <li><a href="' . $root . 'apples/index/page:6" title="Page 6">6</a></li>
                <li><a href="' . $root . 'apples/index/page:7" title="Page 7">7</a></li>
                <li class="next"><a href="' . $root . 'apples/index/page:3" title="Page suivante des résultats" rel="next">&gt;</a></li>
                <li class="last"><a href="' . $root . 'apples/index/page:7" title="Dernière page des résultats" rel="last">&gt;&gt;</a></li>
            </ul>
        </div>';
        $this->assertEqualsXhtml($expected, $result);
    }
}
