<?php
/**
 * Code source de la classe LibricielBootstrap3FormHelperTest.
 *
 * @package LibricielBootstrap3
 * @subpackage Test.Case.View.Helper
 */
App::uses('View', 'View');
App::uses('AppHelper', 'View/Helper');
App::uses('CakeRequest', 'Network');
App::uses('LibricielBootstrap3AbstractTestCase', 'LibricielBootstrap3.Test/Case');
App::uses('LibricielBootstrap3Data', 'LibricielBootstrap3.Utility');
App::uses('LibricielBootstrap3FormHelper', 'LibricielBootstrap3.View/Helper');

/**
 * La classe LibricielBootstrap3FormHelperTest ...
 *
 * @covers LibricielBootstrap3FormHelper
 *
 * @package LibricielBootstrap3
 * @subpackage Test.Case.View.Helper
 */
class LibricielBootstrap3FormHelperTest extends LibricielBootstrap3AbstractTestCase
{
    /**
     * Fixtures utilisés par ces tests unitaires.
     *
     * @var array
     */
    public $fixtures = [
        'core.Apple',
        'core.DataTest',
    ];

    /**
     * Modèle Apple
     *
     * @var Model
     */
    public $Apple = null;

    /**
     * Modèle DataTest
     *
     * @var Model
     */
    public $DataTest = null;

    /**
     * Préparation du test.
     */
    public function setUp()
    {
        parent::setUp();

        $this->DataTest = ClassRegistry::init('DataTest');
        $this->Apple = ClassRegistry::init('Apple');

        $controller = null;
        $this->View = new View($controller);
        $this->LibricielBootstrap3Form = new LibricielBootstrap3FormHelper($this->View);

        $this->LibricielBootstrap3Form->request = new CakeRequest('apples/add', false);
        $this->LibricielBootstrap3Form->request->here = '/apples/add';
        $this->LibricielBootstrap3Form->request['controller'] = 'apples';
        $this->LibricielBootstrap3Form->request['action'] = 'add';
        $this->LibricielBootstrap3Form->request->webroot = '';
        $this->LibricielBootstrap3Form->request->base = '';

        $this->LibricielBootstrap3Form->request->data = [
            'Apple' => [
                'id' => 6,
                'name' => 'Pink Lady',
                'date' => '2014-03-17',
                'category' => 'red',
                'description' => "Line 1\nLine2",
            ],
        ];
    }

    /**
     * Nettoyage postérieur au test.
     */
    public function tearDown()
    {
        parent::tearDown();
        unset($this->View, $this->LibricielBootstrap3Form, $this->DataTest, $this->Apple);
    }

    /**
     * Test de la méthode LibricielBootstrap3FormHelper::buttons().
     */
    public function testButtons()
    {
        // 1. Sans bouton
        $actual = $this->LibricielBootstrap3Form->buttons();
        $this->assertNull($actual);

        // 2. Avec des boutons
        $actual = $this->LibricielBootstrap3Form->buttons(['Cancel', 'Reset', 'Save']);
        $expected =
        '<div class="btn-group col-sm-6 col-sm-offset-4">
            <button name="_action" value="Cancel" class="btn btn-default" type="submit">
                <span class="fa fa-lg fa-times-circle"></span> Annuler</button>
            <button name="_action" value="Reset" class="btn btn-default" type="reset">
                <span class="fa fa-lg fa-undo"></span> Reset</button>
            <button name="_action" value="Save" class="btn btn-primary" type="submit">
                <span class="fa fa-lg fa-floppy-o"></span> Enregistrer</button>
        </div>';
        $this->assertEqualsXhtml($expected, $actual);
    }

    /**
     * Test de la méthode LibricielBootstrap3FormHelper::buttons() après avoir
     * configuré une classe de boutons supplémentaire.
     */
    public function testButtonsWithAdditionalConfiguredButton()
    {
        $this->LibricielBootstrap3Form->config(
            [
                'buttons' => [
                    'Add' => [
                        'class' => 'btn btn-default',
                        'icon-classes' => 'fa fa-lg fa-plus-circle',
                    ],
                ],
            ]
        );

        $actual = $this->LibricielBootstrap3Form->buttons(['Add', 'Reset', 'Save']);
        $expected =
        '<div class="btn-group col-sm-6 col-sm-offset-4">
            <button name="_action" value="Add" class="btn btn-default" type="submit">
                <span class="fa fa-lg fa-plus-circle"></span> Add</button>
            <button name="_action" value="Reset" class="btn btn-default" type="reset">
                <span class="fa fa-lg fa-undo"></span> Reset</button>
            <button name="_action" value="Save" class="btn btn-primary" type="submit">
                <span class="fa fa-lg fa-floppy-o"></span> Enregistrer</button>
        </div>';
        $this->assertEqualsXhtml($expected, $actual);
    }

    /**
     * Test de la méthode LibricielBootstrap3FormHelper::buttons() après avoir
     * modifié les classes de boutons et des icônes.
     */
    public function testButtonsWithChangedConfiguredButtons()
    {
        $this->LibricielBootstrap3Form->config(
            [
                'buttons' => [
                    'Reset' => [
                        'class' => 'btn btn-warning',
                    ],
                    'Save' => [
                        'icon-classes' => 'fa fa-lg fa-plus-square-o',
                    ],
                ],
            ]
        );

        $actual = $this->LibricielBootstrap3Form->buttons(['Reset', 'Save']);
        $expected =
        '<div class="btn-group col-sm-6 col-sm-offset-4">
            <button name="_action" value="Reset" class="btn btn-warning" type="reset">
                <span class="fa fa-lg fa-undo"></span> Reset</button>
            <button name="_action" value="Save" class="btn btn-primary" type="submit">
                <span class="fa fa-lg fa-plus-square-o"></span> Enregistrer</button>
        </div>';
        $this->assertEqualsXhtml($expected, $actual);
    }

    /**
     * Test de la méthode LibricielBootstrap3FormHelper::buttons() après avoir
     * modifié les classes de boutons et des icônes et en ajoutant un lien
     */
    public function testButtonsWithChangedConfiguredButtonsWithLink()
    {
        $this->LibricielBootstrap3Form->config(
            [
                'buttons' => [
                    'Connexion' => [
                        'class' => 'btn btn-primary',
                        'icon-classes' => 'fa fa-sign-in fa-fw',
                    ],
                    'ForgottenPassword' => [
                        'class' => 'btn btn-link',
                        'icon-classes' => null,
                    ],
                ],
            ]
        );

        $actual = $this->LibricielBootstrap3Form->buttons(
            [
                'ForgottenPassword' => [
                    'type' => 'link',
                    'url' => ['controller' => 'users', 'action' => 'forgotten_password'],
                ],
                'Connexion',
            ]
        );
        $expected =
        '<div class="btn-group col-sm-6 col-sm-offset-4">
            <a href="/users/forgotten_password" class="btn btn-link">
                <span class=""></span> Mot de passe oublié ?</a>
            <button name="_action" value="Connexion" class="btn btn-primary" type="submit">
                <span class="fa fa-sign-in fa-fw"></span> Se connecter</button>
        </div>';
        $this->assertEqualsXhtml($expected, $actual);
    }

    /**
     * Test de la méthode LibricielBootstrap3FormHelper::create().
     */
    public function testCreate()
    {
        $actual = $this->LibricielBootstrap3Form->create();
        $expected =
        '<form action="/apples/add" novalidate="novalidate" class="form-horizontal" id="addForm" method="post" accept-charset="utf-8">
            <div style="display:none;"><input type="hidden" name="_method" value="POST"/></div>';
        $this->assertEqualsXhtml($expected, $actual);
    }

    /**
     * Test de la méthode LibricielBootstrap3FormHelper::error().
     */
    public function testError()
    {
        $this->LibricielBootstrap3Form->validationErrors = [
            'Apple' => [
                'name' => [
                    'Champ obligatoire',
                ],
            ],
        ];

        $actual = $this->LibricielBootstrap3Form->error('Apple.name');
        $expected =
        '<div class="error-message-wrapper col-sm-8 col-sm-offset-4">
            <div id="AppleNameErrorMessage" class="error-message help-block">Champ obligatoire</div>
        </div>';
        $this->assertEqualsXhtml($expected, $actual);
    }

    /**
     * Test de la méthode LibricielBootstrap3FormHelper::inputs().
     */
    public function testInputs()
    {
        $fields = [
            'Apple.id',
            'Apple.name',
        ];
        $actual = $this->LibricielBootstrap3Form->inputs($fields);
        $expected =
        '<input type="hidden" name="data[Apple][id]" id="AppleId" value="6"/>
        <div class="input text required form-group required">
            <label for="AppleName" class="control-label col-sm-4">Variété <abbr title="Champ obligatoire" class="required">*</abbr></label>
            <div class="col-sm-8">
                <input name="data[Apple][name]" id="AppleName" maxlength="40" class="form-control" required="required" aria-required="true" type="text" value="Pink Lady"/>
            </div>
        </div>';
        $this->assertEqualsXhtml($expected, $actual);
    }

    /**
     * Test de la méthode LibricielBootstrap3FormHelper::inputs() avec des
     * attributs spéciaux pour un champ.
     */
    public function testInputsWithFieldAttributes()
    {
        $fields = [
            'Apple.id',
            'Apple.name' => [
                'placeholder' => true,
                'required' => true,
                'help-block' => true,
            ],
        ];
        $actual = $this->LibricielBootstrap3Form->inputs($fields);
        $expected =
        '<input type="hidden" name="data[Apple][id]" id="AppleId" value="6"/>
        <div class="input text required form-group required">
            <label for="AppleName" class="control-label col-sm-4">Variété <abbr title="Champ obligatoire" class="required">*</abbr></label>
            <div class="col-sm-8">
                <input name="data[Apple][name]" id="AppleName" placeholder="William&#039;s Pride" required="required" maxlength="40" class="form-control" aria-describedby="AppleNameHelpBlock" aria-required="true" type="text" value="Pink Lady"/>
                <span class="help-block" id="AppleNameHelpBlock">Votre variété de pommes préférée</span>
            </div>
        </div>';
        $this->assertEqualsXhtml($expected, $actual);
    }

    /**
     * Test de la méthode LibricielBootstrap3FormHelper::input() avec un champ
     * en erreur et un help-block.
     */
    public function testInputWithError()
    {
        $this->LibricielBootstrap3Form->validationErrors = [
            'Apple' => [
                'name' => [
                    'Champ obligatoire',
                ],
            ],
        ];

        $actual = $this->LibricielBootstrap3Form->input('Apple.name', ['help-block' => 'Ceci est un test']);
        $expected =
        '<div class="input text required has-error form-group required error">
            <label for="AppleName" class="control-label col-sm-4">Variété <abbr title="Champ obligatoire" class="required">*</abbr></label>
            <div class="col-sm-8">
                <input name="data[Apple][name]" id="AppleName" maxlength="40" aria-describedby="AppleNameErrorMessage AppleNameHelpBlock" aria-invalid="true" class="form-control form-error" required="required" aria-required="true" type="text" value="Pink Lady"/>
                <span class="help-block" id="AppleNameHelpBlock">Ceci est un test</span>
            </div>
            <div class="error-message-wrapper col-sm-8 col-sm-offset-4">
                <div id="AppleNameErrorMessage" class="error-message help-block">Champ obligatoire</div>
            </div>
        </div>';
        $this->assertEqualsXhtml($expected, $actual);
    }

    /**
     * Test de la méthode LibricielBootstrap3FormHelper::date().
     */
    public function testDate()
    {
        $this->LibricielBootstrap3Form->request->data = [];

        // 1. Sans paramètre supplémentaire
        $actual = $this->LibricielBootstrap3Form->input('Apple.date');
        $expected =
        '<div class="input date form-group">
            <label for="AppleDate" class="control-label col-sm-4">Date</label>
            <div class="col-sm-8">
                <select name="data[Apple][date][day]" id="AppleDateDay" class="form-control">
                    <option value=""></option>
                    ' . $this->cakephpDateOptionsDay(false) . '
                </select>
            </div>-<div class="col-sm-8">
                <select name="data[Apple][date][month]" id="AppleDateMonth" class="form-control">
                    <option value=""></option>
                    ' . $this->cakephpDateOptionsMonth(false) . '
                </select>
            </div>-<div class="col-sm-8">
                <select name="data[Apple][date][year]" id="AppleDateYear" class="form-control">
                    <option value=""></option>
                    ' . $this->cakephpDateOptionsYear(null, null, false) . '
                </select>
            </div>
        </div>';
        $this->assertEqualsXhtml($expected, $actual);

        // 2. Avec tous les paramètre supplémentaires possibles
/*
timeFormat
interval
round
*/
        $options = [
            'minYear' => 2010,
            'maxYear' => 2015,
            'dateFormat' => 'MDY',
            //'orderYear' => 'desc' // @info: uniquement pour la méthode year apparemment
        ];
        $actual = $this->LibricielBootstrap3Form->input('Apple.date', $options);
        $expected =
        '<div class="input date form-group">
            <label for="AppleDate" class="control-label col-sm-4">Date</label>
            <div class="col-sm-8">
                <select name="data[Apple][date][month]" id="AppleDateMonth" class="form-control">
                    <option value=""></option>
                    ' . $this->cakephpDateOptionsMonth(false) . '
                </select>
            </div>-<div class="col-sm-8">
                <select name="data[Apple][date][day]" id="AppleDateDay" class="form-control">
                    <option value=""></option>
                    ' . $this->cakephpDateOptionsDay(false) . '
                </select>
            </div>-<div class="col-sm-8">
                <select name="data[Apple][date][year]" id="AppleDateYear" class="form-control">
                    <option value=""></option>
                    ' . $this->cakephpDateOptionsYear(2010, 2015, false) . '
                </select>
            </div>
        </div>';
        $this->assertEqualsXhtml($expected, $actual);
    }

    /**
     * Test de la méthode LibricielBootstrap3FormHelper::input() avec un type "date"
     * et le mode "html5".
     */
    public function testDateHtml5()
    {
        $this->LibricielBootstrap3Form->request->data = [];

        // 1. Sans paramètre supplémentaire
        $actual = $this->LibricielBootstrap3Form->input('Apple.date', ['mode' => 'html5']);
        $expected =
        '<div class="input date form-group">
            <label for="AppleDate" class="control-label col-sm-4">Date</label>
            <div class="col-sm-8">
                <input name="data[Apple][date]" type="date" id="AppleDate" class="form-control"/>
            </div>
        </div>';
        $this->assertEqualsXhtml($expected, $actual);

        // 2. Avec tous les paramètre supplémentaires possibles
        $options = [
            'minYear' => 2010,
            'maxYear' => 2015,
            'mode' => 'html5',
        ];
        $actual = $this->LibricielBootstrap3Form->input('Apple.date', $options);
        $expected =
        '<div class="input date form-group">
            <label for="AppleDate" class="control-label col-sm-4">Date</label>
            <div class="col-sm-8">
                <input name="data[Apple][date]" type="date" id="AppleDate" min="2010-01-01" max="2015-12-31" class="form-control"/>
            </div>
        </div>';
        $this->assertEqualsXhtml($expected, $actual);
    }

    /**
     * Test de la méthode LibricielBootstrap3FormHelper::label().
     */
    public function testLabel()
    {
        // Avec les classes par défaut
        $actual = $this->LibricielBootstrap3Form->label('Apple.name');
        $expected = '<label for="AppleName" class="control-label col-sm-4">Name</label>';
        $this->assertEqualsXhtml($expected, $actual);

        // Avec des classes spécifiques
        $actual = $this->LibricielBootstrap3Form->label('Apple.name', null, ['class' => 'foo-bar-baz']);
        $expected = '<label for="AppleName" class="foo-bar-baz control-label col-sm-4">Name</label>';
        $this->assertEqualsXhtml($expected, $actual, var_export($actual, true));
    }

    /**
     * Test de la méthode LibricielBootstrap3FormHelper::input() avec le type
     * "time".
     */
    public function testTime()
    {
        $this->LibricielBootstrap3Form->request->data = [];

        // 1. Sans paramètre supplémentaire
        $actual = $this->LibricielBootstrap3Form->input('Apple.time', ['type' => 'time']);
        $expected =
        '<div class="input time form-group">
            <label for="AppleTime" class="control-label col-sm-4">Apple.time</label>
            <div class="col-sm-8">
                <select name="data[Apple][time][hour]" id="AppleTimeHour" class="form-control">
                    <option value=""></option>
                    ' . $this->cakephpTimeOptionsHour(false) . '
                </select>
            </div>:<div class="col-sm-8">
                <select name="data[Apple][time][min]" id="AppleTimeMinute" class="form-control">
                    <option value=""></option>
                    ' . $this->cakephpTimeOptionsMinute(false) . '
                </select>
            </div>
        </div>';
        $this->assertEqualsXhtml($expected, $actual);

        // 2. Avec des paramètre supplémentaires
        $options = [
            'type' => 'time',
            'interval' => 15,
        ];
        $actual = $this->LibricielBootstrap3Form->input('Apple.time', $options);
        $expected =
        '<div class="input time form-group">
            <label for="AppleTime" class="control-label col-sm-4">Apple.time</label>
            <div class="col-sm-8">
                <select name="data[Apple][time][hour]" id="AppleTimeHour" class="form-control">
                    <option value=""></option>
                    ' . $this->cakephpTimeOptionsHour(false) . '
                </select>
            </div>:<div class="col-sm-8">
                <select name="data[Apple][time][min]" id="AppleTimeMinute" class="form-control">
                    <option value=""></option>
                    ' . $this->cakephpTimeOptionsMinute(false, 15) . '
                </select>
            </div>
        </div>';
        $this->assertEqualsXhtml($expected, $actual);
    }

    /**
     * Test de la méthode LibricielBootstrap3FormHelper::input() avec le type
     * "time" et le mode "html5".
     */
    public function testTimeHtml5()
    {
        $this->LibricielBootstrap3Form->request->data = [];

        // 1. Sans paramètre supplémentaire
        $params = ['type' => 'time', 'mode' => 'html5'];
        $actual = $this->LibricielBootstrap3Form->input('Apple.time', $params);
        $expected =
        '<div class="input time form-group">
            <label for="AppleTime" class="control-label col-sm-4">Apple.time</label>
            <div class="col-sm-8">
                <input name="data[Apple][time]" type="time" id="AppleTime" class="form-control"/>
            </div>
        </div>';
        $this->assertEqualsXhtml($expected, $actual);

        // 2. Avec des paramètre supplémentaires
        $params = ['type' => 'time', 'mode' => 'html5', 'interval' => 15];
        $actual = $this->LibricielBootstrap3Form->input('Apple.time', $params);
        $expected =
        '<div class="input time form-group">
            <label for="AppleTime" class="control-label col-sm-4">Apple.time</label>
            <div class="col-sm-8">
                <input name="data[Apple][time]" type="time" id="AppleTime" step="900" class="form-control"/>
            </div>
        </div>';
        $this->assertEqualsXhtml($expected, $actual);
    }

    /**
     * Test de la méthode LibricielBootstrap3FormHelper::input() avec le type
     * "datetime".
     */
    public function testDatetime()
    {
        $this->LibricielBootstrap3Form->request->data = [];

        // 1. Sans paramètre supplémentaire
        $actual = $this->LibricielBootstrap3Form->input('Apple.date', ['type' => 'datetime']);
        $expected =
        '<div class="input datetime form-group">
            <label for="AppleDate" class="control-label col-sm-4">Date</label>
            <div class="col-sm-8">
                <select name="data[Apple][date][day]" id="AppleDateDay" class="form-control">
                    <option value=""></option>
                    ' . $this->cakephpDateOptionsDay(false) . '
                </select>
            </div>-<div class="col-sm-8">
                <select name="data[Apple][date][month]" id="AppleDateMonth" class="form-control">
                    <option value=""></option>
                    ' . $this->cakephpDateOptionsMonth(false) . '
                </select>
            </div>-<div class="col-sm-8">
                <select name="data[Apple][date][year]" id="AppleDateYear" class="form-control">
                    <option value=""></option>
                    ' . $this->cakephpDateOptionsYear(null, null, false) . '
                </select>
            </div> <div class="col-sm-8">
                <select name="data[Apple][date][hour]" id="AppleDateHour" class="form-control">
                    <option value=""></option>
                    ' . $this->cakephpTimeOptionsHour(false) . '
                </select>
            </div>:<div class="col-sm-8">
                <select name="data[Apple][date][min]" id="AppleDateMinute" class="form-control">
                    <option value=""></option>
                    ' . $this->cakephpTimeOptionsMinute(false) . '
                </select>
            </div>
        </div>';
        $this->assertEqualsXhtml($expected, $actual);

        // 2. Avec des paramètre supplémentaires
        $options = [
            'type' => 'datetime',
            'minYear' => 2010,
            'maxYear' => 2015,
            'dateFormat' => 'MDY',
            'interval' => 15,
        ];
        $actual = $this->LibricielBootstrap3Form->input('Apple.date', $options);
        $expected =
        '<div class="input datetime form-group">
            <label for="AppleDate" class="control-label col-sm-4">Date</label>
            <div class="col-sm-8">
                <select name="data[Apple][date][month]" id="AppleDateMonth" class="form-control">
                    <option value=""></option>
                    ' . $this->cakephpDateOptionsMonth(false) . '
                </select>
            </div>-<div class="col-sm-8">
                <select name="data[Apple][date][day]" id="AppleDateDay" class="form-control">
                    <option value=""></option>
                    ' . $this->cakephpDateOptionsDay(false) . '
                </select>
            </div>-<div class="col-sm-8">
                <select name="data[Apple][date][year]" id="AppleDateYear" class="form-control">
                    <option value=""></option>
                    ' . $this->cakephpDateOptionsYear(2010, 2015, false) . '
                </select>
            </div> <div class="col-sm-8">
                <select name="data[Apple][date][hour]" id="AppleDateHour" class="form-control">
                    <option value=""></option>
                    ' . $this->cakephpTimeOptionsHour(false) . '
                </select>
            </div>:<div class="col-sm-8">
                <select name="data[Apple][date][min]" id="AppleDateMinute" class="form-control">
                    <option value=""></option>
                    ' . $this->cakephpTimeOptionsMinute(false, 15) . '
                </select>
            </div>
        </div>';
        $this->assertEqualsXhtml($expected, $actual);
    }

    /**
     * Test de la méthode LibricielBootstrap3FormHelper::input() avec le type
     * "datetime" et le mode "html5".
     */
    public function testDatetimeHtml5()
    {
        $this->LibricielBootstrap3Form->request->data = [];

        // 1. Sans paramètre supplémentaire
        $params = ['type' => 'datetime', 'mode' => 'html5'];
        $actual = $this->LibricielBootstrap3Form->input('Apple.date', $params);
        $expected =
        '<div class="input datetime form-group">
            <label for="AppleDate" class="control-label col-sm-4">Date</label>
            <div class="col-sm-8">
                <input name="data[Apple][date]" type="datetime-local" id="AppleDate" class="form-control"/>
            </div>
        </div>';
        $this->assertEqualsXhtml($expected, $actual);

        // 2. Avec des paramètre supplémentaires
        $params = [
            'type' => 'datetime',
            'mode' => 'html5',
            'minYear' => 2010,
            'maxYear' => 2015,
            'interval' => 15,
        ];
        $actual = $this->LibricielBootstrap3Form->input('Apple.date', $params);
        $expected =
        '<div class="input datetime form-group">
            <label for="AppleDate" class="control-label col-sm-4">Date</label>
            <div class="col-sm-8">
                <input name="data[Apple][date]" type="datetime-local" id="AppleDate" min="2010-01-01" max="2015-12-31" step="900" class="form-control"/>
            </div>
        </div>';
        $this->assertEqualsXhtml($expected, $actual);
    }

    /**
     * Test de la méthode LibricielBootstrap3FormHelper::input() avec un type file.
     */
    public function testInputFile()
    {
        $actual = $this->LibricielBootstrap3Form->input('Apple.attachment', ['type' => 'file']);
        $expected =
        '<div class="input file form-group">
            <label for="AppleAttachment" class="control-label col-sm-4">Apple.attachment</label>
            <div class="col-sm-8">
                <input type="file" name="data[Apple][attachment]" id="AppleAttachment" class="form-control"/>
            </div>
        </div>';
        $this->assertEqualsXhtml($expected, $actual);
    }

    /**
     * Test de la méthode LibricielBootstrap3FormHelper::input() avec un type text.
     */
    public function testInputText()
    {
        // Avec la valeur de required déduite (true)
        $actual = $this->LibricielBootstrap3Form->input('Apple.name');
        $expected =
        '<div class="input text required form-group required">
            <label for="AppleName" class="control-label col-sm-4">Variété <abbr title="Champ obligatoire" class="required">*</abbr></label>
            <div class="col-sm-8">
                <input name="data[Apple][name]" id="AppleName" maxlength="40" class="form-control" required="required" aria-required="true" type="text" value="Pink Lady"/>
            </div>
        </div>';
        $this->assertEqualsXhtml($expected, $actual);

        // Avec la valeur de required explicitée (false)
        $actual = $this->LibricielBootstrap3Form->input('Apple.name', ['required' => false]);
        $expected =
        '<div class="input text form-group">
            <label for="AppleName" class="control-label col-sm-4">Variété</label>
            <div class="col-sm-8">
                <input name="data[Apple][name]" id="AppleName" maxlength="40" class="form-control" type="text" value="Pink Lady"/>
            </div>
        </div>';
        $this->assertEqualsXhtml($expected, $actual);

        // Avec la valeur de required déduite (false)
        $actual = $this->LibricielBootstrap3Form->input('Apple.time');
        $expected =
        '<div class="input text form-group">
            <label for="AppleTime" class="control-label col-sm-4">Apple.time</label>
            <div class="col-sm-8">
                <input name="data[Apple][time]" id="AppleTime" class="form-control" type="text"/>
            </div>
        </div>';
        $this->assertEqualsXhtml($expected, $actual);

        // Avec la valeur de required explicitée (true)
        $actual = $this->LibricielBootstrap3Form->input('Apple.time', ['required' => true]);
        $expected =
        '<div class="input text form-group">
            <label for="AppleTime" class="control-label col-sm-4">Apple.time <abbr title="Champ obligatoire" class="required">*</abbr></label>
            <div class="col-sm-8">
                <input name="data[Apple][time]" id="AppleTime" required="required" class="form-control" aria-required="true" type="text"/>
            </div>
        </div>';
        $this->assertEqualsXhtml($expected, $actual);
    }

    /**
     * Test de la méthode LibricielBootstrap3FormHelper::input() avec un type textarea.
     */
    public function testInputTextarea()
    {
        // 1. textarea simple
        $actual = $this->LibricielBootstrap3Form->input('Apple.comment', ['type' => 'textarea']);
        $expected =
        '<div class="input textarea form-group">
            <label for="AppleComment" class="control-label col-sm-4">Commentaire</label>
            <div class="col-sm-8">
                <textarea name="data[Apple][comment]" id="AppleComment" class="form-control" cols="30" rows="6"></textarea>
            </div>
        </div>';
        $this->assertEqualsXhtml($expected, $actual);

        // 2. textarea simple avec help-block
        $actual = $this->LibricielBootstrap3Form->input('Apple.comment', ['type' => 'textarea', 'help-block' => true]);
        $expected =
        '<div class="input textarea form-group"><label for="AppleComment" class="control-label col-sm-4">Commentaire</label>
            <div class="col-sm-8">
                <textarea name="data[Apple][comment]" id="AppleComment" class="form-control" cols="30" rows="6" aria-describedby="AppleCommentHelpBlock"></textarea>
                <span class="help-block" id="AppleCommentHelpBlock">Votre commentaire au sujet de la pomme</span>
            </div>
        </div>';
        $this->assertEqualsXhtml($expected, $actual);
    }

    /**
     * Test de la méthode LibricielBootstrap3FormHelper::input() avec un type "checkbox".
     */
    public function testInputCheckbox()
    {
        $actual = $this->LibricielBootstrap3Form->input('Apple.agree', ['type' => 'checkbox']);
        $expected =
        '<div class="input checkbox form-group">
            <input type="hidden" name="data[Apple][agree]" id="AppleAgree_" value="0"/>
            <input type="checkbox" name="data[Apple][agree]" id="AppleAgree" value="1"/>
            <label for="AppleAgree" class="control-label col-sm-4">Apple.agree</label>
        </div>';
        $this->assertEqualsXhtml($expected, $actual);
    }

    /**
     * Test de la méthode LibricielBootstrap3FormHelper::dateTime().
     */
    public function testInputDateTime()
    {
        $actual = $this->LibricielBootstrap3Form->dateTime('Apple.birth', 'DMY', '24', ['minYear' => 2018, 'maxYear' => 2018]);
        $expected =
        '<div class="col-sm-8">
            <select name="data[Apple][birth][day]" class="form-control" id="AppleBirthDay">
                <option value=""></option>
                <option value="01">1</option>
                <option value="02">2</option>
                <option value="03">3</option>
                <option value="04">4</option>
                <option value="05">5</option>
                <option value="06">6</option>
                <option value="07">7</option>
                <option value="08">8</option>
                <option value="09">9</option>
                <option value="10">10</option>
                <option value="11">11</option>
                <option value="12">12</option>
                <option value="13">13</option>
                <option value="14">14</option>
                <option value="15">15</option>
                <option value="16">16</option>
                <option value="17">17</option>
                <option value="18">18</option>
                <option value="19">19</option>
                <option value="20">20</option>
                <option value="21">21</option>
                <option value="22">22</option>
                <option value="23">23</option>
                <option value="24">24</option>
                <option value="25">25</option>
                <option value="26">26</option>
                <option value="27">27</option>
                <option value="28">28</option>
                <option value="29">29</option>
                <option value="30">30</option>
                <option value="31">31</option>
            </select>
        </div>-<div class="col-sm-8">
            <select name="data[Apple][birth][month]" class="form-control" id="AppleBirthMonth">
                <option value=""></option>
                <option value="01">janvier</option>
                <option value="02">février</option>
                <option value="03">mars</option>
                <option value="04">avril</option>
                <option value="05">mai</option>
                <option value="06">juin</option>
                <option value="07">juillet</option>
                <option value="08">août</option>
                <option value="09">septembre</option>
                <option value="10">octobre</option>
                <option value="11">novembre</option>
                <option value="12">décembre</option>
            </select>
        </div>-<div class="col-sm-8">
            <select name="data[Apple][birth][year]" class="form-control" id="AppleBirthYear">
                <option value=""></option>
                <option value="2018">2018</option>
            </select>
        </div>
        <div class="col-sm-8">
            <select name="data[Apple][birth][hour]" class="form-control" id="AppleBirthHour">
                <option value=""></option>
                <option value="00">0</option>
                <option value="01">1</option>
                <option value="02">2</option>
                <option value="03">3</option>
                <option value="04">4</option>
                <option value="05">5</option>
                <option value="06">6</option>
                <option value="07">7</option>
                <option value="08">8</option>
                <option value="09">9</option>
                <option value="10">10</option>
                <option value="11">11</option>
                <option value="12">12</option>
                <option value="13">13</option>
                <option value="14">14</option>
                <option value="15">15</option>
                <option value="16">16</option>
                <option value="17">17</option>
                <option value="18">18</option>
                <option value="19">19</option>
                <option value="20">20</option>
                <option value="21">21</option>
                <option value="22">22</option>
                <option value="23">23</option>
            </select>
        </div>:<div class="col-sm-8">
            <select name="data[Apple][birth][min]" class="form-control" id="AppleBirthMin">
                <option value=""></option>
                <option value="00">00</option>
                <option value="01">01</option>
                <option value="02">02</option>
                <option value="03">03</option>
                <option value="04">04</option>
                <option value="05">05</option>
                <option value="06">06</option>
                <option value="07">07</option>
                <option value="08">08</option>
                <option value="09">09</option>
                <option value="10">10</option>
                <option value="11">11</option>
                <option value="12">12</option>
                <option value="13">13</option>
                <option value="14">14</option>
                <option value="15">15</option>
                <option value="16">16</option>
                <option value="17">17</option>
                <option value="18">18</option>
                <option value="19">19</option>
                <option value="20">20</option>
                <option value="21">21</option>
                <option value="22">22</option>
                <option value="23">23</option>
                <option value="24">24</option>
                <option value="25">25</option>
                <option value="26">26</option>
                <option value="27">27</option>
                <option value="28">28</option>
                <option value="29">29</option>
                <option value="30">30</option>
                <option value="31">31</option>
                <option value="32">32</option>
                <option value="33">33</option>
                <option value="34">34</option>
                <option value="35">35</option>
                <option value="36">36</option>
                <option value="37">37</option>
                <option value="38">38</option>
                <option value="39">39</option>
                <option value="40">40</option>
                <option value="41">41</option>
                <option value="42">42</option>
                <option value="43">43</option>
                <option value="44">44</option>
                <option value="45">45</option>
                <option value="46">46</option>
                <option value="47">47</option>
                <option value="48">48</option>
                <option value="49">49</option>
                <option value="50">50</option>
                <option value="51">51</option>
                <option value="52">52</option>
                <option value="53">53</option>
                <option value="54">54</option>
                <option value="55">55</option>
                <option value="56">56</option>
                <option value="57">57</option>
                <option value="58">58</option>
                <option value="59">59</option>
            </select>
        </div>';
        $this->assertEqualsXhtml($expected, $actual);
    }

    /**
     * Test de la méthode LibricielBootstrap3FormHelper::dateTime() avec le mode jquery.
     */
    public function testInputDateTimeModeJquery()
    {
        // En spécifiant les paramètres $dateFormat et $timeFormat
        $actual = $this->LibricielBootstrap3Form->dateTime('Apple.birth', 'DMY', '24', ['mode' => 'jquery', 'minYear' => 2018, 'maxYear' => 2018]);
        $expected =
        '<div class="col-sm-8">
            <input name="data[Apple][birth]" data-type="datetime-local" minYear="2018" maxYear="2018" class="form-control" type="text" id="AppleBirth"/>
        </div>';
        $this->assertEqualsXhtml($expected, $actual);

        // En spécifiant uniquement le paramètre $dateFormat
        $actual = $this->LibricielBootstrap3Form->dateTime('Apple.birth', 'DMY', null, ['mode' => 'jquery', 'minYear' => 2018, 'maxYear' => 2018]);
        $expected =
        '<div class="col-sm-8">
            <input name="data[Apple][birth]" data-type="date" minYear="2018" maxYear="2018" class="form-control" type="text" id="AppleBirth"/>
        </div>';
        $this->assertEqualsXhtml($expected, $actual);

        // En spécifiant uniquement le paramètre $timeFormat
        $actual = $this->LibricielBootstrap3Form->dateTime('Apple.birth', null, '24', ['mode' => 'jquery', 'minYear' => 2018, 'maxYear' => 2018]);
        $expected =
        '<div class="col-sm-8">
            <input name="data[Apple][birth]" data-type="time" minYear="2018" maxYear="2018" class="form-control" type="text" id="AppleBirth"/>
        </div>';
        $this->assertEqualsXhtml($expected, $actual);
    }

    /**
     * Test de la méthode LibricielBootstrap3FormHelper::dateTimeHtml5().
     */
    public function testInputDateTimeHtml5()
    {
        $actual = $this->LibricielBootstrap3Form->dateTimeHtml5('Apple.birth', 'date');
        $expected =
        '<div class="col-sm-8">
            <input name="data[Apple][birth]" type="date" class="form-control" id="AppleBirth"/>
        </div>';
        $this->assertEqualsXhtml($expected, $actual);
    }

    /**
     * Test de la méthode LibricielBootstrap3FormHelper::input() avec le type "tel".
     */
    public function testInputTel()
    {
        $actual = $this->LibricielBootstrap3Form->input('Apple.phone', ['type' => 'tel']);
        $expected =
        '<div class="input tel form-group">
            <label for="ApplePhone" class="control-label col-sm-4">Apple.phone</label>
            <div class="col-sm-8">
                <input name="data[Apple][phone]" id="ApplePhone" class="form-control" type="tel"/>
            </div>
        </div>';
        $this->assertEqualsXhtml($expected, $actual);
    }

    /**
     * Test de la méthode LibricielBootstrap3FormHelper::input() avec le type "range".
     */
    public function testInputRange()
    {
        $actual = $this->LibricielBootstrap3Form->input('Apple.weight', ['type' => 'range']);
        $expected =
        '<div class="input range form-group">
            <label for="AppleWeight" class="control-label col-sm-4">Apple.weight</label>
            <div class="col-sm-8">
                <input name="data[Apple][weight]" id="AppleWeight" class="form-control" type="range"/>
            </div>
        </div>';
        $this->assertEqualsXhtml($expected, $actual);
    }

    /**
     * Test de la méthode LibricielBootstrap3FormHelper::input() avec le type "select".
     */
    public function testInputSelect()
    {
        $params = [
            'type' => 'select',
            'options' => [
                'yellow' => 'Jaune',
                'red' => 'Rouge',
                'green' => 'Verte',
            ],
        ];

        // 1. Champ select simple
        $actual = $this->LibricielBootstrap3Form->input('Apple.color', $params);
        $expected =
        '<div class="input select required form-group required">
            <label for="AppleColor" class="control-label col-sm-4">Couleur <abbr title="Champ obligatoire" class="required">*</abbr></label>
            <div class="col-sm-8">
                <select name="data[Apple][color]" id="AppleColor" class="form-control" required="required">
                    <option value=""></option>
                    <option value="yellow">Jaune</option>
                    <option value="red">Rouge</option>
                    <option value="green">Verte</option>
                </select>
            </div>
        </div>';
        $this->assertEqualsXhtml($expected, $actual);

        // 2. Champ select simple avec help-block
        $actual = $this->LibricielBootstrap3Form->input('Apple.color', $params + ['help-block' => true]);
        $expected =
        '<div class="input select required form-group required">
            <label for="AppleColor" class="control-label col-sm-4">Couleur <abbr title="Champ obligatoire" class="required">*</abbr></label>
            <div class="col-sm-8">
                <select name="data[Apple][color]" id="AppleColor" class="form-control" aria-describedby="AppleColorHelpBlock" required="required">
                    <option value=""></option>
                    <option value="yellow">Jaune</option>
                    <option value="red">Rouge</option>
                    <option value="green">Verte</option>
                </select>
                <span class="help-block" id="AppleColorHelpBlock">Veuillez choisir une couleur parmi celles proposéess</span>
            </div>
        </div>';
        $this->assertEqualsXhtml($expected, $actual);
    }

    /**
     * Test de la méthode LibricielBootstrap3FormHelper::input() avec le type
     * "select" et l'attribut "placeholder" renseigné.
     */
    public function testInputSelectWithPlaceholder()
    {
        $params = [
            'type' => 'select',
            'placeholder' => 'Rouge',
            'options' => [
                'yellow' => 'Jaune',
                'red' => 'Rouge',
                'green' => 'Verte',
            ],
        ];
        $actual = $this->LibricielBootstrap3Form->input('Apple.color', $params);
        $expected =
        '<div class="input select required form-group required">
            <label for="AppleColor" class="control-label col-sm-4">Couleur <abbr title="Champ obligatoire" class="required">*</abbr></label>
            <div class="col-sm-8">
                <select name="data[Apple][color]" id="AppleColor" data-placeholder="Rouge" class="form-control" required="required">
                    <option value=""></option>
                    <option value="yellow">Jaune</option>
                    <option value="red">Rouge</option>
                    <option value="green">Verte</option>
                </select>
            </div>
        </div>';
        $this->assertEqualsXhtml($expected, $actual);
    }

    /**
     * Test de la méthode LibricielBootstrap3FormHelper::input() avec le type
     * "select" et multiple à "multiple".
     */
    public function testInputSelectMultiple()
    {
        $params = [
            'type' => 'select',
            'multiple' => true,
            'options' => [
                'yellow' => 'Jaune',
                'red' => 'Rouge',
                'green' => 'Verte',
            ],
        ];

        // 1. Champ select multiple
        $actual = $this->LibricielBootstrap3Form->input('Apple.color', $params);
        $expected =
        '<fieldset class="input select required col-sm-8 col-sm-offset-4 required">
            <legend>Couleur</legend>
            <div class="col-sm-8">
                <input type="hidden" name="data[Apple][color]" value="" id="AppleColor_"/>
                <select name="data[Apple][color][]" id="AppleColor" multiple="multiple" class="form-control" required="required">
                    <option value="yellow">Jaune</option>
                    <option value="red">Rouge</option>
                    <option value="green">Verte</option>
                </select>
            </div>
        </fieldset>';
        $this->assertEqualsXhtml($expected, $actual);

        // 2. Champ select multiple avec help-block
        $actual = $this->LibricielBootstrap3Form->input('Apple.color', $params + ['help-block' => true]);
        $expected =
        '<fieldset class="input select required col-sm-8 col-sm-offset-4 required">
            <legend>Couleur</legend>
            <div class="col-sm-8">
                <input type="hidden" name="data[Apple][color]" value="" id="AppleColor_"/>
                <select name="data[Apple][color][]" id="AppleColor" multiple="multiple" class="form-control" aria-describedby="AppleColorHelpBlock" required="required">
                    <option value="yellow">Jaune</option>
                    <option value="red">Rouge</option>
                    <option value="green">Verte</option>
                </select>
                <span class="help-block" id="AppleColorHelpBlock">Veuillez choisir une couleur parmi celles proposéess</span>
            </div>
        </fieldset>';
        $this->assertEqualsXhtml($expected, $actual);
    }

    /**
     * Test de la méthode LibricielBootstrap3FormHelper::input() avec le type
     * "select" et multiple à "checkbox".
     */
    public function testInputSelectMultipleCheckbox()
    {
        $params = [
            'type' => 'select',
            'multiple' => 'checkbox',
            'options' => [
                'yellow' => 'Jaune',
                'red' => 'Rouge',
                'green' => 'Verte',
            ],
        ];
        $actual = $this->LibricielBootstrap3Form->input('Apple.color', $params);
        $expected =
        '<fieldset class="input select required col-sm-8 col-sm-offset-4 required">
            <legend>Couleur</legend>
            <div class="col-sm-8">
                <input type="hidden" name="data[Apple][color]" value="" id="AppleColor"/>
                <div class="checkbox">
                    <input type="checkbox" name="data[Apple][color][]" value="yellow" id="AppleColorYellow" />
                    <label for="AppleColorYellow" class="control-label col-sm-4">Jaune</label>
                </div>
                <div class="checkbox">
                    <input type="checkbox" name="data[Apple][color][]" value="red" id="AppleColorRed" />
                    <label for="AppleColorRed" class="control-label col-sm-4">Rouge</label>
                </div>
                <div class="checkbox">
                    <input type="checkbox" name="data[Apple][color][]" value="green" id="AppleColorGreen" />
                    <label for="AppleColorGreen" class="control-label col-sm-4">Verte</label>
                </div>
            </div>
        </fieldset>';
        $this->assertEqualsXhtml($expected, $actual);
    }

    /**
     * Test de la méthode LibricielBootstrap3FormHelper::input() avec le type
     * "radio".
     */
    public function testInputRadio()
    {
        $params = [
            'type' => 'radio',
            'options' => [
                'yellow' => 'Jaune',
                'red' => 'Rouge',
                'green' => 'Verte',
            ],
        ];
        $actual = $this->LibricielBootstrap3Form->input('Apple.color', $params);
        $expected =
        '<div class="input radio required form-group required">
            <fieldset class="input radio col-sm-8 col-sm-offset-4">
                <legend>Couleur</legend>
                <div>
                    <input type="hidden" name="data[Apple][color]" id="AppleColor_" value=""/>
                    <input type="radio" name="data[Apple][color]" id="AppleColorYellow" value="yellow" required="required" />
                    <label for="AppleColorYellow">Jaune</label>
                </div>
                <div>
                    <input type="radio" name="data[Apple][color]" id="AppleColorRed" value="red" required="required" />
                    <label for="AppleColorRed">Rouge</label>
                </div>
                <div>
                    <input type="radio" name="data[Apple][color]" id="AppleColorGreen" value="green" required="required" />
                    <label for="AppleColorGreen">Verte</label>
                </div>
            </fieldset>
        </div>';
        $this->assertEqualsXhtml($expected, $actual);
    }

    /**
     * Test de la méthode LibricielBootstrap3FormHelper::input() avec un type staticctrl.
     */
    public function testInputStaticctrl()
    {
        $options = ['type' => 'staticctrl', 'value' => 'red', 'options' => ['red' => 'Rouge']];
        $actual = $this->LibricielBootstrap3Form->input('Apple.color', $options);
        $expected =
        '<div class="input staticctrl required form-group required">
            <p class="control-label col-sm-4">
                <strong>Couleur <abbr title="Champ obligatoire" class="required">*</abbr></strong>
            </p>
            <p class="col-sm-8 form-control-static text">Rouge</p>
        </div>';
        $this->assertEqualsXhtml($expected, $actual);
    }

    /**
     * Test de la méthode LibricielBootstrap3FormHelper::input() avec le type "fiddle".
     */
    public function testInputFiddle()
    {
        $actual = $this->LibricielBootstrap3Form->input('Apple.fiddle', ['type' => 'fiddle']);
        $expected =
        '<div class="invisible fiddle form-group" style="display: none">
            <label for="AppleFiddle" class="control-label col-sm-4">Apple.fiddle</label>
            <div class="col-sm-8">
                <input name="data[Apple][fiddle]" id="AppleFiddle" disabled="disabled" type="text" style="display: none" class="invisible form-control"/>
            </div>
        </div>';
        $this->assertEqualsXhtml($expected, $actual);
    }

    /**
     * Test de la méthode LibricielBootstrap3FormHelper::fiddle().
     */
    public function testFiddle()
    {
        $actual = $this->LibricielBootstrap3Form->fiddle('Apple.fiddle');
        $expected =
        '<div class="col-sm-8">
            <input name="data[Apple][fiddle]" type="text" style="display: none" class="invisible form-control" id="AppleFiddle"/>
        </div>';
        $this->assertEqualsXhtml($expected, $actual);
    }

    /**
     * Test de la méthode LibricielBootstrap3FormHelper::staticctrl().
     */
    public function testStaticctrl()
    {
        // Avec la valeur dans le request data
        $this->LibricielBootstrap3Form->data = ['Apple' => ['color' => 'red']];
        $actual = $this->LibricielBootstrap3Form->staticctrl('Apple.color');
        $expected =
        '<p class="col-sm-8 form-control-static text">red</p>';
        $this->assertEqualsXhtml($expected, $actual);

        // Avec la valeur dans la clé value des paramètres
        $actual = $this->LibricielBootstrap3Form->staticctrl('Apple.color', ['value' => 'red']);
        $expected =
        '<p class="col-sm-8 form-control-static text">red</p>';
        $this->assertEqualsXhtml($expected, $actual);

        // Avec la valeur dans la clé value des paramètres, traduite par les options
        $options = ['value' => 'red', 'options' => ['red' => 'Rouge']];
        $actual = $this->LibricielBootstrap3Form->staticctrl('Apple.color', $options);
        $expected =
        '<p class="col-sm-8 form-control-static text">Rouge</p>';
        $this->assertEqualsXhtml($expected, $actual);
    }

    /**
     * Test de la méthode LibricielBootstrap3FormHelper::wrap().
     */
    public function testWrap()
    {
        $text = '"Pauvre, disent-elles, Tout ce qu\'il a encore à faire"';

        // Sans emballage
        $actual = $this->LibricielBootstrap3Form->wrap($text, false);
        $expected = $text;
        $this->assertEquals($expected, $actual);

        // Avec les valeurs par défaut
        $actual = $this->LibricielBootstrap3Form->wrap($text, true);
        $expected = "<div>{$text}</div>";
        $this->assertEquals($expected, $actual);

        // En spécifiant des options
        $options = [
            'escape' => true,
            'tag' => 'span',
            'col' => 'col-md-5',
            'col-offset' => 'col-offset-md-7',
            'class' => 'error-message-wrapper',
        ];
        $actual = $this->LibricielBootstrap3Form->wrap($text, $options);
        $expected = '<span class="error-message-wrapper col-md-5 col-offset-md-7">&quot;Pauvre, disent-elles, Tout ce qu&#039;il a encore à faire&quot;</span>';
        $this->assertEqualsXhtml($expected, $actual);

        // En spécifiant des options et les paramètres par défaut
        $options = [
            'tag' => 'span',
            'col' => 'col-md-5',
            'col-offset' => 'col-offset-md-7',
            'class' => 'error-message-wrapper',
        ];
        $defaults = [
            'class' => 'error-message',
            'data-foo' => 'bar',
        ];
        $actual = $this->LibricielBootstrap3Form->wrap('Texte', $options, $defaults);
        $expected = '<span class="error-message-wrapper col-md-5 col-offset-md-7" data-foo="bar">Texte</span>';
        $this->assertEqualsXhtml($expected, $actual);
    }

    public function testFieldset()
    {
        $actual = $this->LibricielBootstrap3Form->fieldset('Légende', 'contenu', ['class' => 'foo-bar-baz']);
        $expected = '<fieldset class="foo-bar-baz"><legend>Légende</legend>contenu</fieldset>';
        $this->assertEqualsXhtml($expected, $actual);
    }

    public function testInputGroupAddon()
    {
        $params = [
            'input-group' => true,
            'input-group-addon-before' => '<span class="fa fa-user"> </span>',
            'input-group-addon-after' => '<span class="fa fa-lock"> </span>',
        ];
        $actual = $this->LibricielBootstrap3Form->input('Apple.name', $params);
        $expected =
        '<div class="input text required form-group required">
            <label for="AppleName" class="control-label col-sm-4">Variété <abbr title="Champ obligatoire" class="required">*</abbr></label>
            <div class="col-sm-8">
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-user"></span></span>
                    <input name="data[Apple][name]" id="AppleName" maxlength="40" class="form-control" required="required" aria-required="true" type="text" value="Pink Lady"/>
                    <span class="input-group-addon"><span class="fa fa-lock"></span></span>
                </div>
            </div>
        </div>';
        $this->assertEqualsXhtml($expected, $actual);
    }
}
