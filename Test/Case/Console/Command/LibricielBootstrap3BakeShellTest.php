<?php
App::uses('ConsoleOutput', 'Console');
App::uses('ConsoleInput', 'Console');
App::uses('ShellDispatcher', 'Console');
App::uses('Shell', 'Console');
App::uses('LibricielBootstrap3BakeShell', 'LibricielBootstrap3.Console/Command');
App::uses('ModelTask', 'Console/Command/Task');
App::uses('ControllerTask', 'Console/Command/Task');
App::uses('DbConfigTask', 'Console/Command/Task');
App::uses('Controller', 'Controller');

if (class_exists('ApplesController') === false) {
    class ApplesController extends Controller
    {
    }
}

class LibricielBootstrap3BakeShellTest extends CakeTestCase
{
    /**
     * fixtures
     *
     * @var array
     */
    public $fixtures = ['core.apple'];

    /**
     * setup test
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $out = $this->getMock('ConsoleOutput', [], [], '', false);
        $in = $this->getMock('ConsoleInput', [], [], '', false);

        $this->Shell = $this->getMock(
            'LibricielBootstrap3BakeShell',
            ['in', 'out', 'hr', 'err', 'createFile', '_stop', '_checkUnitTest'],
            [$out, $out, $in]
        );
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        parent::tearDown();
        unset($this->Dispatch, $this->Shell);
    }

    /**
     * test bake all
     *
     * @return void
     */
    public function testAllWithModelName()
    {
        App::uses('Apple', 'Model');
        $userExists = class_exists('Apple');
        $this->skipIf($userExists, 'Apple class exists, cannot test `bake all [param]`.');

        $this->Shell->Model = $this->getMock('ModelTask', [], [&$this->Dispatcher]);
        $this->Shell->Controller = $this->getMock('ControllerTask', [], [&$this->Dispatcher]);
        $this->Shell->View = $this->getMock('ModelTask', [], [&$this->Dispatcher]);
        $this->Shell->DbConfig = $this->getMock('DbConfigTask', [], [&$this->Dispatcher]);

        /*$this->Shell->DbConfig->expects($this->once())
            ->method('getConfig')
            ->will($this->returnValue('test'));*/

        $this->Shell->Model->expects($this->never())
            ->method('getName');

        $this->Shell->Model->expects($this->once())
            ->method('bake')
            ->will($this->returnValue(true));

        $this->Shell->Controller->expects($this->once())
            ->method('bake')
            ->will($this->returnValue(true));

        $this->Shell->View->expects($this->once())
            ->method('execute');

        $this->Shell->expects($this->once())->method('_stop');
        $this->Shell->expects($this->at(2))
            ->method('out')
            ->with('Bake All');

        $this->Shell->expects($this->at(7))
            ->method('out')
            ->with('<success>Bake All complete</success>');

        $this->Shell->connection = 'test';
        $this->Shell->params = [];
        $this->Shell->args = ['Apple'];
        $this->Shell->all();

        $this->assertEquals('Apple', $this->Shell->View->args[0]);
    }
}
