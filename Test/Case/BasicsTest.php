<?php
require_once CakePlugin::path('LibricielBootstrap3') . 'Config' . DS . 'bootstrap.php';

class BasicsTest extends CakeTestCase
{
    /**
     * Test de la fonction array_extract_keys()
     */
    public function testArrayExtractKeys()
    {
        // 1.
        $result = array_extract_keys([], []);
        $expected = [];
        $this->assertEquals($expected, $result, var_export($result, true));

        // 2.
        $result = array_extract_keys(['foo' => 'bar'], []);
        $expected = [];
        $this->assertEquals($expected, $result, var_export($result, true));

        // 3.
        $result = array_extract_keys([], ['foo', 'bar']);
        $expected = [];
        $this->assertEquals($expected, $result, var_export($result, true));

        // 4.
        $result = array_extract_keys(['foo' => 'bar', 'bar' => 'baz', 'baz' => 'boz'], ['foo', 'bar']);
        $expected = ['foo' => 'bar', 'bar' => 'baz'];
        $this->assertEquals($expected, $result, var_export($result, true));
    }

    /**
     * Test de la fonction array_except_keys()
     */
    public function testArrayExceptKeys()
    {
        // 1.
        $result = array_except_keys([], []);
        $expected = [];
        $this->assertEquals($expected, $result, var_export($result, true));

        // 2.
        $result = array_except_keys(['foo' => 'bar'], []);
        $expected = ['foo' => 'bar'];
        $this->assertEquals($expected, $result, var_export($result, true));

        // 3.
        $result = array_except_keys([], ['foo', 'bar']);
        $expected = [];
        $this->assertEquals($expected, $result, var_export($result, true));

        // 4.
        $result = array_except_keys(['foo' => 'bar', 'bar' => 'baz', 'baz' => 'boz'], ['foo', 'bar']);
        $expected = ['baz' => 'boz'];
        $this->assertEquals($expected, $result, var_export($result, true));
    }

    /**
     * Test de la fonction array_extract_keys_by_prefix()
     */
    public function testArrayExtractKeysByPrefix()
    {
        // 1.
        $result = array_extract_keys_by_prefix([], '');
        $expected = [];
        $this->assertEquals($expected, $result, var_export($result, true));

        // 2.
        $result = array_extract_keys_by_prefix(['foo' => 'bar'], 'data-');
        $expected = [];
        $this->assertEquals($expected, $result, var_export($result, true));

        // 3.
        $result = array_extract_keys_by_prefix([], 'data-');
        $expected = [];
        $this->assertEquals($expected, $result, var_export($result, true));

        // 4.
        $result = array_extract_keys_by_prefix(['data-foo' => 'bar', 'bar' => 'baz', 'data-baz' => 'boz'], 'data-');
        $expected = ['data-foo' => 'bar', 'data-baz' => 'boz'];
        $this->assertEquals($expected, $result, var_export($result, true));
    }

    /**
     * Test de la fonction array_except_keys_by_prefix()
     */
    public function testArrayExceptKeysByPrefix()
    {
        // 1.
        $result = array_except_keys_by_prefix([], '');
        $expected = [];
        $this->assertEquals($expected, $result, var_export($result, true));

        // 2.
        $result = array_except_keys_by_prefix(['foo' => 'bar'], 'data-');
        $expected = ['foo' => 'bar'];
        $this->assertEquals($expected, $result, var_export($result, true));

        // 3.
        $result = array_except_keys_by_prefix([], 'data-');
        $expected = [];
        $this->assertEquals($expected, $result, var_export($result, true));

        // 4.
        $result = array_except_keys_by_prefix(['data-foo' => 'bar', 'bar' => 'baz', 'data-baz' => 'boz'], 'data-');
        $expected = ['bar' => 'baz'];
        $this->assertEquals($expected, $result, var_export($result, true));
    }
}
