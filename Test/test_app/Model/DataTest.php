<?php
App::uses('AppModel', 'Model');

class DataTest extends AppModel
{
    public $validate = [
        'id' => [
            'integer' => [
                'rule' => [
                    'integer',
                ],
                'message' => null,
                'required' => null,
                'allowEmpty' => true,
                'on' => null,
            ],
        ],
        'count' => [
            'integer' => [
                'rule' => [
                    'integer',
                ],
                'message' => null,
                'required' => null,
                'allowEmpty' => true,
                'on' => null,
            ],
        ],
        'float' => [
            'numeric' => [
                'rule' => [
                    'numeric',
                ],
                'message' => null,
                'required' => null,
                'allowEmpty' => true,
                'on' => null,
            ],
        ],
        'created' => [
            'datetime' => [
                'rule' => [
                    'datetime',
                ],
                'message' => null,
                'required' => null,
                'allowEmpty' => true,
                'on' => null,
            ],
        ],
        'updated' => [
            'datetime' => [
                'rule' => [
                    'datetime',
                ],
                'message' => null,
                'required' => null,
                'allowEmpty' => true,
                'on' => null,
            ],
        ],
    ];
}
