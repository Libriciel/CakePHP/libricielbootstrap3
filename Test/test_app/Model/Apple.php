<?php
App::uses('AppModel', 'Model');

class Apple extends AppModel
{
    public $validate = [
        'id' => [
            'integer' => [
                'rule' => [
                    'integer',
                ],
                'message' => null,
                'required' => null,
                'allowEmpty' => true,
                'on' => null,
            ],
        ],
        'apple_id' => [
            'integer' => [
                'rule' => [
                    'integer',
                ],
                'message' => null,
                'required' => null,
                'allowEmpty' => true,
                'on' => null,
            ],
        ],
        'color' => [
            'notBlank' => [
                'rule' => [
                    'notBlank',
                ],
                'message' => null,
                'required' => null,
                'allowEmpty' => false,
                'on' => null,
            ],
            'maxLength' => [
                'rule' => [
                    'maxLength',
                    40,
                ],
                'message' => null,
                'required' => null,
                'allowEmpty' => true,
                'on' => null,
            ],
        ],
        'name' => [
            'notBlank' => [
                'rule' => [
                    'notBlank',
                ],
                'message' => null,
                'required' => null,
                'allowEmpty' => false,
                'on' => null,
            ],
            'maxLength' => [
                'rule' => [
                    'maxLength',
                    40,
                ],
                'message' => null,
                'required' => null,
                'allowEmpty' => true,
                'on' => null,
            ],
        ],
        'created' => [
            'datetime' => [
                'rule' => [
                    'datetime',
                ],
                'message' => null,
                'required' => null,
                'allowEmpty' => true,
                'on' => null,
            ],
        ],
        'date' => [
            'date' => [
                'rule' => [
                    'date',
                ],
                'message' => null,
                'required' => null,
                'allowEmpty' => true,
                'on' => null,
            ],
        ],
        'modified' => [
            'datetime' => [
                'rule' => [
                    'datetime',
                ],
                'message' => null,
                'required' => null,
                'allowEmpty' => true,
                'on' => null,
            ],
        ],
        'mytime' => [
            'time' => [
                'rule' => [
                    'time',
                ],
                'message' => null,
                'required' => null,
                'allowEmpty' => true,
                'on' => null,
            ],
        ],
    ];
}
