<?php
App::uses('FormHelper', 'View/Helper');
// @todo: ajouter les clés help-block et placeholder
class LibricielBootstrap3FormHelper extends FormHelper
{
    /**
     * Helpers utilisés par ce helper.
     *
     * @var array
     */
    public $helpers = [
        'Html',
        'LibricielBootstrap3.LibricielBootstrap3Table',
    ];

    /**
     * @todo Configure
     *
     * @var array
     */
    protected $_config = [
        // Types de boutons paramétrés
        'buttons' => [
            'Back' => [
                'class' => 'btn btn-default',
                'icon-classes' => 'fa fa-lg fa-arrow-left',
            ],
            'Cancel' => [
                'class' => 'btn btn-default',
                'icon-classes' => 'fa fa-lg fa-times-circle',
            ],
            'Refuse' => [
                'class' => 'btn btn-danger',
                'icon-classes' => 'fa fa-lg fa-times fa-danger',
            ],
            'Reset' => [
                'type' => 'reset',
                'class' => 'btn btn-default',
                'icon-classes' => 'fa fa-lg fa-undo',
            ],
            'Save' => [
                'class' => 'btn btn-primary',
                'icon-classes' => 'fa fa-lg fa-floppy-o',
            ],
            'Send' => [
                'class' => 'btn btn-success',
                'icon-classes' => 'fa fa-lg fa-paper-plane',
            ],
        ],
        // Valeurs par défaut en fonction du type de champ (de la méthode).
        'defaults' => [
            'buttons' => [
                'div' => [
                    'col' => 'col-sm-6',
                    'col-offset' => 'col-sm-offset-4',
                    'class' => 'btn-group',
                ],
            ],
            'date' => [
                'empty' => true,
                'dateFormat' => 'DMY',
                'monthNames' => true,
            ],
            'datetime' => [
                'empty' => true,
                'dateFormat' => 'DMY',
                'timeFormat' => '24',
                'monthNames' => true,
            ],
            'error' => [
                'class' => 'error-message help-block',
                'wrap' => [
                    'tag' => 'div',
                    'col' => 'col-sm-8',
                    'col-offset' => 'col-sm-offset-4',
                    'class' => 'error-message-wrapper',
                ],
            ],
            'fiddle' => [
//                'class' => 'form-control',
                'disabled' => true,
                'div' => [
                    'class' => 'invisible fiddle',
                    'style' => 'display: none',
                ],
            ],
            'file' => [
                'class' => 'form-control',
                'wrap' => [
                    'tag' => 'div',
                    'col' => 'col-sm-8',
                ],
            ],
            'input' => [
                'id' => null,
                'placeholder' => null,
                'help-block' => null,
                'type' => null,
            ],
            'inputs' => [
                'fieldset' => false,
                'legend' => false,
            ],
            'label' => [
                'class' => 'control-label col-sm-4',
            ],
            'radio' => [
                'fieldset' => [
                     // @info si on ajoute form-group, l'affichage "casse"
                    'class' => 'input radio col-sm-8 col-sm-offset-4',
                ],
            ],
            'select' => [
                'placeholder' => null,
                'multiple' => null,
                'empty' => true,
                'help-block' => null,
                /*'class' => 'form-control',
                'div' => [
                    'tag' => 'div',
                    'col' => 'col-sm-8'
                ]*/
            ],
            'select_multiple' => [
                'div' => [
                    // @info si on ajoute form-group, l'affichage "casse"
                    'class' => 'col-sm-8 col-sm-offset-4',
                ],
            ],
            'textarea' => [
                'class' => 'form-control',
                'help-block' => null,
                'wrap' => [
                    'tag' => 'div',
                    'col' => 'col-sm-8',
                ],
            ],
            'time' => [
                'empty' => true,
                'timeFormat' => '24',
            ],
            'wrap' => [
                'tag' => 'div',
                'escape' => false,
            ],
        ],
    ];

    /**
     * Permet de surcharger la configuration et de la récupérer.
     *
     * @param array|string $config Le configuration à ajouter, si c'est une chaîne
     *  de caractères, celle-ci représentera la clé de configuration à lire.
     * @return array
     */
    public function config($config = [])
    {
        if (is_string($config) === true) {
            $config = (array)Configure::read($config);
        }

        $this->_config = Hash::merge($this->_config, (array)$config);

        return $this->_config;
    }

    /**
     * Surcharge du constructeur pour ajouter la lecture de la configuration.
     *
     * @param View $View
     * @param array $settings
     */
    public function __construct(View $View, $settings = [])
    {
        parent::__construct($View, $settings);
        $this->config('LibricielBootstrap3.' . self::class);
    }

    /**
     * Fusionne les paramètres avec les valeurs par défaut se trouvant dans la
     * sous-clé $key de la clé defaults de la configuration.
     *
     * @param array $params Les paramètres
     * @param string $key Le chemin vers la sous-clé
     * @return array
     */
    protected function _mergeDefaults(array $params, $key)
    {
        if (Hash::check($this->_config, "defaults.{$key}") === true) {
            $params += Hash::get($this->_config, "defaults.{$key}");
        }

        return $params;
    }

    /**
     * Copie la valeur des clés col et col-offset dans l'attribut class des
     * options lorsque cette valeur n'est pas vide et supprime les clés col
     * et col-offset des options.
     *
     * @param array $options
     * @return array
     */
    protected function _moveColAndColOffset(array $options)
    {
        foreach (['col', 'col-offset'] as $key) {
            if (isset($options[$key]) === true && empty($options[$key]) === false) {
                $options = $this->addClass($options, $options[$key]);
                unset($options[$key]);
            }
        }

        return $options;
    }

    /**
     * Retourne le contenu éventuellement enveloppé dans un autre élément élément.
     * Les clés col et col-offset sont également traitées.
     * Les valeurs par défaut sont configurables via la classe Configure (clé
     * defaults.wrap).
     *
     * @param string $content
     * @param bool|array $options
     * @param array $defaults
     * @return string
     */
    public function wrap($content, $options, ?array $defaults = null)
    {
        if ($options !== false) {
            $options = ($options === true ? [] : $options)
                + ($defaults ?? $this->_config['defaults']['wrap']);

            $tag = $options['tag'];
            unset($options['tag']);

            $options = $this->_moveColAndColOffset($options);
            $content = $this->Html->tag($tag, $content, $options);
        }

        return $content;
    }

    public function inputs($fields = null, $blacklist = null, $options = [])
    {
        $fields = $this->_mergeDefaults($fields, __FUNCTION__);

        return parent::inputs($fields, $blacklist, $options);
    }

    /**
     * Déduction du caractère obligatoire du champ.
     */
    protected function _isRequired($fieldName, $options)
    {
        if (isset($options['required']) === false || $options['required'] === null) {
            $this->setEntity($fieldName);
            list(, $field) = $this->entity();
            $required = (array)$this->_introspectModel($this->model(), 'validates', $this->field());
            $options = (array)$options + ['required' => Hash::get($required, $field)];
        }

        return $options['required'] === true;
    }

    protected function _inputLabelText($fieldName, $label, $options)
    {
        // Déduction du caractère obligatoire du champ
        if ($fieldName !== null && $label === null) {
            $label = __m($fieldName);
        }

        if ($this->_isRequired($fieldName, $options)) {
            $label = $label . ' ' . $this->Html->tag('abbr', '*', ['title' => __m('required'), 'class' => 'required']);
        }

        return $label;
    }

    protected function _inputLabel($fieldName, $label, $options)
    {
        $label = $this->_inputLabelText($fieldName, $label, $options);
        $result = parent::_inputLabel($fieldName, $label, $options);

        return $result;
    }

    public function input($fieldName, $options = [])
    {
        $options = ['id' => $this->domId($fieldName)]
            + $this->_mergeDefaults($options, __FUNCTION__);
        $this->setEntity($fieldName);

        if (isset($options['type']) === false) {
            $options = $this->_magicOptions($options);
        }

        $options = $this->_mergeDefaults($options, $options['type']);
        $options = $this->_parseOptions($options);

        $options['div'] = $this->_divOptions($options);

        if ($options['type'] !== 'hidden' && $this->tagIsInvalid() !== false) {
            $options['div'] = $this->addClass($options['div'], 'has-error');
            $options['aria-describedby'] = empty($options['aria-describedby']) ? null : $options['aria-describedby'];
            $options['aria-describedby'] = implode(' ', array_filter([$options['aria-describedby'], "{$options['id']}ErrorMessage"]));
            $options['aria-invalid'] = 'true';
        }

        // placeholder
        if ($options['placeholder'] === true) {
            $options['placeholder'] = __m("{$fieldName}:placeholder");
        }

        if (empty($options['placeholder']) === false && empty($options['data-placeholder']) === true && ($options['type'] === 'select' || empty($options['options']) === false)) {
            $options['data-placeholder'] = $options['placeholder'];
            unset($options['placeholder']);
        }

        if (isset($options['multiple']) === true && empty($options['multiple']) === false) {
            $options['div']['tag'] = 'fieldset';
            $divOptions = $this->_mergeDefaults([], 'select_multiple.div');

            $options['div'] = $this->addClass($options['div'], $divOptions['class']);
            $label = Hash::get($options, 'label');
            $params = ['required' => Hash::get($options, 'required')];
            $legend = $this->_inputLabelText($fieldName, $label, $params);
            $options['before'] = $this->Html->tag('legend', $legend);
            $options['label'] = false;
        } else {
            $options['div'] = $this->addClass($options['div'], 'form-group');
        }

        return parent::input($fieldName, $options);
    }

    public function radio($fieldName, $options = [], $attributes = [])
    {
        $label = Hash::get($options, 'label');
        $params = ['required' => Hash::get($options, 'required')];
        $legend = $this->_inputLabelText($fieldName, $label, $params);
        $attributes += ['legend' => $legend];
        $attributes['fieldset'] = $this->_config['defaults']['radio']['fieldset'];
        $attributes['between'] = '<div>';
        $attributes['separator'] = '</div><div>';
        $attributes['label'] = ['class' => false];
        $result = parent::radio($fieldName, $options, $attributes);

        return str_replace('</fieldset>', '</div></fieldset>', $result);
    }

    public function label($fieldName = null, $text = null, $options = [])
    {
        $options += ['class' => null];
        if ($options['class'] !== false) {
            $defaults = $this->_config['defaults'][__FUNCTION__]['class'];
            $options = $this->addClass($options, $defaults);
        }

        return parent::label($fieldName, $text, $options);
    }

    protected function _helpBlockId(array $params)
    {
        $help_block = Hash::get($params, 'help-block');
        $help_block_id = null;
        if (empty($help_block) === false) {
            $help_block_id = "{$params['id']}HelpBlock";
        }

        return $help_block_id;
    }

    protected function _addHelpBlockParams(array $params)
    {
        $help_block = Hash::get($params, 'help-block');
        if (empty($help_block) === false) {
            $help_block_id = $this->_helpBlockId($params);
            $params['aria-describedby'] = empty($params['aria-describedby']) ? null : $params['aria-describedby'];
            $params['aria-describedby'] = implode(' ', array_filter([$params['aria-describedby'], $help_block_id]));
        }

        return $params;
    }

    protected function _helpBlock($fieldName, array $params)
    {
        $result = null;
        $help_block = Hash::get($params, 'help-block');
        if (empty($help_block) === false) {
            if ($help_block === true) {
                $help_block = __m("{$fieldName}:help-block");
            }
            $result = $this->Html->tag('span', $help_block, ['class' => 'help-block', 'id' => $this->_helpBlockId($params)]);
        }

        return $result;
    }

    /**
     * @todo
     *  - ailleurs ?
     *  - help plutôt que help-block
     *  - CSS error-message
     *  - on ne passe pas ici pour les SELECT
     *  - data-placeholder pour les SELECT (et les copains)
     *  - attribut empty dans le HTML
     *  - required
     */
    public function __call($method, $params)
    {
        if (empty($params) === false) {
            $help_block = null;

            $params[1] = isset($params[1]) === true ? $params[1] : [];
            $params[1] = $this->addClass($params[1], 'form-control');

            $defaults = [
//                'id' => $this->domId($params[0]),
//                'placeholder' => null,
//                'help-block' => null,
            ];
            $params[1] += $defaults;
            //unset($params[1]['required']);
//            // placeholder
//            if ($params[1]['placeholder'] === true) {
//                $params[1]['placeholder'] = __m("{$params[0]}:placeholder");
//            }

            // help-block
            $params[1] = $this->_addHelpBlockParams($params[1]);
            $help_block = Hash::get($params, '1.help-block');
            unset($params[1]['help-block']);
            /*$help_block = Hash::get($params, '1.help-block');
            $help_block_id = null;
            unset($params[1]['help-block']);
            if (empty($help_block) === false) {
                $help_block_id = "{$params[1]['id']}HelpBlock";
                $params[1]['aria-describedby'] = empty($params[1]['aria-describedby']) ? null : $params[1]['aria-describedby'];
                $params[1]['aria-describedby'] = implode(',', array_filter([$params[1]['aria-describedby'], $help_block_id]));
            }*/

            // input-group
            $input_group = Hash::get($params, '1.input-group');
            unset($params[1]['input-group']);

            // input-group-addon-before
            $input_group_addon_before = Hash::get($params, '1.input-group-addon-before');
            unset($params[1]['input-group-addon-before']);

            // input-group-addon-after
            $input_group_addon_after = Hash::get($params, '1.input-group-addon-after');
            unset($params[1]['input-group-addon-after']);

            $params[1]['required'] = $this->_isRequired($params[0], $params[1]);
            if ($this->_isRequired($params[0], $params[1])) {
                $params[1]['aria-required'] = 'true';
            }
        }

        $result = parent::__call($method, $params);

        if (empty($params) === false) {
            // help-block
            $result .= $this->_helpBlock($params[0], $params[1] + ['help-block' => $help_block]);
            /*if (empty($help_block) === false) {
                if ($help_block === true) {
                    $help_block = __m("{$params[0]}:help-block");
                }
                $result .= $this->Html->tag('span', $help_block, ['class' => 'help-block', 'id' => $help_block_id]);
            }*/

            if (empty($input_group_addon_before) === false) {
                $result = $this->Html->tag('span', $input_group_addon_before, ['class' => 'input-group-addon']) . $result;
            }

            if (empty($input_group_addon_after) === false) {
                $result .= $this->Html->tag('span', $input_group_addon_after, ['class' => 'input-group-addon']);
            }

            if ($input_group === true) {
                $result = $this->Html->tag('div', $result, ['class' => 'input-group']);
            }

            // @fixme
            $result = $this->Html->tag('div', $result, ['class' => 'col-sm-8']);
        }

        return $result;
    }

    public function create($model = null, $options = [])
    {
        $defaults = ['autocomplete' => false, 'novalidate' => true];
        $options += $defaults;
        $options = $this->addClass($options, 'form-horizontal');

        return parent::create($model, $options);
    }

    protected function _setButtonsDefaults(array $buttons = [], array $options = [])
    {
        $buttons = Hash::normalize($buttons);
        foreach (array_keys($buttons) as $key) {
            $defaults = ['text' => null, 'class' => null, 'icon-classes' => null];
            $buttons[$key] = (array)$buttons[$key] + $defaults;

            if (empty($buttons[$key]['text']) === true) {
                $buttons[$key]['text'] = __m($key);
            }

            foreach (['class', 'icon-classes', 'type', 'title'] as $foo) {
                if (empty($buttons[$key][$foo]) === true && isset($this->_config['buttons'][$key][$foo]) === true) {
                    $buttons[$key][$foo] = $this->_config['buttons'][$key][$foo];
                }
            }
        }

        return $buttons;
    }

    public function buttons(array $buttons = [], array $options = [])
    {
        if (empty($buttons) === true) {
            return null;
        }

        $buttons = $this->_setButtonsDefaults($buttons, $options);
        $options += $this->_config['defaults'][__FUNCTION__];

        $divOptions = $this->_moveColAndColOffset((array)Hash::get($options, 'div'));
        unset($options['div']);

        $result = '';
        foreach ($buttons as $name => $button) {
            $button = (array)$button + ['type' => 'submit', 'title' => false, 'url' => null];
            if ($button['type'] === 'link') {
                $result .= $this->Html->link(
                    sprintf(
                        '<span class="%s"> </span> %s',
                        $button['icon-classes'],
                        h($button['text'])
                    ),
                    $button['url'],
                    [
                        'class' => $button['class'],
                        'title' => $button['title'],
                        'escape' => false,
                    ]
                );
            } else {
                $result .= $this->button(
                    sprintf(
                        '<span class="%s"> </span> %s',
                        $button['icon-classes'],
                        $button['text']
                    ),
                    [
                        'name' => '_action',
                        'value' => $name,
                        'class' => $button['class'],
                        'type' => $button['type'],
                        'title' => $button['title'],
                    ]
                );
            }
        }

        return $this->Html->tag('div', $result, $divOptions);
    }

    public function error($field, $text = null, $options = [])
    {
        $defaults = ['id' => $this->domId($field) . 'ErrorMessage']
            + $this->_mergeDefaults($options, __FUNCTION__);

        $options = $this->_moveColAndColOffset($options + $defaults);

        $error = parent::error($field, $text, $options);
        $wrap = Hash::get($options, 'wrap');
        if (empty($error) === false && ($wrap === null || empty($wrap) === false)) {
            $wrap = $this->_moveColAndColOffset(in_array($wrap, [true, null], true) ? [] : $wrap);
            $tag = $wrap['tag'];
            unset($wrap['tag']);
            $error = $this->Html->tag($tag, $error, $wrap);
        }

        return $error;
    }

    public function select($fieldName, $options = [], $attributes = [])
    {
        $defaults = [
            'placeholder' => null,
            'help-block' => null,
            'multiple' => null,
            'class' => 'form-control',
            'div' => [
                'tag' => 'div',
                'col' => 'col-sm-8',
            ],
        ];

        $attributes += $defaults;
        $attributes = $this->_addHelpBlockParams($attributes);
        $help_block = Hash::get($attributes, 'help-block');
        unset($attributes['help-block']);

        $divOptions = $attributes['div'];
        unset($attributes['div']);

        if (empty($attributes['multiple']) === false) {
            unset($attributes['empty']);
        }

        $result = parent::select($fieldName, $options, $attributes);
        $tag = $divOptions['tag'];
        unset($divOptions['tag']);

        $result .= $this->_helpBlock($fieldName, $attributes + ['help-block' => $help_block]);

        return $this->Html->tag($tag, $result, $this->_moveColAndColOffset($divOptions));
    }

    /**
     * Retourne un élément textarea, éventuellement enveloppé dans un élément (
     * voir la clé wrap). Les clés col et col-offset sont également traitées.
     * Les valeurs par défaut sont configurables via la classe Configure.
     *
     * @param string $fieldName Le chemin du champ, sous la forme "Alias.champ"
     * @param array $options Un array d'attributs HTML.
     * @return string
     */
    public function textarea($fieldName, $options = [])
    {
        $options = $this->_mergeDefaults($options, __FUNCTION__);

        $wrap = Hash::get($options, 'wrap');
        unset($options['wrap']);

        $options = $this->_addHelpBlockParams($options);
        $help_block = Hash::get($options, 'help-block');
        unset($options['help-block']);

        $options = $this->_moveColAndColOffset($options);
        $result = parent::textarea($fieldName, $options);
        $result .= $this->_helpBlock($fieldName, $options + ['help-block' => $help_block]);

        return $this->wrap($result, $wrap);
    }

    /**
     * Retourne un élément input file, éventuellement enveloppé dans un élément (
     * voir la clé wrap). Les clés col et col-offset sont également traitées.
     * Les valeurs par défaut sont configurables via la classe Configure.
     *
     * @param string $fieldName Le chemin du champ, sous la forme "Alias.champ"
     * @param array $options Un array d'attributs HTML.
     * @return string
     */
    public function file($fieldName, $options = [])
    {
        $options = $this->_mergeDefaults($options, __FUNCTION__);
        $wrap = Hash::get($options, 'wrap');
        unset($options['wrap']);

        $options = $this->_moveColAndColOffset($options);
        $result = parent::file($fieldName, $options);

        return $this->wrap($result, $wrap);
    }

    public function dateTimeHtml5($fieldName, $type = null, $attributes = [])
    {
        $attributes += ['minYear' => null, 'maxYear' => null, 'interval' => null];
        $extra = [
            'min' => $attributes['minYear'] === null ? null : "{$attributes['minYear']}-01-01",
            'max' => $attributes['maxYear'] === null ? null : "{$attributes['maxYear']}-12-31",
            'step' => $attributes['interval'] === null ? null : $attributes['interval'] * 60,
        ];
        $attributes = array_except_keys($attributes, ['monthNames', 'empty', 'minYear', 'maxYear', 'interval']);

        // @todo: data- pour les autres attributs
        // interval est en minutes, step en secondes
        if ($type === 'date') {
            return $this->text($fieldName, ['type' => 'date'] + $attributes + $extra);
        } elseif ($type === 'time') {
            return $this->text($fieldName, ['type' => 'time'] + $attributes + $extra);
        } else {
            return $this->text($fieldName, ['type' => 'datetime-local'] + $attributes + $extra);
        }
    }

    /**
     * @todo: splitter en 3 méthodes (publiques)
     */
    public function dateTime($fieldName, $dateFormat = 'DMY', $timeFormat = '12', $attributes = [])
    {
        $attributes += ['mode' => null]; // @todo: 3 modes -> CakePHP, HTML5, jQuery
        $mode = strtolower($attributes['mode']);
        unset($attributes['mode']);

        if ($mode === 'html5') {
            $type = 'datetime';
            if ($timeFormat === null) {
                $type = 'date';
            } elseif ($dateFormat === null) {
                $type = 'time';
            }

            return $this->dateTimeHtml5($fieldName, $type, $attributes);
        } elseif ($mode === 'jquery') {
            // @see https://jqueryui.com/datepicker/
            //      $(".datepicker").last().datepicker({dateFormat: "dd/mm/yy"})
            //      $.datepicker.regional['fr']
            //------------------------------------------------------------------
            // French initialisation for the jQuery UI date picker plugin.
            // Written by Keith Wood (kbwood{at}iinet.com.au), Stéphane Nahmani (sholby@sholby.net), Stéphane Raimbault <stephane.raimbault@gmail.com>
/*
( function( factory ) {
    if ( typeof define === "function" && define.amd ) {

        // AMD. Register as an anonymous module.
        define( [ "../widgets/datepicker" ], factory );
    } else {

        // Browser globals
        factory( jQuery.datepicker );
    }
}( function( datepicker ) {

datepicker.regional.fr = {
    closeText: "Fermer",
    prevText: "Précédent",
    nextText: "Suivant",
    currentText: "Aujourd'hui",
    monthNames: [ "janvier", "février", "mars", "avril", "mai", "juin",
        "juillet", "août", "septembre", "octobre", "novembre", "décembre" ],
    monthNamesShort: [ "janv.", "févr.", "mars", "avr.", "mai", "juin",
        "juil.", "août", "sept.", "oct.", "nov.", "déc." ],
    dayNames: [ "dimanche", "lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi" ],
    dayNamesShort: [ "dim.", "lun.", "mar.", "mer.", "jeu.", "ven.", "sam." ],
    dayNamesMin: [ "D","L","M","M","J","V","S" ],
    weekHeader: "Sem.",
    dateFormat: "dd/mm/yy",
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: "" };
datepicker.setDefaults( datepicker.regional.fr );

return datepicker.regional.fr;

} ) );
*/
            // @see https://igorescobar.github.io/jQuery-Mask-Plugin/
            // @see https://www.tjvantoll.com/2012/06/30/creating-a-native-html5-datepicker-with-a-fallback-to-jquery-ui/
            // @todo: data- pour les autres attributs
            unset($attributes['monthNames'], $attributes['empty']);
            if ($timeFormat === null) {
                return $this->text($fieldName, ['data-type' => 'date'] + $attributes);
            } elseif ($dateFormat === null) {
                return $this->text($fieldName, ['data-type' => 'time'] + $attributes);
            } else {
                return $this->text($fieldName, ['data-type' => 'datetime-local'] + $attributes);
            }
        } else {
            return parent::dateTime($fieldName, $dateFormat, $timeFormat, $attributes);
        }
    }

    public function checkbox($fieldName, $options = [])
    {
        return parent::checkbox($fieldName, $options);
    }

    protected function _selectOptions($elements = [], $parents = [], $showParents = null, $attributes = [])
    {
        if ($attributes['style'] === 'checkbox') {
            $attributes['class'] = null;
            $attributes['label'] = ['class' => false];
        }

        return parent::_selectOptions($elements, $parents, $showParents, $attributes);
    }

    protected function _getLabel($fieldName, $options)
    {
        if ($options['type'] === 'staticctrl') {
            $text = $options['label'] ?? null;
            $label = $this->_inputLabelText($fieldName, $text, $options);
            $label = $this->Html->tag('strong', $label);
            $result = $this->Html->tag('p', $label, ['class' => 'control-label col-sm-4']);
        } else {
            $result = parent::_getLabel($fieldName, $options);
        }

        return $result;
    }

    public function staticctrl($fieldName, $options = [])
    {
        $this->setEntity($fieldName);
        $options = $this->_parseOptions($options);
        if (in_array($options['type'], ['hidden', 'select']) === true) {
            $options['type'] = 'text';
        }
        $options += ['data-format' => $options['type']];

        $divOptions = $this->_divOptions($options);
        unset($options['div']);

        $options = $this->addClass($options, "col-sm-8 form-control-static {$options['type']}");

        if (isset($options['value']) === true) {
            $value = $options['value'];
            unset($options['value']);
        } else {
            $value = $this->value($fieldName);
        }

        //@todo: si value est un array, ...
        $data = Hash::insert([], $fieldName, $value);
        $value = $this->LibricielBootstrap3Table->data($fieldName, $data, $options);

        unset($options['id'], $options['type'], $options['data-format'], $options['maxlength'], $options['options']);

        return $this->Html->tag('p', $value, $options);
    }

    /**
     * Fonction utilitaire pour ajouter un fieldset et une légende.
     *
     * @param string $legend Le contenu de la balise legend
     * @param string $content Le contenu du fieldset
     * @param array $options Les options qui seront passées au fieldset
     * @return string
     */
    public function fieldset($legend, $content, $options = [])
    {
        $legend = $this->Html->tag('legend', $legend);

        return $this->Html->tag('fieldset', $legend . $content, $options);
    }

    public function fiddle($fieldName, $options = [])
    {
        $options['type'] = 'text';
        $options['style'] = 'display: none';
        $options = $this->addClass($options, 'invisible');

        return parent::text($fieldName, $options);
    }
}
