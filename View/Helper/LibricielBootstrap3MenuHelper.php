<?php
/**
 * La classe LibricielBootstrap3MenuHelper simplifie l'écriture d'un menu pour web-dpo
 * en fournissant des méthodes facilitant la construction de menus sous forme de
 * liste non ordonnées (ul) imbriquées.
 */
App::uses('AppHelper', 'View/Helper');
App::uses('Hash', 'Utility');
App::uses('LibricielBootstrap3Url', 'LibricielBootstrap3.Utility');

class LibricielBootstrap3MenuHelper extends AppHelper
{
    /**
     * Helpers utilisés par ce helper.
     *
     * @var array
     */
    public $helpers = ['Html'];

    protected $_options = [
        'caret' => [
            'tag' => 'span',
            'content' => '<!-- -->',
            'params' => ['class' => 'caret'],
        ],
        'divider' => [
            'tag' => 'li',
            'content' => '<!-- -->',
            'params' => [
                'class' => 'divider',
                'role' => 'separator',
            ],
        ],
        'icon' => [
            'tag' => 'span',
            'content' => '<!-- -->',
            'params' => ['class' => 'fa fa-fw'],
        ],
        'title' => [
            'tag' => 'li',
            'params' => [
                'class' => 'dropdown-header',
                'role' => 'none',
            ],
        ],
        'text' => [
            'tag' => 'li',
            'params' => [
                'class' => 'navbar-text',
                'role' => 'presentation',
            ],
        ],
    ];

    protected function _isDivider($key, $item)
    {
        return is_numeric($key) && $item === 'divider';
    }

    public function divider($key, $item)
    {
        return $this->Html->tag(
            $this->_options['divider']['tag'],
            $this->_options['divider']['content'],
            $this->_options['divider']['params']
        );
    }

    protected function _isTitle($key, $item)
    {
        return is_numeric($key) && is_string($item);
    }

    public function title($key, $item)
    {
        return $this->Html->tag($this->_options['title']['tag'], $item, $this->_options['title']['params']);
    }

    protected function _isDisabled($key, $item)
    {
        return isset($item['disabled']) && $item['disabled'];
    }

    protected function _isEntry($key, $params)
    {
        return isset($params['url']) && ( $params['url'] != '#' );
    }

    protected function _isText($key, $params)
    {
        return isset($params['type']) && ( $params['type'] === 'text' );
    }

    public function entry($text, array $item, array $extra = [])
    {
        $url = LibricielBootstrap3Url::parse($item['url']);
        $icon = $this->Html->tag(
            $this->_options['icon']['tag'],
            $this->_options['icon']['content'],
            [
                'class' => $item['class'] === false
                ? false
                : "{$this->_options['icon']['params']['class']} {$item['class']}",
            ]
        );
        $options = $extra + ['escape' => false, 'title' => $item['title']];

        return $this->Html->link("{$icon} {$text}", $url, $options + ['role' => 'menuitem']);
    }

    public function caret()
    {
        return $this->Html->tag(
            $this->_options['caret']['tag'],
            $this->_options['caret']['content'],
            $this->_options['caret']['params']
        );
    }

    public function text($content, array $params)
    {
        $class = Hash::get($params, 'class');
        unset($params['type'], $params['class']);
        $params = array_merge($this->_options['text']['params'], $params);
        $params = $this->addClass($params, $class);

        return $this->Html->tag(
            $this->_options['text']['tag'],
            $content,
            $params
        );
    }

    /**
     * Permet de construire un menu à plusieurs niveaux.
     *
     * Cette méthode ajoute la possibilité de:spécifier la balise qui sera
     * utilisée pour construire les éléments inactifs "parents" d'éléments
     * actifs, d'ajouter un attribut title aux éléments et de désactiver un
     * élément (et ses sous-éléments) en plus des permissions.
     *
     * @see https://www.w3.org/TR/wai-aria-practices/examples/menubar/menubar-1/menubar-1.html
     *
     * @param array $items Les éléments du menu
     * @fixme
     * @return string
     */
    public function main($items, $tag = 'a', $level = 0, $extraTopUlClass = null)
    {
        $result = '';

        foreach ($items as $key => $item) {
            // Séparateur
            if ($this->_isDivider($key, $item) === true) {
                $result .= $this->divider($key, $item);
            // Titre
            } elseif ($this->_isTitle($key, $item) === true) {
                $result .= $this->title($key, $item);
            // Text
            } elseif ($this->_isText($key, $item) === true) {
                $result .= $this->text($key, (array)$item);
            // Sous-menu ou élément de menu
            } elseif ($this->_isDisabled($key, $item) === false) {
                $content = '';
                $paramsKeys = ['url', 'disabled', 'title', 'class'];
                $defaultParams = ['title' => false, 'class' => false];
                $params = array_extract_keys($item, $paramsKeys) + $defaultParams;

                $subMenuParams = array_except_keys_by_prefix(array_except_keys($item, $paramsKeys), 'data-');
                $subMenu = $this->main($subMenuParams, $tag, $level + 1);

                if ($this->_isEntry($key, $params)) {
                    $extra = array_extract_keys_by_prefix($item, 'data-');
                    $content = $this->entry($key, $params, $extra);
                } elseif (empty($subMenu) === false) {
                    $options = ['title' => $params['title']];
                    if ($tag == 'a') {
                        $options['href'] = '#';
                        $options['role'] = 'menuitem';
                    } else {
                        $options['role'] = 'none';
                    }
                    $options['aria-haspopup'] = 'true';
                    $options['aria-expanded'] = 'false';
                    if ($level === 0) {
                        $options += ['class' => 'dropdown-toggle', 'data-toggle' => 'dropdown'];
                        $content = $this->Html->tag($tag, $key . $this->caret(), $options);
                    } else {
                        $content = $this->Html->tag($tag, $key, $options);
                    }
                }

                $content .= $subMenu;

                if (empty($content) === false) {
                    $class = (empty($subMenu) ? '' : ($level === 0 ? 'dropdown' : 'dropdown-submenu'));
                    $result .= $this->Html->tag('li', $content, ['class' => $class, 'role' => 'none']);
                }
            }
        }

        $options = $level === 0
            ? ['class' => "nav navbar-nav {$extraTopUlClass}", 'role' => 'menubar']
            : ['class' => 'dropdown-menu', 'role' => 'menu'];

        return empty($result) ? $result : $this->Html->tag('ul', $result, $options);
    }
}
