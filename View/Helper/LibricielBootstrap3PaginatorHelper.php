<?php
App::uses('PaginatorHelper', 'View/Helper');

// @todo indicateur en CSS pour les liens sort (asc/desc)
class LibricielBootstrap3PaginatorHelper extends PaginatorHelper
{
    public function first($first = '<< first', $options = [])
    {
        $defaults = [
            'tag' => null,
            'after' => null,
            'model' => $this->defaultModel(),
            'separator' => null,
            'ellipsis' => '...',
            'class' => null,
        ];
        $options = (array)$options + $defaults;

        $result = parent::first($first, $options);
        if (empty($result)) {
            $span = $this->Html->tag('span', h($first));
            $result = $this->Html->tag('li', $span, ['class' => 'first disabled']);
        } else {
            $result = $this->Html->tag('li', $result, ['class' => 'first']);
        }

        return $result;
    }

    public function prev($title = '<< Previous', $options = [], $disabledTitle = null, $disabledOptions = [])
    {
        $defaults = [
            'tag' => 'li',
            'rel' => 'prev',
            'model' => null,
            'title' => null,
        ];
        $options = (array)$options + $defaults;

        if ($this->hasPrev($options['model'])) {
            $result = parent::prev($title, $options, $disabledTitle, $disabledOptions);
        } else {
            $tag = $options['tag'];
            unset($options['model'], $options['rel'], $options['tag']);
            $span = $this->Html->tag('span', h($title));
            $options = $this->addClass($options, 'disabled');
            $result = $this->Html->tag($tag, $span, $options);
        }

        return $result;
    }

    public function next($title = 'Next >>', $options = [], $disabledTitle = null, $disabledOptions = [])
    {
        $defaults = [
            'tag' => 'li',
            'rel' => 'next',
            'model' => null,
            'title' => null,
        ];
        $options = (array)$options + $defaults;

        if ($this->hasNext($options['model'])) {
            $result = parent::next($title, $options, $disabledTitle, $disabledOptions);
        } else {
            $tag = $options['tag'];
            unset($options['model'], $options['rel'], $options['tag']);
            $span = $this->Html->tag('span', h($title));
            $options = $this->addClass($options, 'disabled');
            $result = $this->Html->tag($tag, $span, $options);
        }

        return $result;
    }

    public function last($last = 'last >>', $options = [])
    {
        $defaults = [
            'tag' => null,
            'after' => null,
            'model' => $this->defaultModel(),
            'separator' => null,
            'ellipsis' => '...',
            'class' => null,
        ];
        $options = (array)$options + $defaults;

        $result = parent::last($last, $options);
        if (empty($result)) {
            $span = $this->Html->tag('span', h($last));
            $result = $this->Html->tag('li', $span, ['class' => 'last disabled']);
        } else {
            $result = $this->Html->tag('li', $result, ['class' => 'last']);
        }

        return $result;
    }

    public function link($title, $url = [], $options = [])
    {
        $options = (array)$options;

        $titleAttribute = Hash::get($options, 'title');
        if ($titleAttribute === true && is_numeric($title) === true) {
            $options['title'] = __m('Page %d', $title);
        }

        return parent::link($title, $url, $options);
    }
}
