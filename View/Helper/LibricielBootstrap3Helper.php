<?php
/**
 * La classe LibricielBootstrap3Helper simplifie l'écriture des éléments de base
 * de l'application Echanges Gratuits.
 */
App::uses('AppHelper', 'View/Helper');
App::uses('Hash', 'Utility');

class LibricielBootstrap3Helper extends AppHelper
{
    /**
     * Helpers utilisés par ce helper.
     *
     * @var array
     */
    public $helpers = [
        'Html',
        'LibricielBootstrap3.LibricielBootstrap3Form',
        'LibricielBootstrap3.LibricielBootstrap3Paginator',
        'LibricielBootstrap3.LibricielBootstrap3Table',
    ];

    /**
     * Lecture du cache.
     *
     * @fixme: on n'y passe pas ?
     *
     * @param string $viewFile The view file that is going to be rendered
     * @return void
     */
    public function beforeRender($viewFile)
    {
        parent::beforeRender($viewFile);
        $this->LibricielBootstrap3Table->beforeRender($viewFile);
    }

    /**
     * Sauvegarde du cache.
     *
     * @param string $layoutFile The layout file that was rendered.
     * @return void
     */
    public function afterLayout($layoutFile)
    {
        parent::afterLayout($layoutFile);
        $this->LibricielBootstrap3Table->afterLayout($layoutFile);
    }

    public function actions(array $actions, array $params = [])
    {
        if (empty($actions) === true) {
            return null;
        }

        $defaults = ['class' => 'btn-group'];
        $params += $defaults;
        $result = '';

        foreach (Hash::normalize($actions) as $action => $actionParams) {
            $result .= $this->LibricielBootstrap3Table->action($action, [], (array)$actionParams);
        }

        return $this->Html->tag('div', $result, $params);
    }

    /**
     * @todo
     *  - Labelling the pagination component: https://getbootstrap.com/docs/3.3/components/#pagination-default
     *  - Paramètre title (true vs. null / false)
     *  - SSI on peut facilement ajouter des liens aux liens du counter
     */
    public function pagination(array $params = [])
    {
        // @todo nettoyer les différents défaults
        $allDefaults = [
            'before' => null,
            'after' => null,
            'model' => null,
            'class' => null,
            'separator' => null,
        ];
        $counterDefaults = [
            'format' => __m('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}'),
        ];
        $defaults = [
            'first' => null,
            'last' => null,
            'tag' => 'li',
            'modulus' => '8',
            'ellipsis' => '...',
            'currentClass' => 'active',
            'currentTag' => 'span',
            'title' => null,
        ] + $allDefaults;

        $title = Hash::get($params, 'title') === true;

        $counter = $this->LibricielBootstrap3Paginator->counter($params + $counterDefaults);
        $pagination = $this->Html->tag('p', $counter, ['class' => 'pagination-counter']);
        unset($params['format'], $params['key']);

        $modelClass = Hash::get($params, 'model');

        $numbers = $this->LibricielBootstrap3Paginator->numbers($params + $defaults + $allDefaults);
        if (!empty($numbers)) {
            $first = $this->LibricielBootstrap3Paginator->first(
                __m('<< first'),
                ['title' => $title === true ? __m('<< first:title') : null]
            );
            $prev = $this->LibricielBootstrap3Paginator->prev(
                __m('< prev'),
                ['title' => $title === true ? __m('< prev:title') : null]
            );
            $next = $this->LibricielBootstrap3Paginator->next(
                __m('next >'),
                ['title' => $title === true ? __m('next >:title') : null]
            );
            $last = $this->LibricielBootstrap3Paginator->last(
                __m('last >>'),
                ['title' => $title === true ? __m('last >>:title') : null]
            );

            $pagination .= $this->Html->tag(
                'ul',
                "{$first} {$prev} {$numbers} {$next} {$last}",
                ['class' => 'pagination']
            );
        }

        return $this->Html->tag('div', $pagination, ['class' => 'pagination-block']);
    }

    // @todo
    protected $_defaults = [
        'form' => [
            'buttons' => ['Cancel', 'Save'],
        ],
        'index' => [
            'empty-classes' => 'alert alert-info',
            'empty-message' => 'No result',
            'responsive' => false,
        ],
    ];

    public function index(array $records, array $cells, array $params = [])
    {
        $params += $this->_defaults[__FUNCTION__];
        $empty = [
            'classes' => $params['empty-classes'],
            'message' => $params['empty-message'],
        ];
        $responsive = $params['responsive'];
        unset($params['empty-classes'], $params['empty-message'], $params['responsive']);

        if (empty($records) === true) {
            $result = $this->Html->tag(
                'div',
                __m($empty['message']),
                ['class' => $empty['classes']]
            );
        } else {
            $pagination = $this->pagination($params);
            $result = $this->LibricielBootstrap3Table->table($records, $cells, $params + ['responsive' => $responsive]);
            $result = $pagination . $result . $pagination;
        }

        return $result;
    }

    public function subform(array $fields, array $params = [])
    {
        $result = null;
        if (empty($fields) === false) {
            if (isset($params['options']) && empty($params['options']) === false) {
                $fields = Hash::normalize($fields);

                foreach ($fields as $fieldName => $fieldParams) {
                    $fieldParams = (array)$fieldParams;
                    $path = "options.{$fieldName}";
                    if (isset($fieldParams['options']) === false && Hash::check($params, $path)) {
                        $fieldParams['options'] = Hash::get($params, $path);
                        $fieldParams += ['type' => 'select'];
                    }

                    $fields[$fieldName] = $fieldParams;
                }
            }

            $result = $this->LibricielBootstrap3Form->inputs($fields, $params);
        }

        return $result;
    }

    public function form(array $fields, array $params = [])
    {
        $params += $this->_defaults[__FUNCTION__];
        $buttons = $params['buttons'];
        unset($params['buttons']);

        $result = null;
        if (empty($fields) === false) {
            $result = $this->LibricielBootstrap3Form->create();
            $result .= $this->subform($fields, $params);
            $result .= $this->LibricielBootstrap3Form->buttons($buttons);
            $result .= $this->LibricielBootstrap3Form->end();
        }

        return $result;
    }

    public function subview(array $record, array $fields, array $params = [])
    {
        $result = null;

        if (empty($record) === false && empty($fields) === false) {
            $fields = Hash::normalize($fields);
            foreach ($fields as $fieldName => $options) {
                $fieldParams = [
                    'type' => 'staticctrl',
                    'value' => Hash::get($record, $fieldName),
                ] + (array)$options + [
                    'data-format' => null,
                    'required' => false,
                ];
                $fields[$fieldName] = $fieldParams;
            }

            $result .= $this->subform($fields, $params);
        }

        return $result;
    }

    public function view(array $record, array $fields, array $params = [])
    {
        $result = null;

        $content = $this->subview($record, $fields, $params);
        if (empty($content) === false) {
            $result = $this->Html->tag('div', $content, ['class' => 'form-horizontal']);
        }

        return $result;
    }
}
