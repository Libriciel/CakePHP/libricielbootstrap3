<?php
App::uses('AppHelper', 'View/Helper');
App::uses('LibricielBootstrap3DataInterface', 'LibricielBootstrap3.Utility');
App::uses('LibricielBootstrap3Url', 'LibricielBootstrap3.Utility');

class LibricielBootstrap3TableHelper extends AppHelper
{
    /**
     * Helpers utilisés par ce helper.
     *
     * @var array
     */
    public $helpers = [
        'Form' => [
            'className' => 'LibricielBootstrap3.LibricielBootstrap3Form',
        ],
        'Html',
        'LibricielBootstrap3.LibricielBootstrap3Paginator',
    ];

    /**
     * La liste des types de champs, au sens CakePHP, par nom de modèle.
     *
     * @var array
     */
    protected $_cache = [];

    /**
     * Permet de savoir si le cache a été modifié.
     *
     * @var bool
     */
    protected $_cacheChanged = false;

    /**
     * Classes à ajouter pour les liens d'actions et les îcones suivant l'action
     * du lien.
     * Le contrôleur et le plugin peuvent aussi être pris en compte (underscore).
     *
     * Exemple:
     *  [
     *      'table' => 'table table-hover table-striped table-condensed',
     *      'buttons' => [
     *          'add' => 'fa fa-lg fa-plus-circle',
     *          'view' => 'fa fa-lg fa-eye',
     *          'edit' => 'fa fa-lg fa-pencil',
     *          'delete' => 'fa fa-lg fa-trash',
     *          'groups:view' => 'fa fa-lg fa-user'
     *      ],
     *      'icons' => [
     *          'add' => 'btn-success',
     *          'delete' => 'btn-danger',
     *          'edit' => 'btn-primary',
     *          'view' => 'btn-default',
     *          'groups:view' => 'btn-info'
     *      ]
     *  ]
     *
     * @var array
     */
    protected $_classes = [
        'table' => 'table table-hover table-striped table-condensed',
        'buttons' => [
            'add' => 'fa fa-lg fa-plus-circle',
            'view' => 'fa fa-lg fa-eye',
            'edit' => 'fa fa-lg fa-pencil',
            'delete' => 'fa fa-lg fa-trash',
        ],
        'icons' => [
            'add' => 'btn-success',
            'delete' => 'btn-danger',
            'edit' => 'btn-primary',
            'view' => 'btn-default',
        ],
    ];

    public function engine($settings)
    {
        list($plugin, $engineClass) = pluginSplit($settings['engine'], true);
        App::uses($engineClass, $plugin . 'Utility');

        if (class_exists($engineClass)) {
            if (in_array('LibricielBootstrap3DataInterface', class_implements($engineClass, false), true) === false) {
                throw new RuntimeException(__d('cake_dev', 'Engine class "%s" should implement LibricielBootstrap3DataInterface', $engineClass));
            }
            $this->_engine = new $engineClass($settings);
        } else {
            throw new RuntimeException(__d('cake_dev', 'Engine class "%s" could not be found', $engineClass));
        }
    }

    public function classes($config)
    {
        if (is_string($config) === true) {
            $config = Configure::read($config);
        }

        return $this->_classes = Hash::merge($this->_classes, (array)$config);
    }

    public function __construct(View $View, $settings = [])
    {
        $configureKey = 'LibricielBootstrap3.' . self::class;
        $config = (array)Configure::read($configureKey);
        $defaults = [
            'engine' => 'LibricielBootstrap3.LibricielBootstrap3Data',
            'classes' => $this->_classes,
        ];

        $settings = Hash::merge($defaults, $config, $settings);
        parent::__construct($View, $settings);

        $this->engine($settings);
        $this->classes($settings['classes']);
    }

    /**
     * Retourne le com de la clé de cache qui sera utilisée par ce helper.
     *
     * @return string
     */
    public function cacheKey()
    {
        $params = [
            Inflector::camelize(self::class),
            Inflector::camelize($this->request->params['plugin']),
            Inflector::camelize($this->request->params['controller']),
            $this->request->params['action'],
        ];

        return implode('_', Hash::filter($params));
    }

    /**
     * Lecture du cache.
     *
     * @fixme
     *
     * @param string $viewFile The view file that is going to be rendered
     * @return void
     */
    public function beforeRender($viewFile)
    {
        parent::beforeRender($viewFile);
        $cacheKey = $this->cacheKey();
        $cache = Cache::read($cacheKey, $this->_cacheConfig);

        if ($cache !== false) {
            $this->_cache = $cache;
        }
    }

    /**
     * Sauvegarde du cache.
     *
     * @param string $layoutFile The layout file that was rendered.
     * @return void
     */
    public function afterLayout($layoutFile)
    {
        parent::afterLayout($layoutFile);

        if ($this->_cacheChanged) {
            $cacheKey = $this->cacheKey();
            Cache::write($cacheKey, $this->_cache, $this->_cacheConfig);
        }
    }

    protected function _isLink($path)
    {
        return strpos($path, '/') === 0;
    }

    protected function _eval($expression)
    {
        $result = null;
        $thrown = false;

        try {
            $result = eval("return ( {$expression} );");
        } catch (Throwable $t) {
            $thrown = true;
        }

        if ($thrown === true) {
            try {
                $result = stripslashes(eval("return ( '" . addslashes($expression) . "' );"));
            } catch (Throwable $t) {
                // On n'arrive jamais ici ?
                $message = sprintf("%s (code %s) in %s:%d", $t->getMessage(), $t->getCode(), $t->getFile(), $t->getLine());
                trigger_error($message, E_USER_NOTICE);
            }
        }

        return $result;
    }

    public function thead(array $cells, array $params = [])
    {
        $defaults = [
            'group-actions' => null,
        ];
        $params += $defaults;

        $row = '';
        $actionsColspan = 0;

        foreach (Hash::normalize($cells) as $path => $cell) {
            $cell = (array)$cell + ['sort' => true];
            if ($this->_isLink($path) === false) {
                $text = __m($path);
                if ($cell['sort'] === true) {
                    $content = $this->LibricielBootstrap3Paginator->sort($path, $text);
                } else {
                    $content = $text;
                }
                $row .= $this->Html->tag('th', $content);
            } else {
                $actionsColspan++;
            }
        }

        if ($actionsColspan > 0) {
            if ($params['group-actions'] === true) {
                $actionsColspan = 1;
            }
            $row .= $this->Html->tag('th', __m('Actions'), ['colspan' => $actionsColspan, 'class' => 'actions']);
        }

        return $this->Html->tag('thead', $this->Html->tag('tr', $row));
    }

    protected function _matchingUrl(array $haystack, $url)
    {
        if (is_array($url)) {
            $ext = Hash::get($url, 'ext');
            $keys = [
                "{$url['plugin']}.{$url['controller']}:{$url['action']}.{$ext}",
                "{$url['plugin']}.{$url['controller']}:{$url['action']}",
                "{$url['controller']}:{$url['action']}.{$ext}",
                "{$url['controller']}:{$url['action']}",
                "{$url['action']}.{$ext}",
                "{$url['action']}",
            ];
        } else {
            $keys = (array)$url;
        }

        foreach ($keys as $key) {
            $key = trim($key, '.');
            if (isset($haystack[$key])) {
                return $haystack[$key];
            }
        }

        return null;
    }

    /**
     * @todo: refactoriser ailleurs
     */
    public function action($path, array $record, array $params = [])
    {
        $params += ['escape' => false, 'type' => null, 'hidden' => false];
        $url = isset($params['value'])
            ? $this->_eval($this->_engine->evaluate($record, $params['value']))
            : $this->_engine->evaluate($record, $path);

        $parsed = LibricielBootstrap3Url::parse($url);
        foreach (['title', 'confirm'] as $key) {
            if (isset($params[$key]) === true) {
                if ($params[$key] === true) {
                    if ($key === 'title') {
                        $params[$key] = h($this->_engine->evaluate($record, __m("{$path}:title")));
                    } else {
                        $params[$key] = $this->_engine->evaluate($record, __m("{$path}:confirm"));
                    }
                } elseif ($params[$key] !== false) {
                    if ($key === 'title') {
                        $params[$key] = h($this->_engine->evaluate($record, (string)$params[$key]));
                    } else {
                        $params[$key] = $this->_engine->evaluate($record, (string)$params[$key]);
                    }
                }
            }
        }
        $params['hidden'] = $this->_eval($this->_engine->evaluate($record, $params['hidden']));

        if ($params['hidden'] == true) {
            return ' ';
        }
        unset($params['hidden']);

        $groupActions = Hash::get($params, 'group-actions');
        unset($params['group-actions']);

        $text = h(__m($path));
        // Icône
        $iconClasses = '';
        $matchingIconClasses = $this->_matchingUrl($this->_classes['buttons'], $parsed);
        if ($matchingIconClasses !== null) {
            $iconClasses = $matchingIconClasses;
        }
        $iconSpanClasses = 'text' . ($groupActions === true ? ' sr-only' : '');
        $text = $this->Html->tag('span', ' ', ['class' => $iconClasses])
            . ' '
            . $this->Html->tag('span', $text, ['class' => $iconSpanClasses]);

        // Classe du bouton
        $btnClass = $this->_matchingUrl($this->_classes['icons'], $parsed);
        $params = $this->addClass($params, "btn btn-sm {$btnClass}");
        foreach (['plugin', 'controller', 'action'] as $key) {
            if (empty($parsed[$key]) === false) {
                $params = $this->addClass($params, "{$key}-{$parsed[$key]}");
            }
        }

        if (isset($params['disabled'])) {
            if (is_string($params['disabled'])) {
                $disabled = $this->_eval($this->_engine->evaluate($record, $params['disabled']));
            }

            if ((bool)$disabled !== false) {
                $parsed = '#';
                $params = $this->addClass($params, 'disabled');
            }
        }

        $type = $params['type'];
        unset($params['type'], $params['options']);
        if ($type === 'postLink') {
            return $this->Form->postLink($text, $parsed, $params);
        } else {
            return $this->Html->link($text, $parsed, $params);
        }
    }

    public function type($path)
    {
        list($modelName, $fieldName) = explode('.', $path);

        if (!isset($this->_cache[$modelName])) {
            try {
                $Model = ClassRegistry::init($modelName);
                $schema = $Model->schema();
                $schema = array_combine(array_keys($schema), Hash::extract($schema, '{s}.type'));
                $this->_cache[$modelName] = $schema;
                $this->_cacheChanged = true;
            } catch (Exception $e) {
                $this->_cache[$modelName] = [];
            }
        }

        if (isset($this->_cache[$modelName][$fieldName])) {
            return $this->_cache[$modelName][$fieldName];
        }

        return null;
    }

    public function data($path, array $record, array $params = [])
    {
        $type = isset($params['type']) && !empty($params['type'])
            ? $params['type']
            : $this->type($path);
        $value = isset($params['value'])
            ? $this->_eval($this->_engine->evaluate($record, $params['value']))
            : Hash::get($record, $path);

        if (Hash::check($params, "options.{$path}.{$value}")) {
            $value = Hash::get($params, "options.{$path}.{$value}");
        } elseif (Hash::check($params, "options.{$value}")) {
            $value = Hash::get($params, "options.{$value}");
        }

        if ($value !== null) {
            $format = Hash::get($params, 'data-format');
            if (method_exists('LibricielBootstrap3Data', $format)) {
                $params['data-format'] = null;
                $value = $this->_engine->{$format}($value, $params);
            } elseif (method_exists('LibricielBootstrap3Data', $type)) {
                $value = $this->_engine->{$type}($value, $params);
            } elseif (empty($format) === false && $format !== 'text') {
                $value = $this->_engine->format($value, $format);
            }
            $value = h($value);
        }

        return $value;
    }

    /**
     * @todo Pouvoir décaler le bouton supprimer
     */
    protected function _actionCellParams(array $record, $path, array $params = [])
    {
        $cellParams = [
            'class' => Hash::get($params, 'class'),
        ];
        unset($params['class']);

        $cellParams = $this->_engine->evaluate($record, $cellParams);
        if (empty($cellParams['class']) === false) {
            $cellParams['class'] = $this->_eval($cellParams['class']);
        }

        $url = $this->_engine->evaluate($record, $path);
        $parsed = LibricielBootstrap3Url::parse($url);
        $cellParams = $this->addClass($cellParams, 'action');
        foreach (['plugin', 'controller', 'action'] as $key) {
            if (isset($parsed[$key]) === true && empty($parsed[$key]) === false) {
                $cellParams = $this->addClass($cellParams, $parsed[$key]);
            }
        }

        return $cellParams;
    }

    protected function _dataCellParams(array $record, $path, array $params = [])
    {
        $cellParams = [
            'class' => Hash::get($params, 'class'),
        ];
        unset($params['class']);

        $cellParams = $this->_engine->evaluate($record, $cellParams);
        if (empty($cellParams['class']) === false) {
            $cellParams['class'] = $this->_eval($cellParams['class']);
        }

        if (isset($params['type']) === true) {
            $type = $params['type'];
        } else {
            $type = $this->type($path, $params);
        }

        if (empty($type) === true) {
            $type = 'string';
        }

        $cellParams = $this->addClass($cellParams, "data {$type}");

        if ($type === 'string' && isset($params['data-format']) === true) {
            $cellParams = $this->addClass($cellParams, $params['data-format']);
        }

        $value = Hash::get($record, $path);
        if ($value !== null) {
            if (in_array($type, ['biginteger', 'decimal', 'float', 'integer'])) {
                if ($value > 0) {
                    $cellParams = $this->addClass($cellParams, 'positive');
                } elseif ($value < 0) {
                    $cellParams = $this->addClass($cellParams, 'negative');
                } else {
                    $cellParams = $this->addClass($cellParams, 'zero');
                }
            }
        } else {
            $cellParams = $this->addClass($cellParams, 'null');
        }

        $cellParams['class'] = trim($cellParams['class']);

        return $cellParams;
    }

    public function td(array $record, $path, array $params = [])
    {
        $value = null;
        $cellParams = [];
        $iconClass = Hash::get($params, 'icon');
        unset($params['icon']);

        if ($this->_isLink($path)) {
            $cellParams = $this->_actionCellParams($record, $path, $params);
            unset($params['class']);
            $value = $this->action($path, $record, $params);
        } else {
            $cellParams = $this->_dataCellParams($record, $path, $params);
            unset($params['class']);
            $value = $this->data($path, $record, $params);
        }

        if (empty($iconClass) === false) {
            $iconClass = $this->_engine->evaluate($record, $iconClass);
            $iconClass = $this->_eval($iconClass);
            $value = $this->Html->tag('span', ' ', ['class' => $iconClass]) . " {$value}";
        }

        return $this->Html->tag('td', $value, $cellParams);
    }

    public function tbody(array $records, array $cells, array $params = [])
    {
        $defaults = [
            'group-actions' => null,
        ];
        $params += $defaults;
        $rows = '';

        foreach ($records as $record) {
            $actions = [null => []];
            $row = '';

            foreach (Hash::normalize($cells) as $path => $cell) {
                $cell = (array)$cell + ['btn-group' => null];
                if ($params['group-actions'] !== true || $this->_isLink($path) === false) {
                    $row .= $this->td($record, $path, $cell + $params);
                } else {
                    $btn_group = $cell['btn-group'];
                    unset($cell['btn-group']);
                    $action = trim((string)$this->action($path, $record, $cell + $params));

                    if (empty($action) === false) {
                        $actions[$btn_group][] = $action;
                    }
                }
            }

            if ($params['group-actions'] === true && empty($actions) === false) {
                $divs = '';
                foreach (array_keys($actions) as $btn_group) {
                    if (empty($actions[$btn_group]) === false) {
                        $divs .= $this->Html->tag('div', implode(' ', $actions[$btn_group]), ['class' => 'btn-group', 'role' => 'group']);
                    }
                }
                $row .= $this->Html->tag('td', $divs, ['class' => 'actions']);
            }

            $rows .= $this->Html->tag('tr', $row);
        }

        return $this->Html->tag('tbody', $rows);
    }

    public function table(array $records, array $cells, array $params = [])
    {
        $defaults = [
            'group-actions' => null,
            'responsive' => false,
            'tableClass' => $this->_classes['table'],
        ];
        $params += $defaults;
        $class = $params['tableClass'];
        $responsive = $params['responsive'];
        unset($params['tableClass'], $params['responsive']);

        $result = null;

        if (empty($records) === false) {
            $result = $this->Html->tag(
                'table',
                $this->thead($cells, $params)
                . $this->tbody($records, $cells, $params),
                ['class' => $class]
            );

            if ($responsive === true) {
                $result = sprintf('<div class="table-responsive">%s</div>', $result);
            }
        }

        return $result;
    }
}
