<?php
App::uses('LibricielBootstrap3DataInterface', 'LibricielBootstrap3.Utility');

class LibricielBootstrap3Data implements LibricielBootstrap3DataInterface
{
    protected static $_formats = [
        'phone' => [
            'in' => '%2d%2d%2d%2d%2d',
            'out' => '%02d %02d %02d %02d %02d',
        ],
        'siret' => [
            'in' => '%3d%3d%3d%5d',
            'out' => '%03d %03d %03d %05d',
        ],
        'vat' => [
            'in' => '%2s%2d%3d%3d%3d',
            'out' => '%2s %02d %03d %03d %03d',
        ],
    ];

    protected static $_setupFormatsDone = false;

    public static function setup($formats = null)
    {
        if ($formats === null) {
            $formats = 'LibricielBootstrap3.' . self::class . '.formats';
        }

        if (is_string($formats) === true) {
            $formats = (array)Configure::read($formats);
        }

        static::$_setupFormatsDone = true;
        static::$_formats = $formats + static::$_formats;

        return static::$_formats;
    }

    /**
     * Retourne le paramètre $mixed dont les occurences de #Model.champ# ont
     * été remplacées par leur valeur extraite depuis $data.
     *
     * @see Hash::get()
     *
     * @param array $data
     * @param string|array $mixed
     * @return string|array
     */
    public static function evaluate(array $data, $mixed)
    {
        $result = $mixed;
        $isArray = is_array($mixed);
        $out = [];

        if ($isArray === false && preg_match_all('/#[^#\s]+#/', $result, $out)) {
            foreach (array_unique($out[0]) as $token) {
                $value = Hash::get($data, trim($token, '#'));
                $result = str_replace($token, $value, $result);
            }
        } elseif ($isArray === true) {
            $result = [];
            foreach ($mixed as $key => $value) {
                $result[self::evaluate($data, $key)] = self::evaluate($data, $value);
            }
        }

        return $result;
    }

    public static function biginteger($value, array $params = [])
    {
        return static::integer($value, $params);
    }

    public static function boolean($value, array $params = [])
    {
        return empty($value) ? __('No') : __('Yes');
    }

    public static function date($value, array $params = [])
    {
        $format = Hash::get($params, 'data-format');
        $format = $format ?? __m('format:' . __FUNCTION__);

        return strftime($format, strtotime($value));
    }

    public static function datetime($value, array $params = [])
    {
        if (Hash::check($params, 'data-format') === false) {
            $params['data-format'] = __m('format:' . __FUNCTION__);
        }

        return static::date($value, $params);
    }

    public static function decimal($value, array $params = [])
    {
        return static::float($value, $params);
    }

    public static function float($value, array $params = [])
    {
        return number_format($value, 2, ',', ' ');
    }

    public static function integer($value, array $params = [])
    {
        return number_format($value, 0, ',', ' ');
    }

    public static function time($value, array $params = [])
    {
        if (Hash::check($params, 'data-format') === false) {
            $params['data-format'] = __m('format:' . __FUNCTION__);
        }

        return static::date($value, $params);
    }

    public static function format($value, $format)
    {
        if (static::$_setupFormatsDone === false) {
            static::setup();
        }

        $result = $value;

        if (array_key_exists($format, static::$_formats) === true) {
            $parsed = sscanf($value, static::$_formats[$format]['in']);
            if (is_array($parsed) === true) {
                $parsed = array_filter($parsed);
                $params = array_merge([static::$_formats[$format]['out']], $parsed);
                $result = @call_user_func_array('sprintf', $params);
            }
        } else {
            $message = sprintf("%s, unknown format parameter \"%s\".", __METHOD__, $format);
            trigger_error($message, E_USER_NOTICE);
        }

        if ($result === false) {
            $result = $value;
        }

        return $result;
    }
}
