<?php
abstract class LibricielBootstrap3Url
{
    /**
     * Cache for parsed URL strings.
     *
     * @var array
     */
    protected static $_cache = [];

    /**
     * Returns an URL parsed in a CakePHP array or the original string if it
     * could not be parsed.
     *
     * @param array|string $url The URL to be parsed
     * @return array|string
     */
    public static function parse($url)
    {
        if (is_string($url) && strpos($url, '/') === 0) {
            if (preg_match('/^(.*)#(.*)/', $url, $matches)) {
                $url = $matches[1];
                $hash = $matches[2];
            } else {
                $hash = null;
            }

            // Plugin ?
            $matches = [];
            if (preg_match('/^\/([^\/\.]+)\./', $url, $matches) !== 0) {
                $plugin = Inflector::underscore($matches[1]);
                $url = '/' . $plugin . '/' . substr($url, strlen($matches[0]));
            }

            $result = Router::parse($url);

            $pass = (array)Hash::get($result, 'pass');
            unset($result['pass']);

            $result = $result + $pass;
            if ($hash !== null) {
                $result += ['#' => $hash];
            }
        } else {
            $result = $url;
        }

        if (isset($result['controller']) === true) {
            $result['controller'] = Inflector::underscore($result['controller']);
        }

        return $result;
    }
}
