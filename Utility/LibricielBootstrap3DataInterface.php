<?php
interface LibricielBootstrap3DataInterface
{
    public static function setup($formats = null);

    public static function evaluate(array $data, $mixed);

    public static function biginteger($value, array $params = []);

    public static function boolean($value, array $params = []);

    public static function date($value, array $params = []);

    public static function datetime($value, array $params = []);

    public static function decimal($value, array $params = []);

    public static function float($value, array $params = []);

    public static function integer($value, array $params = []);

    public static function time($value, array $params = []);

    public static function format($value, $format);
}
