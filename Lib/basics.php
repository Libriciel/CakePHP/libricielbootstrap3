<?php
if (function_exists('array_extract_keys') === false) {
    /**
     * Retourne un array contenant les entrées de $array dont les clés sont dans
     * $keys.
     *
     * @param array $array
     * @param array $keys
     * @return array
     */
    function array_extract_keys(array $array, array $keys)
    {
        return array_intersect_key($array, array_flip($keys));
    }
}

if (function_exists('array_except_keys') === false) {
    /**
     * Retourne un array contenant les entrées de $array dont les clés ne sont
     * pas dans $keys.
     *
     * @param array $array
     * @param array $keys
     * @return array
     */
    function array_except_keys(array $array, array $keys)
    {
        return array_diff_key($array, array_flip($keys));
    }
}

if (function_exists('array_extract_keys_by_prefix') === false) {
    /**
     * Retourne un array contenant les entrées de $array dont les clés commencent
     * par $prefix.
     *
     * @param array $array
     * @param string $prefix
     * @return array
     */
    function array_extract_keys_by_prefix(array $array, $prefix)
    {
        $result = [];

        foreach ($array as $key => $value) {
            if (strpos($key, $prefix) === 0) {
                $result[$key] = $value;
            }
        }

        return $result;
    }
}

if (function_exists('array_except_keys_by_prefix') === false) {
    /**
     * Retourne un array contenant les entrées de $array dont les clés ne
     * commencent pas par $prefix.
     *
     * @param array $array
     * @param string $prefix
     * @return array
     */
    function array_except_keys_by_prefix(array $array, $prefix)
    {
        $result = $array;

        foreach (array_keys($array) as $key) {
            if (strpos($key, $prefix) === 0) {
                unset($result[$key]);
            }
        }

        return $result;
    }
}
